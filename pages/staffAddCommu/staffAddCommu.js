// pages/staffAddCommu/staffAddCommu.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id:'',
    channel:{},
    content:''
  },
  send(){
    const data = this.data;
    if(!data.content){
      wx.showToast({
        title: '请输入沟通内容',
        duration:1500
      })
      return;
    }
    wx.showLoading({
      title: '提交中...',
    })
    wx.request({
      url: `${globalData.api.baseUrl}${globalData.api.channel.employeeAdd}?channelid=${data.id}&employeeid=${globalData.userInfo.id}&comment=${data.content}`,
      method:'POST',
      success:(res)=>{
        wx.hideLoading();
        if(res.statusCode==200){
          wx.showToast({
            title: '提交成功',

          })
          wx.navigateBack({
            delta: 1,
          })
        }else{
          wx.showToast({
            title: res.data.message,
            duration:2000,
            icon:'none'
          })
        }
      },
      complete:()=>{
       
      },
      fail:(error)=>{
        wx.hideLoading();
        wx.showToast({
          title: error,
          duration: 2000,
          icon: 'none'
        })
      }
    })
  },
  contentIn(e){
    this.setData({
      content:e.detail.value
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const id = options.id;
    this.setData({
      id
    })
  wx.request({
    url: globalData.api.baseUrl+globalData.api.channel.detail+id,
    success:(res)=>{
      if(res.statusCode==200){
        this.setData({
          channel:res.data.result
        })
      }else{
        wx.showToast({
          title: res.data.message,
          icon:'none'
        })
      }
    },
    complete:()=>{

    }
  })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})