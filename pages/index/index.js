//index.js
//获取应用实例
const globalData = getApp().globalData;
Page({
  data: {
    cardId: '',
   
  },
  changeId(e){
    const type = e.currentTarget.dataset.type;
    if(type==1){
      this.setData({
        cardId: 1
      })
      globalData.cardId = 1;
    }else{
      globalData.cardId = 3;
      this.setData({
        cardId: 3
      })
    }
  },
  next(){
    try {
      wx.setStorageSync('cardId', this.data.cardId)
    } catch (e) {

    }
    if (this.data.cardId!=1){
      wx.navigateTo({
        url: '/pages/chooseId/chooseId',
      })
      return;
    }
   
    wx.navigateTo({
      url: '/pages/login/login',
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    })
  },
  onLoad: function () {
    this.setData({
      cardId:globalData.cardId
    })
  },

   
})
