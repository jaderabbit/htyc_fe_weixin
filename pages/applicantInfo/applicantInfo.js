// pages/applicantInfo/applicantInfo.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id:'',
    certificate:[],
    edu:[],
    workexp:[],
    userInfo:{},
    industryDetailList: [],
    loctionDetailList: [],
    industry: '',
    loction: '',
    salary: '',
    jobs: '',
    lange: [],
    status: '',
    expand: {},
    skills: [],
    trains: [],
    projectexp: [],
    offer:{
      "Contract_period": '',
      "Fixed_salary": '',
      "Reporting_address": "",
      "Reporting_begin_date": '',
      "Reporting_contract": "",
      "Reporting_end_date": '',
      "Reporting_mobilephone": "",
      "address": "",
      "companyid": '',
      "department": "",
      "hire_begin_date": '',
      "hire_end_date": '',
      "jobid": '',
      "performance_salary": '',
      "position": "",
      "probation_period": '',
      "probation_period_salary": '',
      "salary": '',
      "userid": ''
    },
    offerShow:false,
    offerStatus:''
  },
  offerClick(){
   this.setData({
     offerShow: true
   })
  },
  send(){
    const offer = this.data.offer;
    if(!offer.department){
      wx.showToast({
        title: '请输入工作部门',
        icon:'none'
      })
      return;
    }
    if(!offer.salary){
      wx.showToast({
        title: '请输入标准薪水',
        icon:'none'
      })
      return;
    }
    if(!offer.Fixed_salary){
      wx.showToast({
        title: '请输入固定薪水',
        icon:'none'
      })
      return;
    }
    if(!offer.performance_salary){
      wx.showToast({
        title: '请输入绩效薪水',
        icon:'none'
      })
      return;
    }
    if(!offer.probation_period_salary){
      wx.showToast({
        title: '请输入试用期薪水',
        icon:'none'
      })
      return;
    }
    if(!offer.Contract_period){
      wx.showToast({
        title: '请输入合同期限',
        icon:'none'
      })
      return;
    }
    if(!offer.probation_period){
      wx.showToast({
        title: '请输入试用期限',
        icon:'none'
      })
      return;
    }
    if(!offer.Reporting_address){
      wx.showToast({
        title: '请输入报到地址',
        icon:'none'
      })
      return;
    }
    if(!offer.Reporting_contract){
      wx.showToast({
        title: '请输入联系人',
        icon:'none'
      })
      return;
    }
    if(!offer.Reporting_mobilephone){
      wx.showToast({
        title: '请输入联系方式',
        icon:'none'
      })
      return;
    }
    if(!offer.hire_begin_date){
      wx.showToast({
        title: '请选择聘用开始时间',
        icon:'none'
      })
      return;
    }
    if(!offer.hire_end_date){
      wx.showToast({
        title: '请选择聘用结束时间',
        icon:'none'
      })
      return;
    }
    if(!offer.Reporting_begin_date){
      wx.showToast({
        title: '请选择报到开始时间',
        icon:'none'
      })
      return;
    }
    if(!offer.Reporting_end_date){
      wx.showToast({
        title: '请选择报到结束时间',
        icon:'none'
      })
      return;
    }
    wx.showLoading({
      title: '发送中...',
    })
    offer.hire_begin_date = offer.hire_begin_date+' 00:00:00';
    offer.hire_end_date = offer.hire_end_date+' 00:00:00';
    offer.Reporting_begin_date = offer.Reporting_begin_date+' 00:00:00';
    offer.Reporting_end_date = offer.Reporting_end_date+' 00:00:00';
    wx.request({
      url: globalData.api.baseUrl + globalData.api.offer.opr,
      method:'POST',
      data:offer,
      success:(res)=>{
        if(res.statusCode==200){
          wx.hideLoading();
          wx.showToast({
            title: '发送成功',
          })
          this.setData({
            offerShow:false
          })
        }else{
          wx.showToast({
            title: res.data.message,
            icon:'none'
          })
        }
      },
      fail:(error)=>{
        wx.hideLoading();
        wx.showToast({
          title: error,
          icon:'none'
        })
      }
    })
  },
  Reporting_end_dateChange(e){
    if (new Date(this.data.offer.Reporting_begin_date.replace(/-/g, '/')).getTime() >= new Date(e.detail.value.replace(/-/g, '/')).getTime()) {
      wx.showToast({
        title: '结束日期必须大于开始日期',
        icon: 'none'
      })
      return;
    }
    this.setData({
      'offer.Reporting_end_date': e.detail.value
    })
  },
  Reporting_begin_dateChange(e){
    this.setData({
      'offer.Reporting_begin_date': e.detail.value
    })
  },
  hire_end_dateChange(e){
    if (new Date(this.data.offer.hire_begin_date.replace(/-/g, '/')).getTime() >= new Date(e.detail.value.replace(/-/g, '/')).getTime()) {
      wx.showToast({
        title: '结束日期必须大于开始日期',
        icon: 'none'
      })
      return;
    }
    this.setData({
      'offer.hire_end_date': e.detail.value
    })
  },
  hire_begin_dateChange(e){
    this.setData({
      'offer.hire_begin_date': e.detail.value
    })
  },
  Reporting_mobilephoneIn(e){
    this.setData({
      'offer.Reporting_mobilephone': e.detail.value
    })
  },
  Reporting_contractIn(e){
    this.setData({
      'offer.Reporting_contract': e.detail.value
    })
  },
  Reporting_addressIn(e){
    this.setData({
      'offer.Reporting_address': e.detail.value
    })
  },
  probation_periodIn(e){
    let probation_period = parseInt(e.detail.value);
    this.setData({
      'offer.probation_period': probation_period
    })
  },
  Contract_periodIn(e){
    let Contract_period = parseInt(e.detail.value);
    this.setData({
      'offer.Contract_period': Contract_period
    })
  },
  probation_period_salaryIn(e){
    let probation_period_salary = parseInt(e.detail.value);
    this.setData({
      'offer.probation_period_salary': probation_period_salary
    })
  },
  departmentIn(e){
    this.setData({
      'offer.department':e.detail.value
    })
  },
  salaryIn(e){
    let salary = parseInt(e.detail.value);
    this.setData({
      'offer.salary': salary
    })
  },
  Fixed_salaryIn(e){
    let Fixed_salary = parseInt(e.detail.value);
    this.setData({
      'offer.Fixed_salary': Fixed_salary
    })
  },
  performance_salaryIn(e){
    let performance_salary = parseInt(e.detail.value);
    this.setData({
      'offer.performance_salary': performance_salary
    })
  },
  getIndustry(id) {
    wx.request({
      url: globalData.api.baseUrl + globalData.api.dict.industry + id,
      success: (res) => {
        if (res.statusCode == 200) {
          const industryDetailList = res.data.result;
          this.setData({
            industryDetailList
          })
        }
      }
    })
  },
  getLoction(id) {
    wx.request({
      url: globalData.api.baseUrl + globalData.api.dict.city + id,
      success: (res) => {
        if (res.statusCode == 200) {
          const loctionDetailList = res.data.result;
          this.setData({
            loctionDetailList
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getIndustry('');
    this.getLoction('');
    try{
      let userJson = wx.getStorageSync('user');
      if(userJson){
        let userInfo = JSON.parse(userJson);
        const statusArr = ['暂无', '在职，无跳槽打算', '在职，正在找工作', '离职，正在找工作', '离职，无找工作计划', '自由职业者'];
        this.setData({
          status: statusArr[userInfo.status]
        })
        if (userInfo.birthday) {
          let birthYear = parseInt(userInfo.birthday.slice(0, 4));
          let year = new Date().getFullYear();
          userInfo.age = year - birthYear  + '岁';
        } else {
          userInfo.age = '暂无'
        }
        this.setData({
          userInfo
        })
        wx.removeStorageSync('user');
      }
    }catch(e){

    }
    const id = options.id;
    const jobid = options.jobid;
    const offerStatus = options.status;
    const offer = this.data.offer;
    offer.userid = parseInt(id);
    offer.jobid = parseInt(jobid);
    offer.companyid = globalData.userInfo.id;
    if(offerStatus){
      this.setData({
        offerStatus
      })
    }
   
    wx.request({
      url: globalData.api.baseUrl+globalData.api.job.add+jobid,
      success:(res)=>{
        if(res.statusCode==200){
         offer.position = res.data.result.name;
          offer.address = res.data.result.location
          this.setData({
            offer
          })
        }
      }
    })
    
    let cerUrl = globalData.api.user.certificate,
      eduUrl = globalData.api.user.edu,
      wrokexpUrl = globalData.api.user.workexp;
    wx.request({
      url: globalData.api.baseUrl + cerUrl + '/' + id,
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            certificate: res.data.result
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      fail: (error) => {
        wx.showToast({
          title: error,
          icon: 'none'
        })
      }
    })
    wx.request({
      url: globalData.api.baseUrl + eduUrl + '/' + id,
      success: (res) => {
        if (res.statusCode == 200) {
          const edu = res.data.result;
          edu.map((item) => {
            item.startYear = item.admission_date.slice(0, 4);
            item.endYear = item.graduation_date.slice(0, 4);
          })
          this.setData({
            edu
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      fail: (error) => {
        wx.showToast({
          title: error,
          icon: 'none'
        })
      }
    })
    wx.request({
      url: globalData.api.baseUrl + wrokexpUrl + '/' + id,
      success: (res) => {
        if (res.statusCode == 200) {
          const workexp = res.data.result;
          workexp.map((item) => {
            item.startDate = item.employment_date ? item.employment_date.slice(0, 10).replace(/-/g, '.') : item.employment_date;
            item.endDate = item.leave_date ? item.leave_date.slice(0, 10).replace(/-/g, '.') : item.leave_date;
          })
          this.setData({
            workexp
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      fail: (error) => {
        wx.showToast({
          title: error,
          icon: 'none'
        })
      }
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.user.lang + '/' + id,
      success: (res) => {
        if (res.statusCode == 200) {
          const typeArr = ['一般', '良好', '熟练', '精通'];
          const arr = res.data.result;
          arr.map(item => {
            item.hear = typeArr[item.hear - 1];
            item.read_write = typeArr[item.read_write - 1];
          })
          this.setData({
            lange: arr
          })
        }
      }
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.user.expand + '/' + id,
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            expand: res.data.result
          })
        }
      }
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.user.skill + '/' + id,
      success: (res) => {
        if (res.statusCode == 200) {
          const typeArr = ['一般', '良好', '熟练', '精通'];
          const arr = res.data.result;
          arr.map(item => {
            item.mastery_level = typeArr[item.mastery_level - 1];
          })
          this.setData({
            skills: arr
          })
        }
      }
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.user.train + '/' + id,
      success: (res) => {
        if (res.statusCode == 200) {
          const arr = res.data.result;
          arr.map(item => {
            item.start = item.begin_date.slice(0, 7).replace(/-/g, '.');
            item.end = item.end_date.slice(0, 7).replace(/-/g, '.');
          })
          this.setData({
            trains: arr
          })
        }
      }
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.user.projectexp + '/' + id,
      success: (res) => {
        if (res.statusCode == 200) {
          const arr = res.data.result;
          arr.map(item => {
            item.start = item.begin_date.slice(0, 7).replace(/-/g, '.');
            item.end = item.end_date.slice(0, 7).replace(/-/g, '.');
          })
          this.setData({
            projectexp: arr
          })
        }
      }
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.user.userRecommend + id,
      success: (res) => {
        if (res.statusCode == 200) {
          const data = res.data.result;
          let industrys = data.industrys?data.industrys.split(','):[];
          const regions = data.regions?data.regions.split(','):[];
          const industry = [],
            region = [];
          let salary = this.data.salary;
          let salaryInLow = this.data.salaryInLow;
          let salaryInUp = this.data.salaryInUp;
          if (data.pay_lower == -1) {
            salary = '不限';
          } else {
            salary = `${data.pay_lower}~${data.pay_upper}`;
          }
          industrys.map(item => {
            this.data.industryDetailList.map(inner => {
              if (inner.id == item) {
                industry.push(inner.name);
              }
            })
          })
          regions.map(item => {
            this.data.loctionDetailList.map(inner => {
              if (inner.id == item) {
                region.push(inner.name);
              }
            })
          })
          this.setData({
            industry: industry.join('/'),
            loction: region.join('/'),
            salary,
            jobs: data.jobs?data.jobs.replace(/,/g, '/'):''
          })
        } else {
          // wx.showToast({
          //   title: res.data.message,
          //   icon: 'none'
          // })
        }
      },
      fail: (error) => {
        wx.showToast({
          title: error,
          icon: 'none'
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})