// pages/talentTest/talentTest.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    exam: [],
    pageIndex: 1,
    pageSiez: 10,
    companyId: '',
    delBtnWidth: 120,
    startX: "",
    startY: "",
    confirmDeleteShow: false,
    currentIndex: ''
  },
  confirmDeleteToggle(e) {
    const status = e.currentTarget.dataset.status;
    const confirmDeleteShow = status == 1 ? true : false;
    this.setData({
      confirmDeleteShow
    })
  },
  touchS: function (e) {
    if (e.touches.length == 1) {
      this.setData({
        //设置触摸起始点水平方向位置
        startX: e.touches[0].clientX,
        startY: e.touches[0].clientY
      });
    }
  },
  touchM: function (e) {
    if (e.touches.length == 1) {
      //手指移动时水平方向位置
      var moveX = e.touches[0].clientX;
      var moveY = e.touches[0].clientY;
      //手指起始点位置与移动期间的差值
      var disX = this.data.startX - moveX;
      var disY = this.data.startY - moveY;
      if(Math.abs(disY)>Math.abs(disX)){
        return;
      }
      var delBtnWidth = this.data.delBtnWidth;
      var txtStyle = "";
      if (disX == 0 || disX < 0) { //如果移动距离小于等于0，说明向右滑动，文本层位置不变
        txtStyle = "left:0px";
      } else if (disX > 0) { //移动距离大于0，文本层left值等于手指移动距离
        txtStyle = "left:-" + disX + "px";
        if (disX >= delBtnWidth) {
          //控制手指移动距离最大值为删除按钮的宽度
          txtStyle = "left:-" + delBtnWidth + "px";
        }
      }
      //获取手指触摸的是哪一项
      var index = e.currentTarget.dataset.index;
      var exam = this.data.exam;
      exam[index].txtStyle = txtStyle;
      //更新列表的状态
      this.setData({
        exam
      });
    }
  },
  touchE: function (e) {
    if (e.changedTouches.length == 1) {
      //手指移动结束后水平位置
      var endX = e.changedTouches[0].clientX;
      var endY = e.changedTouches[0].clientY;
      //触摸开始与结束，手指移动的距离
      var disX = this.data.startX - endX;
      var disY = this.data.startY - endY;
      if(Math.abs(disY)>Math.abs(disX)){
        return;
      }
      var delBtnWidth = this.data.delBtnWidth;
      //如果距离小于删除按钮的1/2，不显示删除按钮
      var txtStyle = disX > delBtnWidth / 2 ? "left:-" + delBtnWidth + "px" : "left:0px";
      //获取手指触摸的是哪一项
      var index = e.currentTarget.dataset.index;
      var exam = this.data.exam;
      exam[index].txtStyle = txtStyle;
      //更新列表的状态
      this.setData({
        exam
      });
    }
  },
  //获取元素自适应后的实际宽度
  getEleWidth: function (w) {
    var real = 0;
    try {
      var res = wx.getSystemInfoSync().windowWidth;
      var scale = (750 / 2) / (w / 2);
      real = Math.floor(res / scale);
      return real;
    } catch (e) {
      return false;
      // Do something when catch error
    }
  },
  initEleWidth: function () {
    var delBtnWidth = this.getEleWidth(this.data.delBtnWidth);
    this.setData({
      delBtnWidth: delBtnWidth
    });
  },
  confirmDelete() {
    var exam = this.data.exam;
    var index = this.data.currentIndex;
    wx.request({
      url: globalData.api.baseUrl + globalData.api.exam.update + exam[index].id,
      method: 'DELETE',
      success: (res) => {
        if (res.statusCode == 200) {
          //移除列表中下标为index的项
          exam.splice(index, 1);
          //更新列表的状态
          this.setData({
            exam,
            confirmDeleteShow:false
          });
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      fail: (error) => {
        wx.showToast({
          title: error,
          icon: 'none'
        })
      }
    })
  },
  //点击删除按钮事件
  delItem: function (e) {
    //获取列表中要删除项的下标
    var index = e.currentTarget.dataset.index;
   this.setData({
     currentIndex:index,
     confirmDeleteShow: true
   })
  },
  toShare(e) {
    const companyId = e.currentTarget.dataset.company;
    const examId = e.currentTarget.dataset.exam;
    wx.navigateTo({
      url: '/pages/tanlMangeShare/tanlMangeShare?companyId=' + companyId + '&examId=' + examId,
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    })
  },
  toDetail(e) {
    wx.navigateTo({
      url: '/pages/tanlenTestDetail/tanlenTestDetail?id=' + e.currentTarget.dataset.id + '&name=' + e.currentTarget.dataset.name,
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    })
  },
  addTest() {
    wx.navigateTo({
      url: '/pages/addTest/addTest',
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    })
  },
  getTest() {
    const data = this.data;
    wx.request({
      url: `${globalData.api.baseUrl}${globalData.api.exam.list}${data.pageIndex}/${data.pageSiez}/${globalData.userInfo.id}`,
      success: (res) => {
        if (res.statusCode == 200) {
          const arr = res.data.result.data;
          let exam = data.exam;
          arr.map((item) => {
            item.invalid_date = item.invalid_date ? item.invalid_date.slice(0, 10).replace(/-/g, '.') : item.invalid_date;
            item.valid_date = item.valid_date ? item.valid_date.slice(0, 10).replace(/-/g, '.') : item.valid_date;
          })
          if(data.pageIndex>1){
            exam = exam.concat(arr);
          }else{
            exam = arr;
          }
         
          this.setData({
            exam
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      complete: () => {

      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      companyId: globalData.userInfo.id
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    this.initEleWidth();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.getTest();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {
    this.setData({
      pageIndex:1
    })
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    let pageIndex = this.data.pageIndex;
    pageIndex++;
    this.setData({
      pageIndex
    })
    this.getTest();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})