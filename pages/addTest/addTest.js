// pages/addTest/addTest.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    name:'',
    scope:'全公司',
    startDate:'',
    endDate:'',
    departmentShow: false,
    departmentList: [],
    departmentNum: 0
  },
  departmentToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        departmentShow: true
      })
    } else {
      this.setData({
        departmentShow: false
      })
      this.setData({
        scope: this.data.departmentList[this.data.departmentNum].name
      })
    }
  },
  departmentChange(e) {
    this.setData({
      departmentNum: e.detail.value[0]
    })
    this.setData({
      scope: this.data.departmentList[e.detail.value[0]].name
    })
  },
  nameIn(e){
    this.setData({
      name:e.detail.value
    })
  },
  scopeIn(e){
    this.setData({
      scope: e.detail.value
    })
  },
  startDateChange(e){
    this.setData({
      startDate: e.detail.value
    })
  },
  endDateChange(e){
    if (new Date(e.detail.value.replace(/-/g, '/')).getTime() <= new Date(this.data.startDate.replace(/-/g, '/')).getTime()) {
      wx.showToast({
        title: '结束日期必须大于开始日期',
        icon: 'none',
        duration: 2000
      })
      return;
    }
    this.setData({
      endDate: e.detail.value
    })
  },
  next(){
    const data = this.data;
    if(!data.name){
      wx.showToast({
        title: '请输入主题',
        duration:2000
      })
      return;
    }
    if(!data.scope){
      wx.showToast({
        title: '请输入参加部门',
        duration:2000
      })
      return;
    }
    if(!data.startDate){
      wx.showToast({
        title: '请选择开始时间',
        duration:2000
      })
      return;
    }
    if(!data.endDate){
      wx.showToast({
        title: '请选择结束时间',
        duration:2000
      })
      return;
    }
    data.startDate = data.startDate+' 00:00:00';
    data.endDate = data.endDate+' 00:00:00';
    wx.setStorageSync('TEST', JSON.stringify(data));
    wx.navigateTo({
      url: '/pages/addTanlenlTest/addTanlenlTest',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.request({
      url: globalData.api.baseUrl + globalData.api.department.list + '1/100/' + globalData.userInfo.id,
      method: 'GET',
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            departmentList: res.data.result.data
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      complete: () => {

      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})