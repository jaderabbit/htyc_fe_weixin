// pages/offerDetail/offerDetail.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    offer:{}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    try{
      const offerJson = wx.getStorageSync('offer');
      if(offerJson){
        const offer = JSON.parse(offerJson);
        if (!offer.company){
          offer.company = globalData.userInfo;
        }
        if(!offer.user){
          offer.user = globalData.userInfo;
        }
        offer.hire_begin_year = offer.hire_begin_date.slice(0,4);
        offer.hire_begin_month = offer.hire_begin_date.slice(5,7);
        offer.hire_begin_day = offer.hire_begin_date.slice(8,10);
        offer.hire_end_year = offer.hire_end_date.slice(0,4);
        offer.hire_end_month = offer.hire_end_date.slice(5,7);
        offer.hire_end_day = offer.hire_end_date.slice(8,10);
        offer.Reporting_begin_year = offer.Reporting_begin_date.slice(0,4);
        offer.Reporting_begin_month = offer.Reporting_begin_date.slice(5,7);
        offer.Reporting_begin_day = offer.Reporting_begin_date.slice(8,10);
        offer.Reporting_end_year = offer.Reporting_end_date.slice(0,4);
        offer.Reporting_end_month = offer.Reporting_end_date.slice(5,7);
        offer.Reporting_end_day = offer.Reporting_end_date.slice(8,10);
        offer.offer_year = offer.lastmodified.slice(0,4);
        offer.offer_month = offer.lastmodified.slice(5,7);
        offer.offer_day = offer.lastmodified.slice(8,10);
        offer.validay = Math.round((new Date(offer.Reporting_end_date.replace(/-/g, '/')).getTime() - new Date(offer.Reporting_begin_date.replace(/-/g, '/')).getTime()) / 1000 / 3600 / 24);
        this.setData({
          offer
        })
        wx.removeStorageSync('offer');
      }
    }catch(e){

    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})