// pages/answerResult/answerResult.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    result: {},
    name: '',
    examid: '',
    testType: ''
  },
  update() {
    const data = this.data;
    if (data.testType) {
      wx.redirectTo({
        url: '/pages/userChooseTest/userChooseTest',
      })
      return;
    }
    wx.redirectTo({
      url: `/pages/answerQuestions/answerQuestions?examid=${data.examid}&name=${data.name}&update=1`,
    })
  },
  getLabels(flag) {
    wx.request({
      url: globalData.api.baseUrl + globalData.api.exam.label + flag,
      success: (res) => {
        if (res.statusCode == 200) {
          if (res.data.result) {
            this.setData({
              result: res.data.result
            })
          }

        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const name = options.name;
    const examid = options.examid;
    const testType = options.testType;
    if (name) {
      this.setData({
        name,
        examid
      })
      wx.setNavigationBarTitle({
        title: name,
      })
    }
    examid && this.setData({
      examid
    })
    if (testType) {
      this.setData({
        testType
      })
      wx.request({
        url: `${globalData.api.baseUrl}${globalData.api.user.characteranswer}/${globalData.userInfo.id}`,
        success: (res) => {
          if (res.statusCode == 200) {
            this.getLabels(res.data.result.flag);
          } else {
            wx.showToast({
              title: res.data.message,
              icon: 'none'
            })
          }
        },
        fail: (error) => {
          wx.showToast({
            title: error,
            icon: 'none',
          })
        }
      })
      return;
    }
    wx.request({
      url: `${globalData.api.baseUrl}${globalData.api.exam.answer}/${examid}/${globalData.userInfo.id}`,
      success: (res) => {
        if (res.statusCode == 200) {
          this.getLabels(res.data.result.flag);
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      fail: (error) => {
        wx.showToast({
          title: error,
          icon: 'none',
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})