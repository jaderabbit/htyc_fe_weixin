// pages/guidePage/guidePage.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    inter:null
  },
  toIndex() {
    clearTimeout(this.data.inter);
    const now = new Date().getTime();
    try {
      const id = wx.getStorageSync('cardId');
      const userInfoJson = wx.getStorageSync('userInfo');
      const loginTime = wx.getStorageSync('loginTime');
      if (id) {
        globalData.cardId = id;
      }
      if (loginTime) {
        const diffDay = parseInt(((now - parseInt(loginTime)) / 1000 / 3600 / 24));
        if (diffDay < 30) {
          globalData.userInfo = JSON.parse(userInfoJson);
          if (id == 1) {
            wx.redirectTo({
              url: '/pages/companyHome/companyHome',
            })
          } else if (id == 4) {
            wx.redirectTo({
              url: '/pages/personalHome/personalHome',
            })
          } else if (id == 3) {
            wx.redirectTo({
              url: '/pages/staffCommu/staffCommu',
            })
          }
        }
        return;
      }
      if (id) {
        wx.navigateTo({
          url: '/pages/login/login',
        })
        return;
      }
    } catch (e) {

    }
    wx.navigateTo({
      url: '/pages/index/index',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
   let inter = setTimeout(() => {
     this.toIndex();
    }, 2000)
    this.setData({
      inter
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})