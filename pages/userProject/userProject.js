// pages/userProject/userProject.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    name:'',
    role:'',
    duty:'',
    companyName:'',
    startYear:'',
    endYear:'',
    brief:'',
    achievement:''
  },
  briefIn(e){
    this.setData({
      brief:e.detail.value
    })
  },
  achievementIn(e){
    this.setData({
      achievement:e.detail.value
    })
  },
  nameIn(e){
    this.setData({
      name:e.detail.value
    })
  },
  roleIn(e){
    this.setData({
      role:e.detail.value
    })
  },
  dutyIn(e){
    this.setData({
      duty:e.detail.value
    })
  },
  companyNameIn(e){
    this.setData({
      companyName:e.detail.value
    })
  },
  startYearChange(e) {
    this.setData({
      startYear: e.detail.value
    })
  },
  endYearChange(e) {
    if (new Date(this.data.startYear.replace(/-/g, '/')).getTime() >= new Date(e.detail.value.replace(/-/g, '/')).getTime()) {
      wx.showToast({
        title: '结束时间不能小于开始时间',
        icon: 'none'
      })
      return;
    }
    this.setData({
      endYear: e.detail.value
    })
  },
  save() {
    const data = this.data;
    if (!data.companyName) {
      wx.showToast({
        title: '请选输入公司名称',
        icon: 'none'
      })
      return;
    }
    if (!data.role) {
      wx.showToast({
        title: '请输入项目角色',
        icon: 'none'
      })
      return;
    }
    if (!data.duty) {
      wx.showToast({
        title: '请输入项目职责',
        icon: 'none'
      })
      return;
    }

    if (!data.startYear) {
      wx.showToast({
        title: '请选择入学年份',
        icon: 'none'
      })
      return;
    }
    if (!data.endYear) {
      wx.showToast({
        title: '请选择毕业年份',
        icon: 'none'
      })
      return;
    }
    if (!data.brief) {
      wx.showToast({
        title: '请输入项目描述',
        icon: 'none'
      })
      return;
    }
    if (!data.achievement) {
      wx.showToast({
        title: '请输入项目业绩',
        icon: 'none'
      })
      return;
    }
    let method = '', postData = null, projectexpUrl='';
    if (globalData.cardId == 3) {
      projectexpUrl = globalData.api.employee.projectexp;
    } else if (globalData.cardId == 4) {
      projectexpUrl = globalData.api.user.projectexp;
    }
    if (data.id) {
      method = 'PUT';
      postData = {
        "begin_date": data.startYear + ' 00:00:00',
        "achievement": data.achievement,
        "end_date": data.endYear + ' 00:00:00',
        "id": data.id,
        "brief": data.brief,
        "company_name": data.companyName,
        "role": data.role,
        "duty": data.duty,
        "name": data.name
      }
    } else {
      method = 'POST';
      postData = {
        "begin_date": data.startYear + ' 00:00:00',
        "achievement": data.achievement,
        "end_date": data.endYear + ' 00:00:00',
        "brief": data.brief,
        "company_name": data.companyName,
        "role": data.role,
        "duty": data.duty,
        "name": data.name
      }
      if (globalData.cardId == 3) {
        postData.employeeid = globalData.userInfo.id;
      } else if (globalData.cardId == 4) {
        postData.userid = globalData.userInfo.id;
      }
     
    }
    wx.showLoading({
      title: '保存中...',
    })
    wx.request({
      url: globalData.api.baseUrl + projectexpUrl,
      method: method,
      data: postData,
      success: (res) => {
        wx.hideLoading();
        if (res.statusCode == 200) {
          wx.showToast({
            title: '保存成功',
          })
          wx.navigateBack({
            delta: 1,
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      fail: (error) => {
        wx.hideLoading();
        wx.showToast({
          title: error,
          icon: 'none'
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const id = options.id;
    if (id) {
      wx.setNavigationBarTitle({
        title: '编辑项目经验',
      })
      try {
        let projectxpJson = wx.getStorageSync('projectxp');
        if (projectxpJson) {
          const projectxp = JSON.parse(projectxpJson);
          this.setData({
            id: parseInt(id),
            companyName: projectxp.company_name,
            name: projectxp.name,
            startYear: projectxp.begin_date.slice(0, 10),
            endYear: projectxp.end_date.slice(0, 10),
            brief: projectxp.brief,
            role: projectxp.role,
            duty: projectxp.duty,
            achievement: projectxp.achievement
          })
          wx.removeStorageSync('projectxp');
        }
      } catch (e) {

      }
    } else {
      wx.setNavigationBarTitle({
        title: '添加项目经验',
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})