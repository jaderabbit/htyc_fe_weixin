// pages/intlTanlMange/intlTanlMange.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    employeeLists: [],
    pageIndex: 1,
    pageSize: 10,
    searchPageIndex: 1,
    searchPageSize: 10,
    search: '',
    noreadcount:0,
    intlTanlShow:false
  },
  hintHide(){
    this.setData({
      intlTanlShow:false
    })
   
  },
  searchIn(e) {
    let val = e.detail.value.trim();
    if (val) {
      this.setData({
        searchPageIndex: 1
      })
      this.getSearchEmp(val);
    } else {
      this.setData({
        pageIndex: 1
      })
      this.getEmp();
    }
    this.setData({
      search: val,
      searchPageIndex: 1
    })
  },
  tanlantInfo(e) {
    const id = e.currentTarget.dataset.id;
    const num = e.currentTarget.dataset.num;
    try {
      wx.setStorageSync('user', JSON.stringify(this.data.employeeLists[num]))
    } catch (e) {

    }
    wx.navigateTo({
      url: '/pages/interTanlantInfo/interTanlantInfo?id=' + id,
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    })
  },
  getSearchEmp(key) {
    wx.request({
      url: `${globalData.api.baseUrl}${globalData.api.employee.multisearch}${globalData.userInfo.id}/${key}?pageIndex=${this.data.searchPageIndex}&pageSize=${this.data.searchPageSize}`,
      success: (res) => {
        if (res.statusCode == 200) {
          const arr = res.data.result.data;
          arr.map(item => {
            if (item.birthday) {
              let birthYear = parseInt(item.birthday.slice(0, 4));
              let year = new Date().getFullYear();
              item.age = year - birthYear  + '岁';
            } else {
              item.age = '暂无'
            }
          })
          let employeeLists = this.data.employeeLists;
          if (this.data.searchPageIndex>1){
            employeeLists = employeeLists.concat(arr);
          }else{
            employeeLists = arr;
          }
          this.setData({
            employeeLists
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      fail: (error) => {
        wx.showToast({
          title: error,
          icon: 'none'
        })
      }
    })
  },
  getEmp() {
    const data = this.data;
    wx.request({
      url: `${globalData.api.baseUrl}${globalData.api.employee.list}${data.pageIndex}/${data.pageSize}/${globalData.userInfo.id}?status=3`,
      success: (res) => {
        if (res.statusCode == 200) {
          const arr = res.data.result.data;
          arr.map(item=>{
            if (item.birthday) {
              let birthYear = parseInt(item.birthday.slice(0, 4));
              let year = new Date().getFullYear();
              item.age = year - birthYear  + '岁';
            } else {
              item.age = '暂无'
            }
          })
          let employeeLists = this.data.employeeLists;
          if(data.pageIndex>1){
            employeeLists = employeeLists.concat(arr);
          }else{
           if(data.pageIndex==1&&((arr&&arr.length==0)||!arr)){
             this.setData({
               intlTanlShow:true
             })
           }
            employeeLists = arr;
          }
          this.setData({
            employeeLists
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      complete: () => {

      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.getEmp();
    wx.request({
      url: globalData.api.baseUrl + globalData.api.channel.companyusernoreadcount + '?companyid=' + globalData.userInfo.id,
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            noreadcount: res.data.result
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    let pageIndex = this.data.pageIndex;
    let searchPageIndex = this.data.searchPageIndex;
    const val = this.data.search;
    if (val) {
      searchPageIndex++;
      this.setData({
        searchPageIndex
      })
      this.getSearchEmp(val);
    } else {
      pageIndex++;
      this.setData({
        pageIndex
      })
      this.getEmp();
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    let path = `/pages/codeLogin/codeLogin?companyId=${globalData.userInfo.id}`
    return { path: path };
  }
})