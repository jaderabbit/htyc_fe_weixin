// pages/userCer/userCer.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    time:'',
    currentImg:'',
    name:'',
    desc:'',
    id:'',
    img:''
  },
  save() {
    const data = this.data;
    if (!data.name) {
      wx.showToast({
        title: '请输入证书名称',
        icon: 'none'
      })
      return;
    }
    if (!data.img) {
      wx.showToast({
        title: '请上传证书图片',
        icon: 'none'
      })
      return;
    }
    if (!data.time) {
      wx.showToast({
        title: '请选获取时间',
        icon: 'none'
      })
      return;
    }
    if (!data.desc) {
      wx.showToast({
        title: '请输入证书描述',
        icon: 'none'
      })
      return;
    }
    
    let method = '', postData = null;
    let cerUrl = '';
    if (globalData.cardId == 3) {
      cerUrl = globalData.api.employee.certificate;
    } else if (globalData.cardId == 4) {
      cerUrl = globalData.api.user.certificate;
    }
    if (data.id) {
      method = 'PUT';
      postData = {
        "acquisition_date": data.time + ' 00:00:00',
        "brief": data.desc,
        "id": data.id,
        "name": data.name,
        cert_file:data.img
      }
    } else {
      method = 'POST';
      postData = {
        "acquisition_date": data.time + ' 00:00:00',
        "brief": data.desc,
        "name": data.name,
        cert_file:data.img
      }
      if (globalData.cardId == 3) {
        postData.employeeid = globalData.userInfo.id;
      } else if (globalData.cardId == 4) {
        postData.userid = globalData.userInfo.id;
      }
    }
    wx.showLoading({
      title: '保存中...',
    })
    wx.request({
      url: globalData.api.baseUrl + cerUrl,
      method: method,
      data: postData,
      success: (res) => {
        wx.hideLoading();
        if (res.statusCode == 200) {
          wx.showToast({
            title: '保存成功',
          })
          wx.navigateBack({
            delta: 1,
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      fail: (error) => {
        wx.hideLoading();
        wx.showToast({
          title: error,
          icon: 'none'
        })
      }
    })
  },
  descIn(e){
    this.setData({
      desc:e.detail.value
    })
  },
  nameIn(e){
    this.setData({
      name:e.detail.value
    })
  },
  timeChange(e){
    this.setData({
      time:e.detail.value
    })
  },
  upload() {
    wx.chooseImage({
      success: (res) => {
        const IMGURL = res.tempFilePaths[0];
        this.setData({
          currentImg: IMGURL
        })
        wx.uploadFile({
          url: globalData.api.baseUrl + globalData.api.common.uploadFile,
          filePath: IMGURL,
          name: 'name',
          success: (res) => {
            if (res.statusCode == 200) {
              this.setData({
                img: JSON.parse(res.data).result
              })
            }
          },
          fail: (res) => {
            console.log(res);
          }
        })
      },
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const id = parseInt(options.id);
    if (id) {
      wx.setNavigationBarTitle({
        title: '编辑证书',
      })
      try {
        let cerJson = wx.getStorageSync('cer');
        if (cerJson) {
          const cer = JSON.parse(cerJson);
          this.setData({
            id:parseInt(id),
            name: cer.name,
            time: cer.acquisition_date.slice(0, 10),
            desc: cer.brief,
            currentImg: 'https://futurenext.com.cn:8086/' + cer.cert_file
          })
          wx.removeStorageSync('cer');
        }
      } catch (e) {

      }
    }else{
      wx.setNavigationBarTitle({
        title: '新增证书',
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})