// pages/userWokrep/userWokrep.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    title:'',
    companyNmae:'',
    titleList: [],
    titleShow: false,
    titleNum: 0,
    titleId:0,
    startYear: '',
    endYear: '',
    content:'',
    id:'',
    types: [{
      id: 1,
      name: '全职'
    }, {
      id: 2,
      name: '兼职'
    }, {
      id: 3,
      name: '实习'
    }],
    type: '',
  },
  chooseType(e) {
    const id = e.currentTarget.dataset.id;
    this.setData({
      type: id
    })
  },
  save() {
    const data = this.data;
    if (!data.companyNmae) {
      wx.showToast({
        title: '请选输入公司名称',
        icon: 'none'
      })
      return;
    }
    if (!data.title) {
      wx.showToast({
        title: '请选择职称',
        icon: 'none'
      })
      return;
    }
    if (!data.type) {
      wx.showToast({
        title: '请选择岗位类型',
        icon: 'none'
      })
      return;
    }
   
    if (!data.startYear) {
      wx.showToast({
        title: '请选择入学年份',
        icon: 'none'
      })
      return;
    }
    if (!data.endYear) {
      wx.showToast({
        title: '请选择毕业年份',
        icon: 'none'
      })
      return;
    }
    if (!data.content) {
      wx.showToast({
        title: '请输入工作内容',
        icon: 'none'
      })
      return;
    }
    let method = '', postData = null;
    let  wrokexpUrl = '';
    if (globalData.cardId == 3) {
      wrokexpUrl = globalData.api.employee.workexp;
    } else if (globalData.cardId == 4) {
      wrokexpUrl = globalData.api.user.workexp;
    }
    if (data.id) {
      method = 'PUT';
      postData = {
        "employment_date": data.startYear + ' 00:00:00',
        "titleid": data.titleId,
        "leave_date": data.endYear + ' 00:00:00',
        "id": data.id,
        "brief": data.content,
        "compay_name": data.companyNmae,
        "type": data.type,
      }
    } else {
      method = 'POST';
      postData = {
        "employment_date": data.startYear + ' 00:00:00',
        "titleid": data.titleId,
        "leave_date": data.endYear + ' 00:00:00',
        "brief": data.content,
        "compay_name": data.companyNmae,
        "type": data.type,
      }
      if (globalData.cardId == 3) {
        postData.employeeid = globalData.userInfo.id;
      } else if (globalData.cardId == 4) {
        postData.userid = globalData.userInfo.id;
      }
    }
    wx.showLoading({
      title: '保存中...',
    })
    wx.request({
      url: globalData.api.baseUrl + wrokexpUrl,
      method: method,
      data: postData,
      success: (res) => {
        wx.hideLoading();
        if (res.statusCode == 200) {
          wx.showToast({
            title: '保存成功',
          })
          wx.navigateBack({
            delta: 1,
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      fail: (error) => {
        wx.hideLoading();
        wx.showToast({
          title: error,
          icon: 'none'
        })
      }
    })
  },
  contentIn(e){
    this.setData({
      content:e.detail.value
    })
  },
  startYearChange(e) {
    this.setData({
      startYear: e.detail.value
    })
  },
  endYearChange(e) {
    if (new Date(this.data.startYear.replace(/-/g, '/')).getTime() >= new Date(e.detail.value.replace(/-/g,'/')).getTime()) {
      wx.showToast({
        title: '离职时间不能小于入职时间',
        icon: 'none'
      })
      return;
    }
    this.setData({
      endYear: e.detail.value
    })
  },
  companyNmaeIn(e){
    this.setData({
      companyNmae:e.detail.value
    })
  },
  titleToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        titleShow: true
      })
    } else {
      this.setData({
        titleShow: false
      })
      this.setData({
        title: this.data.titleList[this.data.titleNum].name,
        titleId: this.data.titleList[this.data.titleNum].id
      })
    }
  },
  titleChange(e) {
    this.setData({
      titleNum: e.detail.value[0]
    })
    this.setData({
      title: this.data.titleList[e.detail.value[0]].name,
      titleId: this.data.titleList[e.detail.value[0]].id,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const id = options.id;
    if (id) {
      wx.setNavigationBarTitle({
        title: '编辑工作经历',
      })
      try {
        let workexpJson = wx.getStorageSync('workexp');
        if (workexpJson) {
          const workexp = JSON.parse(workexpJson);
          this.setData({
            id:parseInt(id),
            companyNmae: workexp.compay_name,
            title: workexp.title.name,
            startYear: workexp.employment_date.slice(0, 10),
            endYear: workexp.leave_date.slice(0, 10),
            content: workexp.brief,
            titleId: workexp.title.id,
            type:workexp.type
          })
          wx.removeStorageSync('workexp');
        }
      } catch (e) {

      }
    }else{
      wx.setNavigationBarTitle({
        title: '添加工作经历',
      })
    }
    wx.request({
      url: globalData.api.baseUrl + globalData.api.dict.titleList,
      method: 'GET',
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            titleList: res.data.result
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      complete: () => {

      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})