// pages/companyBasicInfo/companyBasicInfo.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},
    currentImg:'',
    logo: '',
    staffSizeArr:[{name:'0-49',id:1},{name:'50-99',id:2},{name:'100-499',id:3},{name:'500-999',id:4},{name:'1000-4999',id:5},{name:'5000以上',id:6}],
    staffSizeShow:false,
    staffSize:'',
    staffSizeNum:0,
    listedArr:[{name:'未上市',id:1},{name:'国内上市',id:2},{name:'海外上市',id:3}],
    listed:'',
    listedShow:false,
    listedNum:0,
    companyTypeArr:[{name:'国有企业',id:1},{name:'外资企业',id:2},{name:'民营企业',id:3},{name:'中外合资',id:4}],
    companyType:'',
    companyTypeShow:false,
    companyTypeNum:0,
    main_business:''
  },
  main_businessIn(e){
    this.setData({
      main_business:e.detail.value
    })
  },
  companyTypeToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        companyTypeShow: true
      })
    } else {
      this.setData({
        companyTypeShow: false
      })
      this.setData({
        companyType: this.data.companyTypeArr[this.data.companyTypeNum].name,
      })
    }
  },
  companyTypeChange(e) {
    this.setData({
      companyTypeNum: e.detail.value[0]
    })
  },
  staffSizeToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        staffSizeShow: true
      })
    } else {
      this.setData({
        staffSizeShow: false
      })
      this.setData({
        staffSize: this.data.staffSizeArr[this.data.staffSizeNum].name,
      })
    }
  },
  staffSizeChange(e) {
    this.setData({
      staffSizeNum: e.detail.value[0]
    })
  },
  listedToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        listedShow: true
      })
    } else {
      this.setData({
        listedShow: false
      })
      this.setData({
        listed: this.data.listedArr[this.data.listedNum].name,
      })
    }
  },
  listedChange(e) {
    this.setData({
      listedNum: e.detail.value[0]
    })
  },
  uploadLogo() {
    wx.chooseImage({
      success: (res) => {
        const IMGURL = res.tempFilePaths[0];
        this.setData({
          logo: IMGURL
        })
        wx.uploadFile({
          url: globalData.api.baseUrl + globalData.api.common.uploadFile,
          filePath: IMGURL,
          name: 'name',
          success: (res) => {
            if (res.statusCode == 200) {
              this.setData({
                'userInfo.user.logo': JSON.parse(res.data).result
              })
              globalData.userInfo.user.logo = JSON.parse(res.data).result;
            }
          },
          fail: (res) => {
            console.log(res);
          }
        })
      },
    })
  },
  upload() {
    wx.chooseImage({
      success: (res) => {
        const IMGURL = res.tempFilePaths[0];
        this.setData({
          currentImg: IMGURL
        })
        wx.uploadFile({
          url: globalData.api.baseUrl + globalData.api.common.uploadFile,
          filePath: IMGURL,
          name: 'name',
          success: (res) => {
            if (res.statusCode == 200) {
              this.setData({
                'userInfo.license_file': JSON.parse(res.data).result
              })
              globalData.userInfo.license_file = JSON.parse(res.data).result;
            }
          },
          fail: (res) => {
            console.log(res);
          }
        })
      },
    })
  },
  nameIn(e){
    console.log(e.detail.value)
    this.setData({
      'userInfo.name':e.detail.value
    })
  },
  licensecodeIn(e){
    this.setData({
      'userInfo.license_code': e.detail.value
    })
  },
  legalpersonIn(e){
    this.setData({
      'userInfo.legal_person': e.detail.value
    })
  },
  addressIn(e){
    this.setData({
      'userInfo.address': e.detail.value
    })
  },
  contactIn(e){
    this.setData({
      'userInfo.contact': e.detail.value
    })
  },
  updateInfo(){
    const userInfo = this.data.userInfo;
    if (!userInfo.name){
      wx.showToast({
        title: '请输入企业名称',
        duration:1500
      })
      return;
    }
    if (!userInfo.user.logo){
      wx.showToast({
        title: '请上传头像',
        duration:1500
      })
      return;
    }
    if (!userInfo.license_code){
      wx.showToast({
        title: '请输入营业执照号',
        duration:1500
      })
      return;
    }
    if (!userInfo.legal_person){
      wx.showToast({
        title: '请输入法人代表',
        duration:1500
      })
      return;
    }
    if (!userInfo.address){
      wx.showToast({
        title: '请输入办公地址',
        duration:1500
      })
      return;
    }
    if (!userInfo.contact){
      wx.showToast({
        title: '请输入联系人',
        duration:1500
      })
      return;
    }
    if (!this.data.staffSize){
      wx.showToast({
        title: '请选择人员规模',
        icon:'none'
      })
      return;
    }
    if (!this.data.companyType){
      wx.showToast({
        title: '请选择公司类型',
        icon:'none'
      })
      return;
    }
    if (!this.data.listed){
      wx.showToast({
        title: '请选择是否上市',
        icon:'none'
      })
      return;
    }
    if (!this.data.main_business){
      wx.showToast({
        title: '请输入主营业务',
        icon:'none'
      })
      return;
    }
    wx.showLoading({
      title: '更新中...',
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.company.update,
      method:'PUT',
      data:{
        "address": userInfo.address,
        "contact": userInfo.contact,
        "id": userInfo.id,
        "legalperson": userInfo.legal_person,
        "licensecode": userInfo.license_code,
        "name": userInfo.name,
        "be_listed": this.data.listedArr[this.data.listedNum].id,
        "company_type": this.data.companyTypeArr[this.data.companyTypeNum].id,
        "main_business": this.data.main_business,
        "staff_size": this.data.staffSizeArr[this.data.staffSizeNum].id,
        logo:userInfo.user.logo
      },
      success:(res)=>{
        wx.hideLoading();
        if(res.statusCode==200){
          wx.showToast({
            title: '更新成功',
          })
          globalData.userInfo.address = userInfo.address;
          globalData.userInfo.contact = userInfo.contact;
          globalData.userInfo.legal_person = userInfo.legal_person;
          globalData.userInfo.license_code = userInfo.license_code;
          globalData.userInfo.name = userInfo.name;
          globalData.userInfo.be_listed = this.data.listedArr[this.data.listedNum].id;
          globalData.userInfo.company_type = this.data.companyTypeArr[this.data.companyTypeNum].id;
          globalData.userInfo.staff_size = this.data.staffSizeArr[this.data.staffSizeNum].id;
          globalData.userInfo.main_business = this.data.main_business;
          try {
            wx.setStorageSync('userInfo', JSON.stringify(globalData.userInfo));
          } catch (e) {

          }
          this.setData({
            userInfo:globalData.userInfo
          })
         wx.navigateBack({
           delta: 1,
         })
        }else{
          wx.showToast({
            title: res.data.message,
            duration:2000,
            icon:'none'
          })
        }
      },
      complete:()=>{
        
      },
      fail:(error)=>{
        wx.hideLoading();
        wx.showToast({
          title: error,
          duration: 2000,
          icon: 'none'
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let staffSize = '',staffSizeNum=0;
    this.data.staffSizeArr.map((item,index)=>{
      if (item.id == globalData.userInfo.staff_size){
        staffSize = item.name;
        staffSizeNum = index;
      }
    })
    let companyType = '', companyTypeNum=0;
    this.data.companyTypeArr.map((item,index)=>{
      if (item.id == globalData.userInfo.company_type){
        companyType = item.name;
        companyTypeNum = index;
      }
    })
    let listed = '', listedNum=0;
    this.data.listedArr.map((item,index)=>{
      if (item.id == globalData.userInfo.be_listed){
        listed = item.name;
        listedNum = index;
      }
    })
    this.setData({
      userInfo: globalData.userInfo,
      currentImg:'https://futurenext.com.cn/' + globalData.userInfo.license_file,
      logo: 'https://futurenext.com.cn/' + globalData.userInfo.user.logo,
      'userInfo.user.logo': globalData.userInfo.user.logo,
      staffSize,
      staffSizeNum,
      companyType,
      companyTypeNum,
      listed,
      listedNum,
      main_business: globalData.userInfo.main_business
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  
})