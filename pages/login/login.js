// pages/login/login.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    loginType: 1, //1:密码登录，2：验证码登录
    cardId: '',
    phone: '',
    password: '',
    code: '',
    codeText: '获取验证码',
    isSendCode: false,
    examId:''
  },
  toHome(){
    try {
      wx.removeStorageSync('userInfo');
      wx.removeStorageSync('loginTime');
      wx.removeStorageSync('cardId');
      globalData.cardId = 1;
    } catch (e) {

    }
    wx.reLaunch({
      url: '/pages/index/index',
      success: function (res) { },
      fail: function (res) { },
      complete: function (res) { },
    })
  },
  sendCode() {
    const data = this.data;
    const type = data.cardId == 1 ? 'company' : data.cardId == 3 ? 'employee' :'user';
    if (data.isSendCode) {
      return;
    }
    const ISPHONE = /^1[3456789]\d{9}$/;
    if (!ISPHONE.test(data.phone)) {
      wx.showToast({
        title: '请输入正确的手机号',
        duration: 1500
      })
      return;
    }
    wx.request({
      url: globalData.api.baseUrl + globalData.api.common.loginCaptcha+type+'/' +data.phone,
      method: 'GET',
      success: (res) => {
        if (res.statusCode == 200) {

        } else {
          wx.showToast({
            title: res.data.message,
            duration:3000,
            icon:'none'
          })
        }
      },
      complete: () => {

      }
    })
    let time = 60;
    let inter = null;
    this.setData({
      isSendCode: true,
      codeText: '还剩60s'
    })
    inter = setInterval(() => {
      time--;
      this.setData({
        codeText: `还剩${time}s`
      })
      if (time == 0) {
        clearInterval(inter);
        this.setData({
          isSendCode: false,
          codeText: '获取验证码'
        })
      }
    }, 1000)
  },
  phoneIn(e) {
    this.setData({
      phone: e.detail.value
    })
  },
  passwordIn(e) {
    this.setData({
      password: e.detail.value
    })
  },
  codeIn(e) {
    this.setData({
      code: e.detail.value
    })
  },
  login() {
    const data = this.data;
    const ISPHONE = /^1[3456789]\d{9}$/;
    if (!ISPHONE.test(data.phone)) {
      wx.showToast({
        title: '请输入正确的手机号',
        duration: 1500
      })
      return;
    }
    if (data.loginType == 1 && !data.password) {
      wx.showToast({
        title: '请输入密码',
        duration: 1500
      })
      return;
    }
    if (data.loginType == 2 && !data.code) {
      wx.showToast({
        title: '请输入验证码',
        duration: 1500
      })
      return;
    }
    wx.showLoading({
      title: '登录中...',
    })
    if (data.cardId == 1) { //企业登录
      if (data.loginType == 1) {
        wx.request({
          url: `${globalData.api.baseUrl}${globalData.api.company.login}?mobilephone=${data.phone}&password=${data.password}`,
          method: 'POST',
          success: (res) => {
            wx.hideLoading();
            if (res.statusCode == 200) {
              globalData.userInfo = res.data.result;
              let loginTime = new Date().getTime();
              try{
                wx.setStorageSync('userInfo', JSON.stringify(res.data.result));
                wx.setStorageSync('loginTime', loginTime);
              }catch(e){

              }
              wx.showToast({
                title: '登录成功',
              })
              if (res.data.result.user.is_admin){
                wx.navigateTo({
                  url: '/pages/companyHome/companyHome',
                  success: function (res) { },
                  fail: function (res) { },
                  complete: function (res) { },
                })
              }else{
                wx.navigateTo({
                  url: '/pages/recruit/recruit',
                  success: function (res) { },
                  fail: function (res) { },
                  complete: function (res) { },
                })
              }
             
            } else {
              wx.showToast({
                title: res.data.message,
                duration: 2000,
                icon: 'none'
              })
            }
          },
          fail: (error) => {
            wx.hideLoading();
            wx.showToast({
              title: error,
              duration: 2000,
              icon: 'none'
            })
          },
          complete: () => {
            
          }
        })
      } else {
        wx.request({
          url: `${globalData.api.baseUrl}${globalData.api.company.loginByCode}?mobilephone=${data.phone}&identifyingcode=${data.code}`,
          method: 'POST',
          success: (res) => {
            wx.hideLoading();
            if (res.statusCode == 200) {
              globalData.userInfo = res.data.result;
              let loginTime = new Date().getTime();
              try {
                wx.setStorageSync('userInfo', JSON.stringify(res.data.result));
                wx.setStorageSync('loginTime', loginTime);
              } catch (e) {

              }
              wx.showToast({
                title: '登录成功',
              })
              if (res.data.result.user.is_admin) {
                wx.navigateTo({
                  url: '/pages/companyHome/companyHome',
                  success: function (res) { },
                  fail: function (res) { },
                  complete: function (res) { },
                })
              } else {
                wx.navigateTo({
                  url: '/pages/recruit/recruit',
                  success: function (res) { },
                  fail: function (res) { },
                  complete: function (res) { },
                })
              }
            } else {
              wx.showToast({
                title: res.data.message,
                duration: 2000,
                icon: 'none'
              })
            }
          },
          fail: (error) => {
            wx.hideLoading();
            wx.showToast({
              title: error,
              duration: 2000,
              icon: 'none'
            })
          },
          complete: () => {
           
          }
        })
      }
      return;
    }
    if (data.cardId == 4) {
      if (data.loginType == 1) {
        wx.request({
          url: `${globalData.api.baseUrl}${globalData.api.user.login}?telephone=${data.phone}&password=${data.password}`,
          method: 'POST',
          success: (res) => {
            wx.hideLoading();
            if (res.statusCode == 200) {
              globalData.userInfo = res.data.result;
              let loginTime = new Date().getTime();
              try {
                wx.setStorageSync('userInfo', JSON.stringify(res.data.result));
                wx.setStorageSync('loginTime', loginTime);
              } catch (e) {

              }
              wx.showToast({
                title: '登录成功',
              })
              wx.navigateTo({
                url: '/pages/personalHome/personalHome',
                success: function(res) {},
                fail: function(res) {},
                complete: function(res) {},
              })
            } else {
              wx.showToast({
                title: res.data.message,
                duration: 2000,
                icon: 'none'
              })
            }
          },
          fail: (error) => {
            wx.hideLoading();
            wx.showToast({
              title: error,
              duration: 2000,
              icon: 'none'
            })
          },
          complete: () => {
            
          }
        })
      } else {
        wx.request({
          url: `${globalData.api.baseUrl}${globalData.api.user.identifyingcode}?mobilephone=${data.phone}&identifyingcode=${data.code}`,
          method: 'POST',
          success: (res) => {
            wx.hideLoading();
            if (res.statusCode == 200) {
              globalData.userInfo = res.data.result;
              let loginTime = new Date().getTime();
              try {
                wx.setStorageSync('userInfo', JSON.stringify(res.data.result));
                wx.setStorageSync('loginTime', loginTime);
              } catch (e) {

              }
              wx.showToast({
                title: '登录成功',
              })
              wx.navigateTo({
                url: '/pages/personalHome/personalHome',
                success: function(res) {},
                fail: function(res) {},
                complete: function(res) {},
              })
            } else {
              wx.showToast({
                title: res.data.message,
                duration: 2000,
                icon: 'none'
              })
            }
          },
          fail: (error) => {
            wx.hideLoading();
            wx.showToast({
              title: error,
              duration: 2000,
              icon: 'none'
            })
          },
          complete: () => {
            
          }
        })
      }
      return;
    }
    if (data.cardId == 3) {
      if (data.loginType == 1) {
        wx.request({
          url: `${globalData.api.baseUrl}${globalData.api.employee.login}?telephone=${data.phone}&password=${data.password}`,
          method: 'POST',
          success: (res) => {
            wx.hideLoading();
            if (res.statusCode == 200) {
              globalData.userInfo = res.data.result;
              let status = res.data.result.status;
              if(status==1){
                wx.showToast({
                  title: '您的员工身份未通过审核，请耐心等待',
                  icon:'none'
                })
                return;
              }
              if(status==2){
                wx.showToast({
                  title: '您的员工身份审核被拒绝，请联系管理员',
                  icon:'none'
                })
                return;
              }
              let loginTime = new Date().getTime();
              try {
                wx.setStorageSync('userInfo', JSON.stringify(res.data.result));
                wx.setStorageSync('loginTime', loginTime);
              } catch (e) {

              }
              wx.showToast({
                title: '登录成功',
              })
              wx.navigateTo({
                url: '/pages/staffCommu/staffCommu',
                success: function(res) {},
                fail: function(res) {},
                complete: function(res) {},
              })
            } else {
              wx.showToast({
                title: res.data.message,
                duration: 2000,
                icon: 'none'
              })
            }
          },
          fail: (error) => {
            wx.hideLoading();
            wx.showToast({
              title: error,
              duration: 2000,
              icon: 'none'
            })
          },
          complete: () => {
           
          }
        })
      } else {
        wx.request({
          url: `${globalData.api.baseUrl}${globalData.api.employee.identifyingcode}?mobilephone=${data.phone}&identifyingcode=${data.code}`,
          method: 'POST',
          success: (res) => {
            wx.hideLoading();
            if (res.statusCode == 200) {
              globalData.userInfo = res.data.result;
              let loginTime = new Date().getTime();
              try {
                wx.setStorageSync('userInfo', JSON.stringify(res.data.result));
                wx.setStorageSync('loginTime', loginTime);
              } catch (e) {

              }
              wx.showToast({
                title: '登录成功',
              })
              wx.navigateTo({
                url: '/pages/staffCommu/staffCommu',
                success: function(res) {},
                fail: function(res) {},
                complete: function(res) {},
              })
            } else {
              wx.showToast({
                title: res.data.message,
                duration: 2000,
                icon: 'none'
              })
            }
          },
          fail: (error) => {
            wx.hideLoading();
            wx.showToast({
              title: error,
              duration: 2000,
              icon: 'none'
            })
          },
          complete: () => {
           
          }
        })
      }
      return;
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const phone = options.phone,
      examId = options.examId;
    if(phone){
      this.setData({
        phone
      })
    }
    if (examId){
      this.setData({
        examId: options.examId
      })
    }
   
  },
  changLoginType(e) {
    const loginType = e.currentTarget.dataset.type;
    this.setData({
      loginType
    })
  },
  toRegister() {
    wx.navigateTo({
      url: '/pages/conpanyRegister/conpanyRegister',
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.setData({
      cardId: globalData.cardId
    })
    const cardId = this.data.cardId;
    if (cardId == 1) {
      wx.setNavigationBarTitle({
        title: '企业登录',
      })
    } else if (cardId == 3) {
      wx.setNavigationBarTitle({
        title: '员工登录',
      })
    } else if (cardId == 4) {
      wx.setNavigationBarTitle({
        title: '个人登录',
      })
    }
  },
  forgetPassword(){
    wx.navigateTo({
      url: '/pages/forgetPassword/forgetPassword',
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },



})