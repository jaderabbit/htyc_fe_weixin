// pages/userSelfIntro/userSelfIntro.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    label: '',
    hint: '',
    content: '',
    id:'',
    type:''
  },
  contentIn(e) {
    this.setData({
      content: e.detail.value
    })
  },
  save(){
    if(!this.data.content){
      wx.showToast({
        title: '请输入内容',
        icon:'none'
      })
      return;
    }
    let method = '', postData = null, expandUrl='';
    if (globalData.cardId == 3) {
      expandUrl = globalData.api.employee.expand;
    } else if (globalData.cardId == 4) {
      expandUrl = globalData.api.user.expand;
    }
    if (this.data.id) {
      method = 'PUT';
      postData = {
        "additional_information": this.data.type==1?'':this.data.content,
        "id": this.data.id,
        "self_evaluation": this.data.type == 1 ? this.data.content : ''
      }
    } else {
      method = 'POST';
      postData = {
        "additional_information": this.data.type == 1 ? '' : this.data.content,
        "self_evaluation": this.data.type == 1 ? this.data.content : '',
      }
      if (globalData.cardId == 3) {
        postData.employeeid = globalData.userInfo.id;
      } else if (globalData.cardId == 4) {
        postData.userid = globalData.userInfo.id;
      }
    }
    wx.showLoading({
      title: '保存中...',
    })
    wx.request({
      url: globalData.api.baseUrl + expandUrl,
      method: method,
      data: postData,
      success: (res) => {
        wx.hideLoading();
        if (res.statusCode == 200) {
          wx.showToast({
            title: '保存成功',
          })
          wx.navigateBack({
            delta: 1,
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      fail: (error) => {
        wx.hideLoading();
        wx.showToast({
          title: error,
          icon: 'none'
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const type = options.type;
    const id = options.id;
    this.setData({
      type
    })
    if (type == 1) {
      this.setData({
        label: '自我评价',
        hint: '请输入自我评价内容'
      })
      if(id){
        wx.setNavigationBarTitle({
          title: '编辑自我评价',
        })
        try{
          let self = wx.getStorageSync('self');
          console.log(self);
          this.setData({
            id: parseInt(id),
            content: self
          })
          wx.removeStorageSync('self');
        }catch(e){

        }
      }else{
        wx.setNavigationBarTitle({
          title: '添加自我评价',
        })
      }
    } else {
      if (id) {
        wx.setNavigationBarTitle({
          title: '编辑附加信息',
        })
        try {
          let expand = wx.getStorageSync('expand');
          this.setData({
            id: parseInt(id),
            content: expand
          })
          wx.removeStorageSync('expand');
        } catch (e) {

        }
       
      } else {
        wx.setNavigationBarTitle({
          title: '添加附加信息',
        })
      }
      this.setData({
        label: '附加信息',
        hint: '可以填写一些您的兴趣爱好、特长等内容'
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})