// pages/personalEditInfo/personalEditInfo.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo:{},
    sexArr:['女','男'],
    borthDate:'1985.03',
    marryArr: ['已婚未育','未婚','已婚已育'],
    currentImg:''
  },
  upload() {
    wx.chooseImage({
      success: (res) => {
        const IMGURL = res.tempFilePaths[0];
        this.setData({
          currentImg: IMGURL
        })
        wx.uploadFile({
          url: globalData.api.baseUrl + globalData.api.common.uploadFile,
          filePath: IMGURL,
          name: 'name',
          success: (res) => {
            if (res.statusCode == 200) {
              this.setData({
                'userInfo.logo': JSON.parse(res.data).result
              })
            }
          },
          fail: (res) => {
            console.log(res);
          }
        })
      },
    })
  },
  emailIn(e){
    this.setData({
      'userInfo.email':e.detail.value
    })
  },
  nameIn(e){
    this.setData({
      'userInfo.name':e.detail.value
    })
  },
  sexChange(e){
    console.log(e.detail.value)
    this.setData({
      'userInfo.sex': e.detail.value==0?true:false
    })
  },
  birthdayChange(e){
    this.setData({
      'userInfo.birthday': e.detail.value
    })
  },
  marryChange(e){
    console.log(e)
    this.setData({
      'userInfo.ismarried': this.data.marryArr[e.detail.value] == '未婚' ? '0' : this.data.marryArr[e.detail.value] == '已婚未育' ? '1' : '2'
    })
  },
  // telephoneIn(e){
  //   this.setData({
  //     'userInfo.telephone': e.detail.value
  //   })
  // },
  cityChange(e){
    console.log(e)
    this.setData({
      'userInfo.city': e.detail.value[1] + e.detail.value[2]
    })
  },
 birthplaceChange(e){
    console.log(e)
    this.setData({
      'userInfo.birthplace': e.detail.value[1] + e.detail.value[2]
    })
  },
  update(){
    const userInfo = this.data.userInfo;
    const ISEMAIL = /^([a-zA-Z]|[0-9])(\w|\-)+@[a-zA-Z0-9]+\.([a-zA-Z]{2,4})$/;
    if(!userInfo.name){
      wx.showToast({
        title: '请输入姓名',
        duration:1500
      })
      return;
    }
    
    if (!userInfo.birthday){
      wx.showToast({
        title: '请选择出生年月',
        duration:1500
      })
      return;
    }
    if (!userInfo.ismarried){
      wx.showToast({
        title: '请选择婚育情况',
        duration:1500
      })
      return;
    }
    if (!userInfo.logo){
      wx.showToast({
        title: '请上传头像',
        duration:1500
      })
      return;
    }
    if (!ISEMAIL.test(userInfo.email)){
      wx.showToast({
        title: '请输入正确的邮箱',
        duration:1500
      })
      return;
    }
    if (!userInfo.city){
      wx.showToast({
        title: '请选择城市',
        duration:1500
      })
      return;
    }
    wx.showLoading({
      title: '更新中...',
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.user.edit,
      method:'PUT',
      data:{
        "birthday": userInfo.birthday+' 00:00:00',
        "birthplace": userInfo.birthplace,
        "city": userInfo.city,
        "id": userInfo.id,
        "ismarried": userInfo.ismarried,
        "name": userInfo.name,
        "sex": userInfo.sex,
        logo: userInfo.logo,
        "email": userInfo.email,
        "video_file": globalData.userInfo.video_file
      },
      success:(res)=>{
        wx.hideLoading()
        if(res.statusCode==200){
          wx.showToast({
            title: '更新成功',
          })
          globalData.userInfo.birthday = userInfo.birthday;
          globalData.userInfo.city = userInfo.city;
          globalData.userInfo.ismarried = userInfo.ismarried;
          globalData.userInfo.name = userInfo.name;
          globalData.userInfo.sex = userInfo.sex;
          globalData.userInfo.logo = userInfo.logo;
          globalData.userInfo.email = userInfo.email;
          // globalData.userInfo.video_file = '111';
          try {
            wx.setStorageSync('userInfo', JSON.stringify(globalData.userInfo));
          } catch (e) {

          }
          wx.navigateBack({
            delta: 1,
          })
        }else{
          wx.showToast({
            title: res.data.message,
            mask: true,
            duration:2000,
            icon:'none',
            success: function(res) {},
            fail: function(res) {},
            complete: function(res) {},
          })
        }
      },
      complete:()=>{
        
      },
      fail:(error)=>{
        wx.hideLoading()
        wx.showToast({
          title: error,
          mask: true,
          duration: 2000,
          icon: 'none',
          success: function (res) { },
          fail: function (res) { },
          complete: function (res) { },
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const userInfo = globalData.userInfo;
    userInfo.birthday = userInfo.birthday.slice(0,10);
    this.setData({
      userInfo
    })
    if (globalData.userInfo.logo){
      this.setData({
        currentImg: 'https://futurenext.com.cn:8086/' + globalData.userInfo.logo,
        'userInfo.logo': globalData.userInfo.logo
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})