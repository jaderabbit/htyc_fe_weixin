// pages/answerQuestions/answerQuestions.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    exam: {},
    single: {},
    num: 1,
    percent: 0,
    answer: [],
    isUpdate: '',
    isCharactUpdate:'',
    name: '',
    testType: ''
  },
  chooseAnswer(e) {
    const num = e.currentTarget.dataset.num;
    const answer = this.data.answer;
    answer[this.data.num - 1].code = this.data.single[`option${num}_code`];
    this.setData({
      answer
    })
    setTimeout(()=>{
      this.checkResult();
    },300)
   
  },
  rollBack(){
    let num = this.data.num;
    if(num==1){
      return;
    }
    num--;
    const percent = parseInt((num / this.data.exam.singles.length) * 100)
    this.setData({
      single: this.data.exam.singles[num-1],
      num,
      percent
    })
  },
  checkResult() {
    const data = this.data;
    let num = data.num;
    if (!data.answer[num - 1].code) {
      wx.showToast({
        title: '请选择当前问题答案',
        icon: 'none'
      })
      return;
    }
    this.setData({
      single: data.exam.singles[num]
    })
    num++;

    if (num > data.exam.singles.length) {
      let method = data.isUpdate ? 'PUT' : 'POST';
      let charctmethod = data.isCharactUpdate ? 'PUT' : 'POST';
      if (data.testType) {
        wx.request({
          url: globalData.api.baseUrl + globalData.api.user.characteranswer,
          method: charctmethod,
          data: {
            "exam_singles": data.answer,
            "examid": data.exam.id,
            "userid": globalData.userInfo.id
          },
          success: (res) => {
            if (res.statusCode == 200) {
              console.log(res)
              wx.navigateTo({
                url: '/pages/answerResult/answerResult?examid=' + data.exam.id + '&name=' + data.name + '&testType=' + data.testType,
                success: function(res) {},
                fail: function(res) {},
                complete: function(res) {},
              })
            } else {
              wx.showToast({
                title: res.data.message,
                icon: 'none'
              })
            }
          },
          fail: (error) => {
            wx.showToast({
              title: error,
              icon: 'none'
            })
          }
        })
        return;
      }
      wx.request({
        url: globalData.api.baseUrl + globalData.api.exam.answer,
        method: method,
        data: {
          "employee_id": globalData.userInfo.id,
          "exam_singles": data.answer,
          "id": data.exam.id
        },
        success: (res) => {
          if (res.statusCode == 200) {
            console.log(res)
            wx.navigateTo({
              url: '/pages/answerResult/answerResult?examid=' + data.exam.id + '&name=' + data.name,
              success: function(res) {},
              fail: function(res) {},
              complete: function(res) {},
            })
          } else {
            wx.showToast({
              title: res.data.message,
              icon: 'none'
            })
          }
        },
        fail: (error) => {
          wx.showToast({
            title: error,
            icon: 'none'
          })
        }
      })

      return;
    }
    const percent = parseInt((num / data.exam.singles.length) * 100)
    this.setData({
      num,
      percent
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const id = options.examid;
    const name = options.name;
    const type = options.type;
    const testType = options.testType;
    console.log(type, testType)
    if (testType == 'user' && type) {
      this.setData({
        testType
      })
      let isStudent = type == 1 ? true : false;
      wx.request({
        url: globalData.api.baseUrl + globalData.api.user.character + '?isstudent=' + isStudent,
        success: (res) => {
          if (res.statusCode == 200) {
            const exam = res.data.result;
            const single = exam.singles[0];
            const answer = [];
            wx.setNavigationBarTitle({
              title: exam.name,
            })
            exam.singles.map((item) => {
              let obj = {
                "code": "",
                "id": item.id,
              }
              answer.push(obj);
            })
            const percent = parseInt((1 / exam.singles.length) * 100);
            this.setData({
              exam,
              single,
              percent,
              answer,
              name:exam.name
            })
          } else {
            wx.showToast({
              title: res.data.message,
              icon: 'none'
            })
          }
        },
        fail: (error) => {
          wx.showToast({
            title: error,
            icon: 'none'
          })
        }
      })
    }
    if (name) {
      this.setData({
        name
      })
      wx.setNavigationBarTitle({
        title: name,
        success: function(res) {},
        fail: function(res) {},
        complete: function(res) {},
      })
    }

    const update = options.update;
    if (update) {
      this.setData({
        isUpdate: update
      })
    }

    if (id) {
      wx.request({
        url: globalData.api.baseUrl + globalData.api.exam.paperDetail + id,
        success: (res) => {
          if (res.statusCode == 200) {
            const exam = res.data.result;
            const single = exam.singles[0];
            const answer = [];
            exam.singles.map((item) => {
              let obj = {
                "code": "",
                "id": item.id,
                "sequenceid": item.sequenceid
              }
              answer.push(obj);
            })
            const percent = parseInt((1 / exam.singles.length) * 100);
            this.setData({
              exam,
              single,
              percent,
              answer
            })
          } else {
            wx.showToast({
              title: res.data.message,
              icon: 'none'
            })
          }
        },
        fail: (error) => {
          wx.showToast({
            title: error,
            icon: 'none'
          })
        }
      })
    }
    wx.request({
      url: globalData.api.baseUrl + globalData.api.user.isanswer+'?userid='+globalData.userInfo.id,
      success:(res)=>{
        if(res.statusCode==200){
          this.setData({
            isCharactUpdate:res.data.result
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})