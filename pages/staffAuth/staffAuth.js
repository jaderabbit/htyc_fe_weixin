// pages/staffAuth/staffAuth.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    employeeLists: [],
    pageIndex: 1,
    pageSize: 10,
  },
  opre(e){
    const num = e.currentTarget.dataset.num;
    const id = e.currentTarget.dataset.id;
    const status = parseInt(e.currentTarget.dataset.status);
    wx.request({
      url: `${globalData.api.baseUrl}${globalData.api.employee.status}?id=${id}&status=${status}`,
      method:'PUT',
      success:(res)=>{
        if(res.statusCode==200){
          let employeeLists = this.data.employeeLists;
          employeeLists.splice(num,1);
          this.setData({
            employeeLists
          })
          wx.showToast({
            title: '操作成功',
            icon:'none'
          })
        }else{
          wx.showToast({
            title: res.data.message,
            icon:'none'
          })
        }
      },
      fail:(error)=>{
        wx.showToast({
          title: error,
          icon:'none'
        })
      }
    })
  },
  getEmp() {
    const data = this.data;
    wx.request({
      url: `${globalData.api.baseUrl}${globalData.api.employee.list}${data.pageIndex}/${data.pageSize}/${globalData.userInfo.id}?status=1`,
      success: (res) => {
        if (res.statusCode == 200) {
          const arr = res.data.result.data;
          arr.map(item => {
            if (item.birthday) {
              let birthYear = parseInt(item.birthday.slice(0, 4));
              let year = new Date().getFullYear();
              item.age = year - birthYear  + '岁';
            } else {
              item.age = '暂无'
            }
            item.name = item.name.slice(0,1)+'**';
          })
          let employeeLists = this.data.employeeLists;
          if (data.pageIndex > 1) {
            employeeLists = employeeLists.concat(arr);
          } else {
            employeeLists = arr;
          }
          this.setData({
            employeeLists
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      complete: () => {

      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getEmp();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
      let pageIndex = this.data.pageIndex;
      pageIndex++;
      this.setData({
        pageIndex
      })
      this.getEmp();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})