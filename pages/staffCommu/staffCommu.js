// pages/staffCommu/staffCommu.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    channelLists: [],
    pageIndex: 1,
    pageSize: 10,
    noreadcount:0,
    is_first_login:0
  },
  hintHide(){
    globalData.userInfo.is_first_login = 0;
    this.setData({
      is_first_login:0
    })
  },
  addCommu(e){
    wx.navigateTo({
      url: '/pages/staffComuDetail/staffComuDetail?id=' + e.currentTarget.dataset.id + '&name=' + e.currentTarget.dataset.name,
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      is_first_login:globalData.userInfo.is_first_login
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.message.noreadcount + '?msgType=employee' + '&accountid=' + globalData.userInfo.id,
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            noreadcount: res.data.result
          })
        }
      }
    })
  },
  getChannelLists(){
    const data = this.data;
    wx.request({
      url: `${globalData.api.baseUrl}${globalData.api.channel.list}${data.pageIndex}/${data.pageSize}/${globalData.userInfo.companyid}`,
      success: (res) => {
        if (res.statusCode == 200) {
          const arr = res.data.result.data;
          let channelLists = data.channelLists;
          let nowTime = new Date().getTime();
          arr.map((item) => {
            let invalidTime = new Date(item.invalid_date.replace(/-/g, '/')).getTime();
            item.statusText = nowTime > invalidTime ? '已过期' : '进行中';
            item.invalid_date = item.invalid_date.slice(0, 10).replace(/-/g, '.');
            item.valid_date = item.valid_date.slice(0, 10).replace(/-/g, '.');
          })
          if(data.pageIndex>1){
            channelLists = channelLists.concat(arr);
          }else{
            channelLists = arr;
          }
          this.setData({
            channelLists
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      complete: () => {

      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getChannelLists();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    this.setData({
      pageIndex:1
    })
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    let pageIndex = this.data.pageIndex;
    pageIndex++;
    this.setData({
      pageIndex
    })
    this.getChannelLists();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})