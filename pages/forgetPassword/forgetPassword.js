// pages/forgetPassword/forgetPassword.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cardId: '',
    phone: '',
    password: '',
    comfirnPsd: '',
    code: '',
    codeText: '获取验证码',
    isSendCode: false
  },
  sendCode() {
    const data = this.data;
    if (data.isSendCode) {
      return;
    }
    const ISPHONE = /^1[3456789]\d{9}$/;
    if (!ISPHONE.test(data.phone)) {
      wx.showToast({
        title: '请输入正确的手机号',
        duration: 1500
      })
      return;
    }
    wx.request({
      url: globalData.api.baseUrl + globalData.api.common.captcha + '+86' + data.phone,
      method: 'GET',
      success: (res) => {
        if (res.statusCode == 200) {

        } else {
          wx.showToast({
            title: res.data.message,
            duration: 2000,
            icon: 'none'
          })
        }
      },
      complete: () => {

      }
    })
    let time = 60;
    let inter = null;
    this.setData({
      isSendCode: true,
      codeText: '还剩60s'
    })
    inter = setInterval(() => {
      time--;
      this.setData({
        codeText: `还剩${time}s`
      })
      if (time == 0) {
        clearInterval(inter);
        this.setData({
          isSendCode: false,
          codeText: '获取验证码'
        })
      }
    }, 1000)
  },
  phoneIn(e) {
    this.setData({
      phone: e.detail.value
    })
  },
  passwordIn(e) {
    this.setData({
      password: e.detail.value
    })
  },
  codeIn(e) {
    this.setData({
      code: e.detail.value
    })
  },
  comfirnPsdIn(e) {
    this.setData({
      comfirnPsd: e.detail.value
    })
  },
  upatePassword() {
    const data = this.data;
    const ISPHONE = /^1[3456789]\d{9}$/;
    if (!ISPHONE.test(data.phone)) {
      wx.showToast({
        title: '请输入正确的手机号',
        duration: 2000
      })
      return;
    }
    if ( !data.password) {
      wx.showToast({
        title: '请输入密码',
        duration: 2000
      })
      return;
    }
    if (data.password !=data.comfirnPsd) {
      wx.showToast({
        title: '两次密码不一致',
        duration: 2000
      })
      return;
    }
    if ( !data.code) {
      wx.showToast({
        title: '请输入验证码',
        duration: 2000
      })
      return;
    }
    wx.showLoading({
      title: '提交中...',
    })
    if(data.cardId==1){
      wx.request({
        url: globalData.api.baseUrl + globalData.api.company.forgetpassword,
        method:'PUT',
        data:{
          "captcha": data.code,
          "password": data.password,
          "telephone": data.phone
        },
        success:(res)=>{
          wx.hideLoading();
          if(res.statusCode==200){
            wx.showToast({
              title: '修改成功',
            })
            wx.navigateBack({
              delta: 1,
            })
          }else{
            wx.showToast({
              title: res.data.message,
              duration:2000,
              icon:'none'
            })
          }
        },
        complete:()=>{
         
        },
        fail:(error)=>{
          wx.hideLoading();
          wx.showToast({
            title: error,
            duration: 2000,
            icon: 'none'
          })
        }
      })
      return;
    }
    if(data.cardId==3){
      wx.request({
        url: globalData.api.baseUrl + globalData.api.employee.forgetpassword,
        method:'PUT',
        data:{
          "captcha": data.code,
          "password": data.password,
          "telephone": data.phone
        },
        success:(res)=>{
          wx.hideLoading();
          if(res.statusCode==200){
            wx.showToast({
              title: '修改成功',
            })
            wx.navigateBack({
              delta: 1,
            })
          }else{
            wx.showToast({
              title: res.data.message,
              duration:2000,
              icon:'none'
            })
          }
        },
        complete:()=>{
         
        },
        fail:(error)=>{
          wx.hideLoading();
          wx.showToast({
            title: error,
            duration: 2000,
            icon: 'none'
          })
        }
      })
      return;
    }
    if(data.cardId==4){
      wx.request({
        url: globalData.api.baseUrl + globalData.api.user.forgetpassword,
        method:'PUT',
        data:{
          "captcha": data.code,
          "password": data.password,
          "telephone": data.phone
        },
        success:(res)=>{
          wx.hideLoading();
          if(res.statusCode==200){
            wx.showToast({
              title: '修改成功',
            })
            wx.navigateBack({
              delta: 1,
            })
          }else{
            wx.showToast({
              title: res.data.message,
              duration:2000,
              icon:'none'
            })
          }
        },
        complete:()=>{
          
        },
        fail:(error)=>{
          wx.showToast({
            title: error,
            duration: 2000,
            icon: 'none'
          })
        }
      })
      return;
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.setData({
      cardId: globalData.cardId
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})