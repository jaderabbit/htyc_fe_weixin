// pages/userEdu/userEdu.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    degree:'',
    school:'',
    major:'',
    startYear:'',
    endYear:'',
    degreeList: [],
    majorList: [],
    majorDetailList: [],
    majorShow: false,
    majorNum: 0,
    degreeShow: false,
    degreeNum: 0,
    majorId:0,
    degreeId:0,
    id:''
  },
  save(){
    const data = this.data;
    if(!data.school){
      wx.showToast({
        title: '请选择学校',
        icon:'none'
      })
      return;
    }
    if(!data.degree){
      wx.showToast({
        title: '请选择学历',
        icon:'none'
      })
      return;
    }
    if(!data.major){
      wx.showToast({
        title: '请选择专业',
        icon:'none'
      })
      return;
    }
    
    if(!data.startYear){
      wx.showToast({
        title: '请选择入学年份',
        icon:'none'
      })
      return;
    }
    let startYearTime = new Date(data.startYear.replace(/-/g,'/')).getTime();
    let birthdayTime = new Date(globalData.userInfo.birthday.replace(/-/g,'/')).getTime();
    if (startYearTime < birthdayTime){
      wx.showToast({
        title: '入学时间必须大于出生时间',
        icon: 'none'
      })
      return;
    }
    if(!data.endYear){
      wx.showToast({
        title: '请选择毕业年份',
        icon:'none'
      })
      return;
    }
    let method = '',postData=null;
    let  eduUrl = '';
    if (globalData.cardId == 3) {
      eduUrl = globalData.api.employee.edu;
    } else if (globalData.cardId == 4) {
      eduUrl = globalData.api.user.edu;
    }
    if(data.id){
      method='PUT';
      postData = {
        "admission_date": data.startYear+' 00:00:00',
        "degreeid": data.degreeId,
        "graduation_date": data.endYear+' 00:00:00',
        "id": data.id,
        "majorid": data.majorId,
        "school_name": data.school
      }
    }else{
      method = 'POST';
      postData = {
        "admission_date": data.startYear + ' 00:00:00',
        "degreeid": data.degreeId,
        "graduation_date": data.endYear + ' 00:00:00',
        "majorid": data.majorId,
        "school_name": data.school
      }
      if (globalData.cardId == 3) {
        postData.employeeid = globalData.userInfo.id;
      } else if (globalData.cardId == 4) {
        postData.userid = globalData.userInfo.id;
      }
    }
    wx.showLoading({
      title: '保存中...',
    })
    wx.request({
      url: globalData.api.baseUrl + eduUrl,
      method:method,
      data:postData,
      success:(res)=>{
        wx.hideLoading();
        if(res.statusCode==200){
          wx.showToast({
            title: '保存成功',
          })
          wx.navigateBack({
            delta: 1,
          })
        }else{
          wx.showToast({
            title: res.data.message,
            icon:'none'
          })
        }
      },
      fail:(error)=>{
        wx.hideLoading();
        wx.showToast({
          title: error,
          icon:'none'
        })
      }
    })
  },
  chooseSchool(){
    wx.navigateTo({
      url: '/pages/chooseSchool/chooseSchool',
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    })
  },
  startYearChange(e){
    this.setData({
      startYear:e.detail.value
    })
  },
  endYearChange(e){
    if (new Date(this.data.startYear.replace(/-/g, '/')).getTime() >= new Date(e.detail.value.replace(/-/g, '/')).getTime()){
      wx.showToast({
        title: '毕业年份不能小于入学年份',
        icon:'none'
      })
      return;
    }
    this.setData({
      endYear:e.detail.value
    })
  },
  degreeToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        degreeShow: true
      })
    } else {
      this.setData({
        degreeShow: false
      })
      this.setData({
        degree: this.data.degreeList[this.data.degreeNum].name,
        degreeId: this.data.degreeList[this.data.degreeNum].id,
        
      })
    }
  },
  degreeChange(e) {
    this.setData({
      degreeNum: e.detail.value[0]
    })
    this.setData({
      degree: this.data.degreeList[e.detail.value[0]].name,
      degreeId: this.data.degreeList[e.detail.value[0]].id
    })
  },
  majorToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        majorShow: true
      })
    } else {
      this.setData({
        majorShow: false
      })
      this.setData({
        major: this.data.majorDetailList[this.data.majorNum].name,
        majorId: this.data.majorDetailList[this.data.majorNum].id
      })
    }
  },
  majorChange(e) {
    this.getMajor(this.data.majorList[e.detail.value[0]].id);
    this.setData({
      majorNum: e.detail.value[1]
    })
    this.setData({
      major: this.data.majorDetailList[e.detail.value[1]].name,
      majorId: this.data.majorDetailList[e.detail.value[1]].id,
    })
  },
  getMajor(id) {
    wx.request({
      url: globalData.api.baseUrl + globalData.api.dict.majorList + id,
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            majorDetailList: res.data.result
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const id = options.id;
    if(id){
      wx.setNavigationBarTitle({
        title: '编辑教育经历',
      })
     try{
       let eduJson = wx.getStorageSync('edu');
       console.log(eduJson)
       if(eduJson){
         console.log(2)
        const edu = JSON.parse(eduJson);
         console.log(edu)
         this.setData({
           id:parseInt(id),
           school: edu.school_name,
           degree:edu.degree.name,
           major:edu.major.name,
           startYear: edu.admission_date.slice(0,10),
           endYear: edu.graduation_date.slice(0,10),
           degreeId:edu.degree.id,
           majorId:edu.major.id
         })
         wx.removeStorageSync('edu');
       }
     }catch(e){

     }
    }else{
      wx.setNavigationBarTitle({
        title: '新增教育经历',
      })
    }
    wx.request({
      url: globalData.api.baseUrl + globalData.api.dict.degreeList,
      method: 'GET',
      success: (res) => {
        if (res.statusCode == 200) {
          console.log(res)
          this.setData({
            degreeList: res.data.result
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      complete: () => {

      }
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.dict.majorclass,
      method: 'GET',
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            majorList: res.data.result
          })
          this.getMajor(res.data.result[0].id)
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      complete: () => {

      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    try{
      const school = wx.getStorageSync('school');
      if(school){
        this.setData({
          school
        })
        wx.removeStorageSync('school');
      }
    }catch(e){

    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})