// pages/personalResumeAssistant/personalResumeAssistant.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    updateMask: false,
    pageIndex: 1,
    pageSize: 10,
    resumes:[],
    currentResumeId:0,
    currentResumeNum:0,
    comments:''
  },
  commentsIn(e){
    this.setData({
      comments:e.detail.value
    })
  },
  closeUpdateMask(){
    this.setData({
      updateMask: false
    })
  },
  changeUpdateMask(e){
    this.setData({
      updateMask:true,
      currentResumeNum: e.currentTarget.dataset.num,
      currentResumeId:e.currentTarget.dataset.id
    })
  },
  update(){
    const data = this.data;
    if(!data.comments){
      wx.showToast({
        title: '请输入修改意见',
        duration:2000
      })
      return;
    }
    wx.showLoading({
      title: '发送中...',
    })
    wx.request({
      url: globalData.api.baseUrl+globalData.api.resume.update,
      method:'PUT',
      data:{
        "comments": data.comments,
        "resume_file": data.resumes[data.currentResumeNum].resume_file,
        "resumeid":data.currentResumeId
      },
      success:(res)=>{
        wx.hideLoading();
        if(res.statusCode==200){
          wx.showToast({
            title: '发送成功',
          })
          this.setData({
            updateMask:false
          })
        }else{
          wx.showToast({
            title: res.data.message,
            duration:2000,
            icon:'none'
          })
        }
      },
      complete:()=>{
       
      },fail:(error)=>{
        wx.hideLoading();
        wx.showToast({
          title: error,
          duration: 2000,
          icon: 'none'
        })
      }
    })
  },
  checkUpdateDetail(e) {
    wx.navigateTo({
      url: '/pages/resumeUpdateDetail/resumeUpateDetail?id='+e.currentTarget.dataset.id,
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    })
  },
  uploadResume() {
    wx.navigateTo({
      url: '/pages/addResume/addResume',
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    })
  },
  getResume(){
    const data = this.data;
    wx.request({
      url: `${globalData.api.baseUrl}${globalData.api.resume.list}${data.pageIndex}/${data.pageSize}/${globalData.userInfo.id}`,
      success: (res) => {
        if (res.statusCode == 200) {
          const arr = res.data.result.data;
          let resumes = this.data.resumes;
          resumes = resumes.concat(arr);
          this.setData({
            resumes
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      complete: () => {

      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.getResume();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {
    this.setData({
      pageIndex:1
    })
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    let pageIndex = this.data.pageIndex;
    pageIndex++;
    this.setData({
      pageIndex
    })
    this.getResume();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})