// pages/chooseCompany/chooseCompany.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    key: '',
    company:null,
    isShow:true
  },
  comfrin() {
    const data = this.data;
    if(!data.company){
      wx.showToast({
        title: '请选择公司',
        icon:'none'
      })
      return;
    }
    wx.request({
      url: globalData.api.baseUrl + globalData.api.user.convertemployee,
      method:'Post',
      data:{
        companyid:data.company.id,
        telephone:globalData.userInfo.telephone
      },
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      success: (res) => {
        if (res.statusCode == 200) {
          if (res.data.result) {
            globalData.cardId = 3;
            globalData.userInfo = res.data.result;
            let loginTime = new Date().getTime();
            try {
              wx.setStorageSync('cardId', 3);
              wx.setStorageSync('userInfo', JSON.stringify(res.data.result));
              wx.setStorageSync('loginTime', loginTime);
            } catch (e) {

            }

            wx.redirectTo({
              url: '/pages/staffCommu/staffCommu',
              success: function (res) { },
              fail: function (res) { },
              complete: function (res) { },
            })
          } 
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      fail: (error) => {
        wx.showToast({
          title: error,
          icon: 'none'
        })
      }
    })
  },
  dele() {
    this.setData({
      list: [],
      key: ''
    })
  },
  choose(e) {
    const num = e.currentTarget.dataset.num;
    const company = this.data.list[num];
    this.setData({
      company,
      key: company.name,
      isShow:false
    })
  },
  keyIn(e) {
    const key = e.detail.value;
    this.setData({
      key
    })
    if (key) {
      wx.request({
        url: globalData.api.baseUrl + globalData.api.company.searchKey + key + '?pageIndex=1&pageSize=100',
        success: (res) => {
          if (res.statusCode == 200) {
            this.setData({
              isShow:true
            })
            this.setData({
              list: res.data.result.data
            })
          } else {
            wx.showToast({
              title: res.data.message,
              icon: 'none'
            })
          }
        },
        fail: (error) => {
          wx.showToast({
            title: error,
            icon: 'none'
          })
        }
      })
    }
    setTimeout(() => {

    }, 20)
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})