// pages/companyMy/companyMy.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    roleMaskShow:false,
    isInner:false,
    roleNum: 0,
    percentcomplete: 0,
    num: 0,
    id:'',
    noreadcount:0,
    epmNum:0
  },
  roleChange(e) {
    const roleNum = e.detail.value[0];
    this.setData({
      roleNum
    })
  },
  roleChangeComfirn() {
    this.setData({
      roleMaskShow: false
    })
    const roleNum = this.data.roleNum;
    if (roleNum == 0) {
      wx.redirectTo({
        url: '/pages/companyHome/companyHome',
        success: function (res) { },
        fail: function (res) { },
        complete: function (res) { },
      })
    }else{
      wx.redirectTo({
        url: '/pages/recruit/recruit',
        success: function (res) { },
        fail: function (res) { },
        complete: function (res) { },
      })
    }
    
    
  },
  roleChooseShow(){
    this.setData({
      roleMaskShow:true
    })
  },
  roleChooseHide() {
    this.setData({
      roleMaskShow: false
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      isInner:globalData.isInner,
      id:globalData.userInfo.id
    })
    wx.request({
      url: `${globalData.api.baseUrl}${globalData.api.company.role}/${globalData.userInfo.id}`,
      success:(res)=>{
        if(res.statusCode==200){
          console.log(res)
        }else{
          wx.showToast({
            title: res.data.message,
            icon:'none'
          })
        }
      },
      complete:()=>{
        
      }
    })
    const num = Math.round(globalData.userInfo.exposure_degree / 100 * 17);
    this.setData({
      num,
      percentcomplete: globalData.userInfo.exposure_degree
    })
    if(!this.data.isInner){
      wx.request({
        url: globalData.api.baseUrl + globalData.api.message.noreadcount + '?msgType=company' + '&accountid=' + globalData.userInfo.id,
        success: (res) => {
          if (res.statusCode == 200) {
            this.setData({
              noreadcount: res.data.result
            })
          }
        }
      })
    }else{
      wx.request({
        url: globalData.api.baseUrl + globalData.api.channel.companyusernoreadcount + '?companyid=' + globalData.userInfo.id,
        success: (res) => {
          if (res.statusCode == 200) {
            this.setData({
              noreadcount: res.data.result
            })
          }
        }
      })
    }
   
    wx.request({
      url: globalData.api.baseUrl + globalData.api.employee.number + '?companyid=' + globalData.userInfo.id +'&status='+1,
      success:(res)=>{
        if(res.statusCode==200){
          this.setData({
            epmNum:res.data.result
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  
})