// pages/comMsgDetail/comMsgDetail.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    job: {},
    result: []
  },
  toChat() {
    const data = this.data.result[0];
    if (!data) {
      return;
    }
    wx.request({
      url: globalData.api.baseUrl + globalData.api.job.candidate,
      method: 'PUT',
      data: {
        userid: data.userid,
        jobid: data.jobid,
        status: 1
      },
      header: {
        'content-type': 'application/x-www-form-urlencoded' 
      },
      success: (res) => {
        if (res.statusCode == 200) {
          wx.navigateTo({
            url: `/pages/messageDetail/messageDetail?type=${2}&user=${data.userid}&company=${data.companyid}`,
            success: function(res) {},
            fail: function(res) {},
            complete: function(res) {},
          })
        }else{
          wx.showToast({
            title: res.data.message,
            icon:'none'
          })
        }
      },
      fail:(error)=>{
        wx.showToast({
          title: error,
          icon:'none'
        })
      }
    })

  },
  getJob(id) {
    wx.request({
      url: globalData.api.baseUrl + globalData.api.job.add + id,
      success: (res) => {
        if (res.statusCode == 200) {
          const job = res.data.result;
          job.valid_startdate = job.valid_startdate.slice(0, 10).replace(/-/g, '.');
          job.valid_startdate = job.valid_enddate.slice(0, 10).replace(/-/g, '.');
          this.setData({
            job
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const jobid = options.jobid;
    const userid = options.userid;
    wx.request({
      url: `${globalData.api.baseUrl}${globalData.api.job.procedureResult}${jobid}/${userid}`,
      success: (res) => {
        if (res.statusCode == 200) {
          const result = res.data.result;
          this.getJob(result[0].jobid);
          result.map(item => {
            item.finished_time = item.finished_time.slice(0, 19).replace(/-/g, '.');
          })
          this.setData({
            result
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      fail: (error) => {
        wx.showToast({
          title: error,
          icon: 'none'
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})