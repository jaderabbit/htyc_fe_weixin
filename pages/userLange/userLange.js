// pages/userLange/userLange.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    typeArr:['英语','日语','法语','西班牙语','德语','俄语'],
    typeShow:false,
    type:'',
    typeNum:0,
    ability:[{name:'一般',id:1},{name:'良好',id:2},{name:'熟悉',id:3},{name:'精通',id:4}],
    hearShow:false,
    hear:'',
    hearNum:0,
    readShow:false,
    read:'',
    readNum:0,
    id:''
  },
  typeIn(e){
    this.setData({
      type:e.detail.value
    })
  },
  typeToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        typeShow: true
      })
    } else {
      this.setData({
        typeShow: false
      })
      this.setData({
        type: this.data.typeArr[this.data.typeNum],
      })
    }
  },
  typeChange(e) {
    this.setData({
      typeNum: e.detail.value[0]
    })
  },
  hearToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        hearShow: true
      })
    } else {
      this.setData({
        hearShow: false
      })
      this.setData({
        hear: this.data.ability[this.data.hearNum].name,
      })
    }
  },
  hearChange(e) {
    this.setData({
      hearNum: e.detail.value[0]
    })
  },
  readToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        readShow: true
      })
    } else {
      this.setData({
        readShow: false
      })
      this.setData({
        read: this.data.ability[this.data.readNum].name,
      })
    }
  },
  readChange(e) {
    this.setData({
      readNum: e.detail.value[0]
    })
  },
  save() {
    const data = this.data;
    if (!data.type) {
      wx.showToast({
        title: '请选择语种',
        icon: 'none'
      })
      return;
    }
    if (!data.hear) {
      wx.showToast({
        title: '请选择听说能力',
        icon: 'none'
      })
      return;
    }
    if (!data.read) {
      wx.showToast({
        title: '请选择读写能力',
        icon: 'none'
      })
      return;
    }
    let method = '', postData = null, langUrl='';
    if (globalData.cardId == 3) {
      langUrl = globalData.api.employee.lang;
    } else if (globalData.cardId == 4) {
      langUrl = globalData.api.user.lang;
    }
    if (data.id) {
      method = 'PUT';
      postData = {
        "hear": data.ability[data.hearNum].id,
        "id": data.id,
        "language": data.type,
        "read_write": data.ability[data.readNum].id
      }
    } else {
      method = 'POST';
      postData = {
        "hear": data.ability[data.hearNum].id,
        "language": data.type,
        "read_write": data.ability[data.readNum].id,
      }
      if (globalData.cardId == 3) {
        postData.employeeid = globalData.userInfo.id;
      } else if (globalData.cardId == 4) {
        postData.userid = globalData.userInfo.id;
      }
    }
    wx.showLoading({
      title: '保存中...',
    })
    wx.request({
      url: globalData.api.baseUrl + langUrl,
      method: method,
      data: postData,
      success: (res) => {
        wx.hideLoading();
        if (res.statusCode == 200) {
          wx.showToast({
            title: '保存成功',
          })
          wx.navigateBack({
            delta: 1,
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      fail: (error) => {
        wx.hideLoading();
        wx.showToast({
          title: error,
          icon: 'none'
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const id = options.id;
    if (id) {
      wx.setNavigationBarTitle({
        title: '编辑语言能力',
      })
      try {
        let langeJson = wx.getStorageSync('lange');
        if (langeJson) {
          const lange = JSON.parse(langeJson);
          let hearNum = this.data.hearNum;
          let readNum = this.data.readNum;
          this.data.ability.map((item,index)=>{
            if(item.name==lange.hear){
              hearNum = index;
            }
            if (item.name == lange.read_write){
              readNum = index;
            }
          })
          this.setData({
            hearNum,
            readNum,
            id:parseInt(id),
            type: lange.language,
            hear: lange.hear,
            read: lange.read_write
          })
          wx.removeStorageSync('lange');
        }
      } catch (e) {

      }
    } else {
      wx.setNavigationBarTitle({
        title: '添加语言能力',
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})