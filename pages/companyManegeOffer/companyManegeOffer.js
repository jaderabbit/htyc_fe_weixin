// pages/companyManegeOffer/companyManegeOffer.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    offers:[],
    pageIndex:1,
    pageSize:10,
    time: ''
  },
  toDetail(e){
    const num = e.currentTarget.dataset.num;
    try{
      wx.setStorageSync('offer', JSON.stringify(this.data.offers[num]));
    }catch(e){

    }
    wx.navigateTo({
      url: '/pages/offerDetail/offerDetail',
    })
  },
  getList(){
    const data = this.data;
    wx.request({
      url: `${globalData.api.baseUrl}${globalData.api.offer.company}${globalData.userInfo.id}?pageIndex=${data.pageIndex}&pageSize=${data.pageSize}`,
      success: (res) => {
        if (res.statusCode == 200) {
          const arr = res.data.result.data;
          let offers = data.offers;
          if(arr){
            offers = offers.concat(arr);
          }
          this.setData({
            offers
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getList();
    const date = new Date();
    let month = date.getMonth() + 1;
    month = month >= 10 ? month : '0' + month;
    let datey = date.getDate();
    datey = datey >= 10 ? datey : '0' + datey;
    let day = date.getDay();
    const arr = ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六']
    const time = `${month}月${datey}日  ${arr[day]}`;
    this.setData({
      time
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    let pageIndex = this.data.pageIndex;
    pageIndex++;
    this.setData({
      pageIndex
    })
    this.getList();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})