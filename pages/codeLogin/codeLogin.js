// pages/codeLogin/codeLogin.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    companyId: 0,
    examId: 0
  },
  toRegister() {
    globalData.cardId = 3;
    wx.setStorageSync('cardId', 3);
    wx.navigateTo({
      url: '/pages/conpanyRegister/conpanyRegister',
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const companyId = options.companyId;
    const examId = options.examId;
    this.setData({
      companyId,
      examId: examId||''
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },
  getPhoneNumber(e) {
    wx.login({
      success: res => {
        console.log(res);
        console.log(e);
        wx.request({
          url: `${globalData.api.baseUrl}${globalData.api.thirdapplogin.usermobilephone}`,
          method: 'POST',
          data: {
            code: res.code,
            encryptedData: e.detail.encryptedData,
            iv: e.detail.iv
          },
          header: {
            'content-type': 'application/x-www-form-urlencoded'
          },
          success: (res) => {
            if (res.statusCode == 200) {
              const result = res.data.result;
              console.log(result);
              globalData.cardId = 3;
              wx.setStorageSync('cardId', 3);
              if (result.employee && result.employee.companyid == this.data.companyId) {
                wx.navigateTo({
                  url: `/pages/login/login?companyId=${this.data.companyId}&examId=${this.data.examId}`,
                  success: function(res) {},
                  fail: function(res) {},
                  complete: function(res) {},
                })
              } else {
                wx.showToast({
                  title: '您还未注册为该公司员工，请先注册',
                  icon: 'none',
                  duration: 2000
                })
                setTimeout(() => {
                  wx.navigateTo({
                    url: `/pages/conpanyRegister/conpanyRegister?companyId=${this.data.companyId}&examId=${this.data.examId}`,
                    success: function(res) {},
                    fail: function(res) {},
                    complete: function(res) {},
                  })
                }, 2000)

              }
            } else {
              wx.showToast({
                title: res.data.message,
                icon: 'none'
              })
            }
          },
          fail: (error) => {
            wx.showToast({
              title: error,
              icon: 'none'
            })
          }
        })
      }
    })
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})