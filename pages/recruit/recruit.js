// pages/recruit/recruit.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    pageIndex: 1,
    pageSize: 10,
    noreadcount:0,
    hintShow:false
  },
  hintHide(){
    this.setData({
      hintShow:false
    })
    
  },
 createComparisonFunction(propertyName) {
    return function (object1, object2) {
      var value1 = object1[propertyName];
      var value2 = object2[propertyName];
      if (value1 > value2) {
        return -1;
      } else if (value1 < value2) {
        return 1;
      } else {
        return 0;
      }
    };
  },
  checkApplicantInfo(e) {
    const id = e.currentTarget.dataset.id;
    const num = e.currentTarget.dataset.num;
    const jobid = e.currentTarget.dataset.jobid;
    try {
      wx.setStorageSync('user', JSON.stringify(this.data.list[num].user));
      wx.navigateTo({
        url: `/pages/applicantInfo/applicantInfo?id=${id}&jobid=${jobid}`,
        success: function(res) {},
        fail: function(res) {},
        complete: function(res) {},
      })
    } catch (e) {

    }

  },
  agree(e) {
    const id = e.currentTarget.dataset.id;
    const action = e.currentTarget.dataset.action;
    const num = e.currentTarget.dataset.num;
    wx.request({
      url: `${globalData.api.baseUrl}${globalData.api.job.jobSet}?id=${id}&action=${action}`,
      method: 'PUT',
      success: (res) => {
        if (res.statusCode == 200) {

          wx.showToast({
            title: '操作成功',
          })
          const list = this.data.list;
          list.splice(num, 1);
          this.setData({
            list
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      complete: () => {

      }
    })
  },
  getItem() {
    const data = this.data;
    wx.request({
      url: `${globalData.api.baseUrl}${globalData.api.job.recommendcompany}${data.pageIndex}/${data.pageSize}/${globalData.userInfo.id}/${true}/${false}`,
      success: (res) => {
        if (res.statusCode == 200) {
          let arr = res.data.result.data;
          arr.sort(this.createComparisonFunction('company_match_percent'));
          let list = this.data.list;
           arr.map((item) => {
            item.user.name = item.user.name ? (!item.company_action ? item.user.name.substring(0, 1) + '**' : item.user.name) : '暂无';
            if (item.user.birthday) {
              let birthYear = parseInt(item.user.birthday.slice(0, 4));
              let year = new Date().getFullYear();
              item.user.age = year - birthYear  + '岁';
            } else {
              item.user.age = '暂无'
            }
            return item;
          })
          if(data.pageIndex==1&&((arr&&arr.length==0)||!arr)){
            this.setData({
              hintShow:true
            })
          }
          list = list.concat(arr);
          this.setData({
            list: list
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      fail: (error) => {
        wx.showToast({
          title: error,
          icon: 'none'
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.getItem();
    wx.request({
      url: globalData.api.baseUrl + globalData.api.message.noreadcount + '?msgType=company' + '&accountid=' + globalData.userInfo.id,
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            noreadcount: res.data.result
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
   let pageIndex = this.data.pageIndex;
   pageIndex++;
   this.setData({
     pageIndex
   })
   this.getItem();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})