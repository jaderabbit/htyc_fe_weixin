// pages/commuDetailList/commuDetailList.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    name: '',
    empname: '',
    channel: []
  },
  toChat(e) {
    const channelid = e.currentTarget.dataset.channelid;
    const employeeid = e.currentTarget.dataset.employeeid;
    if (channelid) {
      wx.navigateTo({
        url: '/pages/messageDetail/messageDetail?employeeid=' + employeeid + '&channelid=' + channelid + '&type=2',
      })
    }

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const employeeid = options.employeeid;
    const channelid = options.channelid;
    const name = options.name;
    const empname = options.empname;
    wx.request({
      url: globalData.api.baseUrl + globalData.api.channel.employee + '/' + channelid + '/' + employeeid,
      success: (res) => {
        if (res.statusCode == 200) {
          const arr = res.data.result;
          arr.map(item => {
            item.lastmodified = item.lastmodified.replace(/-/g, '.');
          })
          this.setData({
            channel: arr,
            name,
            empname
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      complete: () => {

      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})