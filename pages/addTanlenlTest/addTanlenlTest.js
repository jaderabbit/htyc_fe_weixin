// pages/addTanlenlTest/addTanlenlTest.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageIndex: 1,
    pageSize: 10,
    ques: [],
    queId: ''
  },
  add() {
    const status = globalData.userInfo.status;
    let title = '';
    switch (status) {
      case 0:
        title = '您的企业正在审核中，请耐心等待';
        break;
      case 1:
        title = '您的企业审核未通过';
        break;
      case 4:
        title = '您的企业已被临时封号';
        break;
      case 5:
        title = '您的企业已被永久封号';
        break;
      case 6:
        title = '您的企业已被删除';
        break;
      default:
        title = '';
        break;
    }
    if (title) {
      wx.showToast({
        title: title,
        icon: 'none'
      })
      return;
    }
    try {
      const testJson = wx.getStorageSync('TEST');
      if (testJson) {
        const test = JSON.parse(testJson);
        const data = this.data;
        if(!data.queId){
          wx.showToast({
            title: '请选择测试题库',
            duration:2000
          })
          return;
        }
        wx.showLoading({
          title: '新增中...',
        })
        wx.request({
          url: globalData.api.baseUrl+globalData.api.exam.add,
          method:'POST',
          data:{
            "companyid": globalData.userInfo.id,
            "examid": data.queId,
            "invalid_date": test.endDate,
            "name": test.name,
            "scope": test.scope,
            "valid_date": test.startDate
          },
          success:(res)=>{
            wx.hideLoading();
            if(res.statusCode==200){
              wx.showToast({
                title: '新增成功',
              })
              try{
                wx.removeStorageSync('TEST');
              }catch(e){

              }
              wx.navigateBack({
                delta: 2,
              })
            }else{
              wx.showToast({
                title: res.data.message,
                duration:2000,
                icon:'none'
              })
            }
          },
          complete:()=>{
           
          },
          fail:(error)=>{
            wx.hideLoading();
            wx.showToast({
              title: error,
              duration: 2000,
              icon: 'none'
            })
          }
        })
      }
    } catch (e) {

    }
  },
  chooseItem(e) {
    this.setData({
      queId: e.currentTarget.dataset.id
    })
  },
  toTestDetail(e) {
    wx.navigateTo({
      url: '/pages/questionDetail/questionDetail?id='+e.currentTarget.dataset.id,
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const data = this.data;
    wx.request({
      url: `${globalData.api.baseUrl}${globalData.api.exam.paperList}${data.pageIndex}/${data.pageSize}`,
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            ques: res.data.result.data
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon:'none'
          })
        }
      },
      complete: () => {

      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})