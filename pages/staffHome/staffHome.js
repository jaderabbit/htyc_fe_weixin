// pages/staffHome/staffHome.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tests: [],
    pageIndex: 1,
    pageSize: 10,
    noreadcount:0
  },
  answer(e) {
    const examid = e.currentTarget.dataset.examid;
    const name = e.currentTarget.dataset.name;
    const isexam = e.currentTarget.dataset.isexam;
    if (isexam) {
      wx.navigateTo({
        url: '/pages/answerResult/answerResult?examid=' + examid + '&name=' + name,
        success: function (res) { },
        fail: function (res) { },
        complete: function (res) { },
      })

    } else {
      wx.navigateTo({
        url: '/pages/answerQuestions/answerQuestions?examid=' + examid + '&name=' + name,
        success: function(res) {},
        fail: function(res) {},
        complete: function(res) {},
      })
    }

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const data = this.data;
    wx.request({
      url: `${globalData.api.baseUrl}${globalData.api.exam.employeeallexamlist}${data.pageIndex}/${data.pageSize}/${globalData.userInfo.id}`,
      success: (res) => {
        if (res.statusCode == 200) {
          const tests = res.data.result.data;
          tests.map((item) => {
            item.invalid_date = item.invalid_date ? item.invalid_date.slice(0, 10).replace(/-/g, '.') : item.invalid_date;
            item.valid_date = item.valid_date ? item.valid_date.slice(0, 10).replace(/-/g, '.') : item.valid_date;
          })
          this.setData({
            tests
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      fail: (error) => {
        wx.showToast({
          title: error,
          icon: 'none'
        })
      }
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.message.noreadcount + '?msgType=employee' + '&accountid=' + globalData.userInfo.id,
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            noreadcount: res.data.result
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})