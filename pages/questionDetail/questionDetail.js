// pages/questionDetail/questionDetail.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    exam:{}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const id = options.id;
    if(id){
      wx.request({
        url: globalData.api.baseUrl + globalData.api.exam.paperDetail+id,
        success:(res)=>{
          if(res.statusCode==200){
            this.setData({
              exam:res.data.result
            })
          }else{
            wx.showToast({
              title: res.data.message,
              icon:'none'
            })
          }
        },
        fail:(error)=>{
          wx.showToast({
            title: error,
            icon:'none'
          })
        }
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})