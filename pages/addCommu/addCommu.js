// pages/addCommu/addCommu.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    endDate:'',
    startDate:'',
    name:'',
    department:'全公司',
    departmentShow:false,
    departmentList:[],
    departmentNum:0,
    description:''
  },
  descriptionIn(e){
    this.setData({
      description:e.detail.value
    })
  },
  departmentToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        departmentShow: true
      })
    } else {
      this.setData({
        departmentShow: false
      })
      this.setData({
        department: this.data.departmentList[this.data.departmentNum].name
      })
    }
  },
  departmentChange(e) {
    this.setData({
      departmentNum: e.detail.value[0]
    })
    this.setData({
      department: this.data.departmentList[e.detail.value[0]].name
    })
  },
  nameIn(e){
    this.setData({
      name: e.detail.value
    })
  },
  departmentIn(e){
    this.setData({
      department: e.detail.value
    })
  },
  endDateChange(e){
   if(new Date(e.detail.value.replace(/-/g, '/')).getTime() <= new Date(this.data.startDate.replace(/-/g, '/')).getTime()){
     wx.showToast({
       title: '结束日期必须大于开始日期',
       icon:'none',
       duration:2000
     })
     return;
   }
    this.setData({
      endDate:e.detail.value
    })
  },
  startDateChange(e){
    this.setData({
      startDate: e.detail.value
    })
  },
  add(){
    const status = globalData.userInfo.status;
    let title = '';
    switch (status) {
      case 0:
        title = '您的企业正在审核中，请耐心等待';
        break;
      case 1:
        title = '您的企业审核未通过';
        break;
      case 4:
        title = '您的企业已被临时封号';
        break;
      case 5:
        title = '您的企业已被永久封号';
        break;
      case 6:
        title = '您的企业已被删除';
        break;
      default:
        title = '';
        break;
    }
    if (title) {
      wx.showToast({
        title: title,
        icon: 'none'
      })
      return;
    }
    const data = this.data;
    if(!data.name){
      wx.showToast({
        title: '请输入主题',
        duration:1500
      })
      return;
    }
    if(!data.department){
      wx.showToast({
        title: '请输入参加部门',
        duration:1500
      })
      return;
    }
    if(!data.startDate){
      wx.showToast({
        title: '请选择开始日期',
        duration:1500
      })
      return;
    }
    if(!data.endDate){
      wx.showToast({
        title: '请选择结束日期',
        duration:1500
      })
      return;
    }
    if (!data.description){
      wx.showToast({
        title: '请输入沟通主题说明',
        icon:'none'
      })
      return;
    }
    wx.showLoading({
      title: '新增中...',
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.channel.add,
      method:'POST',
      data:{
        "companyid": globalData.userInfo.id,
        "invalid_date": data.endDate+' 00:00:00',
        "scope": data.department,
        "subject": data.name,
        "valid_date": data.startDate +' 00:00:00',
        "description": data.description,
      },
      success:(res)=>{
        wx.hideLoading();
        if(res.statusCode==200){
          wx.showToast({
            title: '新增成功',
          })
          wx.navigateBack({
            delta: 1,
          })
        }else{
          wx.showToast({
            title: res.data.message,
            duration:2000,
            icon:'none'
          })
        }
      },
      fail:(error)=>{
        wx.hideLoading();
        wx.showToast({
          title: error,
          duration: 2000,
          icon: 'none'
        })
      },
      complete:()=>{
       
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.request({
      url: globalData.api.baseUrl + globalData.api.department.list + '1/100/' + globalData.userInfo.id,
      method: 'GET',
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            departmentList: res.data.result.data
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      complete: () => {

      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})