// pages/department/department.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    pageIndex: 1,
    pageSize: 10,
    delBtnWidth: 108,
    startX: "",
    startY:''
  },
  touchS: function(e) {
    if (e.touches.length == 1) {
      this.setData({
        //设置触摸起始点水平方向位置
        startX: e.touches[0].clientX,
        startY: e.touches[0].clientY,
      });
    }
  },
  touchM: function(e) {
    if (e.touches.length == 1) {
      //手指移动时水平方向位置
      var moveX = e.touches[0].clientX;
      var moveY = e.touches[0].clientY;
      //手指起始点位置与移动期间的差值
      var disX = this.data.startX - moveX;
      var disY = this.data.startY - moveY;
      if(Math.abs(disY)>Math.abs(disX)){
        return;
      }
      var delBtnWidth = this.data.delBtnWidth;
      var txtStyle = "";
      if (disX == 0 || disX < 0) { //如果移动距离小于等于0，说明向右滑动，文本层位置不变
        txtStyle = "left:0px";
      } else if (disX > 0) { //移动距离大于0，文本层left值等于手指移动距离
        txtStyle = "left:-" + disX + "px";
        if (disX >= delBtnWidth) {
          //控制手指移动距离最大值为删除按钮的宽度
          txtStyle = "left:-" + delBtnWidth + "px";
        }
      }
      //获取手指触摸的是哪一项
      var index = e.currentTarget.dataset.index;
      var list = this.data.list;
      list[index].txtStyle = txtStyle;
      //更新列表的状态
      this.setData({
        list: list
      });
    }
  },
  touchE: function(e) {
    if (e.changedTouches.length == 1) {
      //手指移动结束后水平位置
      var endX = e.changedTouches[0].clientX;
      var endY = e.changedTouches[0].clientY;
      //触摸开始与结束，手指移动的距离
      var disX = this.data.startX - endX;
      var disY = this.data.startY - endY;
      if (Math.abs(disY) > Math.abs(disX)){
        return;
      }
      var delBtnWidth = this.data.delBtnWidth;
      //如果距离小于删除按钮的1/2，不显示删除按钮
      var txtStyle = disX > delBtnWidth / 2 ? "left:-" + delBtnWidth + "px" : "left:0px";
      //获取手指触摸的是哪一项
      var index = e.currentTarget.dataset.index;
      var list = this.data.list;
      list[index].txtStyle = txtStyle;
      //更新列表的状态
      this.setData({
        list: list
      });
    }
  },
  //获取元素自适应后的实际宽度
  getEleWidth: function(w) {
    var real = 0;
    try {
      var res = wx.getSystemInfoSync().windowWidth;
      var scale = (750 / 2) / (w / 2);
      real = Math.floor(res / scale);
      return real;
    } catch (e) {
      return false;
      // Do something when catch error
    }
  },
  initEleWidth: function() {
    var delBtnWidth = this.getEleWidth(this.data.delBtnWidth);
    this.setData({
      delBtnWidth: delBtnWidth
    });
  },
  //点击删除按钮事件
  delItem: function(e) {
    //获取列表中要删除项的下标
    var index = e.currentTarget.dataset.index;
    var list = this.data.list;
    wx.request({
      url: globalData.api.baseUrl + globalData.api.department.edit + list[index].id,
      method: 'DELETE',
      success: (res) => {
        if (res.statusCode == 200) {
          //移除列表中下标为index的项
          list.splice(index, 1);
          //更新列表的状态
          this.setData({
            list: list
          });
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      fail: (error) => {
        wx.showToast({
          title: error,
          icon: 'none'
        })
      }
    })

  },
  edit(e) {
    const num = e.currentTarget.dataset.num;
    if (num || num == 0) {
      const dep = this.data.list[num];
      wx.navigateTo({
        url: '/pages/addDepartment/addDepartment?name=' + dep.name + '&id=' + dep.id,
      })
    } else {
      wx.navigateTo({
        url: '/pages/addDepartment/addDepartment',
      })
    }
  },
  getList() {
    const data = this.data;
    wx.request({
      url: `${globalData.api.baseUrl}${globalData.api.department.list}${data.pageIndex}/${data.pageSize}/${globalData.userInfo.id}`,
      success: (res) => {
        if (res.statusCode == 200) {
          const arr = res.data.result.data;
          let list = this.data.list;
          if(arr){
            arr.map(item => {
              item.txtStyle = '';
            })
            if(data.pageIndex>1){
              list = list.concat(arr);
            }else{
              list = arr;
            }
           
          }
          this.setData({
            list
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    this.initEleWidth();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.getList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {
    this.setData({
      pageIndex:1
    })
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    let pageIndex = this.data.pageIndex;
    pageIndex++;
    this.setData({
      pageIndex
    })
    this.getList();
  },  

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})