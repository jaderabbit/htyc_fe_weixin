// pages/jobApplyHistory/jobApplyHistory.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    action: true,
    istoday: true,
    pageIndex: 1,
    pageSize: 10,
    type:1,
    jobs:[],
    revoke:''
  },
  undo(e){
    const id = e.currentTarget.dataset.id;
    const num = e.currentTarget.dataset.num;
    const jobs = this.data.jobs;
    wx.request({
      url: globalData.api.baseUrl + globalData.api.job.revoke,
      method:'PUT',
      data:{
        id:id
      },
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      success:(res)=>{
        if(res.statusCode==200){
          jobs.splice(num,1);
          this.setData({
            jobs
          })
        }
      }
    })
  },
  changeNav(e) {
    const action = e.currentTarget.dataset.action;
    const istoday = e.currentTarget.dataset.istoday;
    const type = e.currentTarget.dataset.type;
    const revoke = e.currentTarget.dataset.revoke;
    this.setData({
      action,
      istoday,
      type,
      pageIndex:1
    })
    if(revoke){
      this.setData({
        revoke
      })
    }else{
      this.setData({
        revoke:''
      })
    }
    this.applyJob();
  },
  applyJob() {
    const data = this.data;
    let url = `${globalData.api.baseUrl}${globalData.api.job.userApplyList}${data.pageIndex}/${data.pageSize}/${globalData.userInfo.id}/${data.action}/${data.istoday}`;
    if(data.revoke){
      url = `${globalData.api.baseUrl}${globalData.api.job.userApplyList}${data.pageIndex}/${data.pageSize}/${globalData.userInfo.id}/${data.action}/${data.istoday}?revoke_action=${data.revoke}`
    }
    wx.request({
      url:url,
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            jobs:res.data.result.data
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon:'none'
          })
        }
      },
      complete: () => {

      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.applyJob();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    let pageIndex = this.data.pageIndex;
    pageIndex++;
    this.setData({
      pageIndex
    })
    this.applyJob();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})