// pages/userAnswerQue/userAnswerQue.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    job: {},
    userid: '',
    procedures: [],
    pindex: '',
    tindex: ''
  },
  answer(e) {
    const pindex = e.currentTarget.dataset.pindex;
    const tindex = e.currentTarget.dataset.tindex;
    const val = e.detail.value;
    const procedures = this.data.procedures;
    procedures[pindex].tasks[tindex].result = val;
    this.setData({
      pindex,
      tindex,
      procedures
    })
  },
  add() {
    const data = this.data;
    let now = new Date();
    let hour = now.getHours();
    hour = hour > 10 ? hour : '0' + hour;
    let minute = now.getMinutes();
    minute = minute > 10 ? hour : '0' + minute;
    let second = now.getSeconds();
    second = second > 10 ? second : '0' + second;
    let time = now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + now.getDate() + ' ' + hour + ':' + minute + ':' + second;
    let isComlete = false;
    data.procedures.map(item => {
      isComlete = !item.tasks.some(item => {
        return item.result == '';
      })
    })
    if(!isComlete){
      wx.showToast({
        title: '请回答所有问题',
        icon:'none'
      })
      return;
    }
    data.procedures.map(item => {
     item.tasks.map(item=>{
       item.finished_time = time;
     })
    })
    wx.showLoading({
      title: '新增中',
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.job.procedureResult,
      method:'POST',
      data:{
        "jobid": data.job.id,
        "procedures": data.procedures,
        "userid": data.userid
      },
      success:(res)=>{
        wx.hideLoading();
        if(res.statusCode==200){
          wx.navigateBack({
            delta: 1,
          })
        }else{
          wx.showToast({
            title: res.data.message,
            icon:'none'
          })
        }
      },
      fail:(error)=>{
        wx.hideLoading();
        wx.showToast({
          title: error,
          icon:'none'
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const userid = parseInt(options.userid) ;
    const job = JSON.parse(wx.getStorageSync('taskQue'));
    this.setData({
      userid,
      job
    })
    const produces = [];
    this.data.job.produces.map(item => {
      let produce = {};
      let tasks = []
      produce.procedureid = item.id;
      item.tasks.map(item => {
        let task = {};
        task.finished_time = '';
        task.result = '';
        task.taskid = item.id;
        tasks.push(task);
      })
      produce.tasks = tasks;
      produces.push(produce);
    })
    this.setData({
      procedures:produces
    })
    wx.removeStorageSync('taskQue');
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})