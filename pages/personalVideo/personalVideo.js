// pages/personalVideo/personalVideo.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    videoFile:'',
    exampUrl:'https://futurenext.com.cn/file/video/selfintroduction.mp4',
    exampShow:false
  },
  exampToggle(e){
    const status = e.currentTarget.dataset.status;
    let exampShow = status==1?true:false;
    this.setData({
      exampShow
    })
  },
  uploadVideo(){
    wx.navigateTo({
      url: `/pages/uploadVideo/uploadVideo?id=${globalData.userInfo.id}`,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.request({
      url: globalData.api.baseUrl + globalData.api.user.videofile+globalData.userInfo.id,
      success:(res)=>{
        if(res.statusCode==200){
          this.setData({
            videoFile:res.data.result
          })
          globalData.userInfo.video_file = res.data.result;
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})