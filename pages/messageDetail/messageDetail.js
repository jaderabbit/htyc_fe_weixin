// pages/messageDetail/messageDetail.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    channelid: '',
    companyExpand: true,
    userExpand: true,
    animationData: {},
    animation: null,
    sysMsg: {},
    msg: [],
    cardId: '',
    type: '',
    pageIndex: 1,
    pageSize: 20,
    sendMsg: '',
    userid: '',
    companyid: '',
    epmid: '',
    inter: null,
    updateMsgInter: null,
    postShow: false,
    postAsk: [],
    postAnswer: [],
    companyAdsShow: false,
    companyAds: '',
    applyShow: false,
    applyAsk: [],
    applyAnswer: [],
    applyVideo: '',
    applyVideoShow: false,
    applyTest: {},
    applyTestShow: false,
    certificate: [],
    edu: [],
    workexp: [],
    userInfo: {},
    userInfoShow: false,
    industryDetailList: [],
    loctionDetailList: [],
    industry: '',
    loction: '',
    salary: '',
    jobs: '',
    lange: [],
    status: '',
    expand: {},
    skills: [],
    trains: [],
    projectexp: []
  },
  companyExpandToggle() {
    let companyExpand = this.data.companyExpand;
    if (companyExpand) {
      this.data.animation.height(0).step();
    } else {
      this.data.animation.height('624rpx').step();
    }
    this.setData({
      companyExpand: !companyExpand,
      animationData: this.data.animation.export()
    })
  },
  userExpandToggle() {
    let userExpand = this.data.userExpand;
    if (userExpand) {
      this.data.animation.height(0).step();
    } else {
      this.data.animation.height('312rpx').step();
    }
    this.setData({
      userExpand: !userExpand,
      animationData: this.data.animation.export()
    })
  },
  userInfoShowToggle(e) {
    const status = e.currentTarget.dataset.status;
    let userInfoShow = status == 1 ? true : false
    this.setData({
      userInfoShow
    })
  },
  getIndustry(id) {
    wx.request({
      url: globalData.api.baseUrl + globalData.api.dict.industry + id,
      success: (res) => {
        if (res.statusCode == 200) {
          const industryDetailList = res.data.result;
          this.setData({
            industryDetailList
          })
        }
      }
    })
  },
  getLoction(id) {
    wx.request({
      url: globalData.api.baseUrl + globalData.api.dict.city + id,
      success: (res) => {
        if (res.statusCode == 200) {
          const loctionDetailList = res.data.result;
          this.setData({
            loctionDetailList
          })
        }
      }
    })
  },
  updateMsgStatus(type, msgIdArr, callback) {
    wx.request({
      url: globalData.api.baseUrl + globalData.api.message.status,
      method: 'PUT',
      data: {
        "accountid": msgIdArr,
        "type": type
      },
      success: (res) => {
        callback && callback();
      }
    })
  },
  updateSysMsgStatus(type, msgIdArr, callback) {
    wx.request({
      url: globalData.api.baseUrl + globalData.api.message.sysstatus,
      method: 'PUT',
      data: {
        "accountid": msgIdArr,
        "type": type
      },
      success: (res) => {
        callback && callback();
      }
    })
  },
  postToggle(e) {
    const status = e.currentTarget.dataset.status;
    let postShow = status == 1 ? true : false
    this.setData({
      postShow
    })
  },
  applyTestToggle(e) {
    const status = e.currentTarget.dataset.status;
    let applyTestShow = status == 1 ? true : false
    this.setData({
      applyTestShow
    })
  },
  applyVideoToggle(e) {
    const status = e.currentTarget.dataset.status;
    let applyVideoShow = status == 1 ? true : false
    this.setData({
      applyVideoShow
    })
  },
  applyToggle(e) {
    const status = e.currentTarget.dataset.status;
    let applyShow = status == 1 ? true : false
    this.setData({
      applyShow
    })
  },
  adsToggle(e) {
    const status = e.currentTarget.dataset.status;
    let companyAdsShow = status == 1 ? true : false
    this.setData({
      companyAdsShow
    })
  },
  sendMsgIn(e) {
    this.setData({
      sendMsg: e.detail.value
    })
  },
  getLabels(flag) {
    wx.request({
      url: globalData.api.baseUrl + globalData.api.exam.label + flag,
      success: (res) => {
        if (res.statusCode == 200) {
          if (res.data.result) {
            this.setData({
              applyTest: res.data.result
            })
          }

        }
      }
    })
  },
  getMsgByLastId(type, id, callback) {
    let url = '';
    const data = this.data;
    let msg = data.msg;
    if (type == 'user') {
      url = `${globalData.api.baseUrl}${globalData.api.message.userLast}1/10/${data.companyid}/${data.userid}/${id}?usertype=user`
    } else if (type == 'emp') {
      if (data.channelid) {
        url = `${globalData.api.baseUrl}${globalData.api.channel.latestlist}${data.channelid}/${data.epmid}?pageIndex=1&pageSize=10&latestID=${id}&usertype=employee`
      } else {
        url = `${globalData.api.baseUrl}${globalData.api.message.epmLast}1/10/${data.companyid}/${data.epmid}/${id}?usertype=employee`
      }

    }
    wx.request({
      url: url,
      success: (res) => {
        if (res.statusCode == 200) {
          callback && callback();
          msg = msg.concat(res.data.result.data);
          this.setData({
            msg
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      fail: (error) => {
        wx.showToast({
          title: error,
          icon: 'none'
        })
      }
    })
  },
  sendComfrin() {
    const data = this.data;
    if (!data.sendMsg) {
      wx.showToast({
        title: '请输入发送消息',
        icon: 'none'
      })
      return;
    }
    if (data.cardId == 1 && data.userid) {
      wx.request({
        url: globalData.api.baseUrl + globalData.api.message.userSend,
        method: 'POST',
        data: {
          "company_userid": data.companyid,
          "direction": true,
          "text": data.sendMsg,
          "userid": data.userid
        },
        success: (res) => {
          if (res.statusCode == 200) {
            let msg = data.msg;
            msg.push(res.data.result)
            this.setData({
              msg,
              sendMsg: ''
            })
          } else {
            wx.showToast({
              title: res.data.message,
              icon: 'none'
            })
          }
        },
        fail: (error) => {
          wx.showToast({
            title: error,
            icon: 'none'
          })
        }
      })
      return;
    }
    if (data.cardId == 1 && data.epmid) {
      if (data.channelid) {
        wx.request({
          url: globalData.api.baseUrl + globalData.api.channel.sendmsg,
          method: 'POST',
          data: {
            "channelid": data.channelid,
            "direction": true,
            "employeeid": data.epmid,
            "text": data.sendMsg
          },
          success: (res) => {
            if (res.statusCode == 200) {
              let msg = data.msg;
              msg.push(res.data.result)
              this.setData({
                msg,
                sendMsg: ''
              })
            } else {
              wx.showToast({
                title: res.data.message,
                icon: 'none'
              })
            }
          },
          fail: (error) => {
            wx.showToast({
              title: error,
              icon: 'none'
            })
          }
        })
      } else {
        wx.request({
          url: globalData.api.baseUrl + globalData.api.message.epmSend,
          method: 'POST',
          data: {
            "company_userid": data.companyid,
            "direction": true,
            "text": data.sendMsg,
            "userid": data.epmid
          },
          success: (res) => {
            if (res.statusCode == 200) {
              let msg = data.msg;
              msg.push(res.data.result)
              this.setData({
                msg,
                sendMsg: ''
              })
            } else {
              wx.showToast({
                title: res.data.message,
                icon: 'none'
              })
            }
          },
          fail: (error) => {
            wx.showToast({
              title: error,
              icon: 'none'
            })
          }
        })
      }

      return;
    }
    if (data.cardId == 3 && data.epmid) {
      if(data.channelid){
        wx.request({
          url: globalData.api.baseUrl + globalData.api.channel.sendmsg,
          method: 'POST',
          data: {
            "channelid": data.channelid,
            "direction": false,
            "employeeid": data.epmid,
            "text": data.sendMsg
          },
          success: (res) => {
            if (res.statusCode == 200) {
              let msg = data.msg;
              msg.push(res.data.result)
              this.setData({
                msg,
                sendMsg: ''
              })
            } else {
              wx.showToast({
                title: res.data.message,
                icon: 'none'
              })
            }
          },
          fail: (error) => {
            wx.showToast({
              title: error,
              icon: 'none'
            })
          }
        })
      }else{
        wx.request({
          url: globalData.api.baseUrl + globalData.api.message.epmSend,
          method: 'POST',
          data: {
            "company_userid": data.companyid,
            "direction": false,
            "text": data.sendMsg,
            "userid": data.epmid
          },
          success: (res) => {
            if (res.statusCode == 200) {
              let msg = data.msg;
              msg.push(res.data.result)
              this.setData({
                msg,
                sendMsg: ''
              })
            } else {
              wx.showToast({
                title: res.data.message,
                icon: 'none'
              })
            }
          },
          fail: (error) => {
            wx.showToast({
              title: error,
              icon: 'none'
            })
          }
        })
      }
     
      return;
    }
    if (data.cardId == 4 && data.userid) {
      wx.request({
        url: globalData.api.baseUrl + globalData.api.message.userSend,
        method: 'POST',
        data: {
          "company_userid": data.companyid,
          "direction": false,
          "text": data.sendMsg,
          "userid": data.userid
        },
        success: (res) => {
          if (res.statusCode == 200) {
            let msg = data.msg;
            msg.push(res.data.result)
            this.setData({
              msg,
              sendMsg: ''
            })
          } else {
            wx.showToast({
              title: res.data.message,
              icon: 'none'
            })
          }
        },
        fail: (error) => {
          wx.showToast({
            title: error,
            icon: 'none'
          })
        }
      })
      return;
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (options.employeeid) {
      this.setData({
        epmid: parseInt(options.employeeid)
      })
    }
    if (options.channelid) {
      this.setData({
        channelid: parseInt(options.channelid)
      })
    }
    if (options.company) {
      this.setData({
        companyid: parseInt(options.company)
      })
    }
    if (options.user) {
      this.setData({
        userid: parseInt(options.user)
      })
    }
    if (options.employee) {
      this.setData({
        epmid: parseInt(options.employee)
      })
    }
    const cardId = globalData.cardId;
    this.setData({
      cardId
    })
    const data = this.data;
    let title = options.type == 1 ? '系统消息' : '沟通消息';
    let usertype = '';
    this.setData({
      type: options.type
    })
    wx.setNavigationBarTitle({
      title: title,
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    })
    if (data.userid) {
      let inter = setInterval(() => {
        this.getMsgByLastId('user', data.msg[data.msg.length - 1].id);
      }, 2000)
      this.setData({
        inter
      })
    }
    if (data.epmid) {
      let inter = setInterval(() => {
        this.getMsgByLastId('emp', data.msg[data.msg.length - 1].id);
      }, 2000)
      this.setData({
        inter
      })
    }
    if (options.type == 1) {
      let msg = JSON.parse(wx.getStorageSync('MSG'));
      this.setData({
        sysMsg: msg
      })
      wx.removeStorageSync('MSG');
    } else {
      if (cardId == 1) {
        usertype = 'company'
        if (options.channelid) {
          wx.request({
            url: `${globalData.api.baseUrl}${globalData.api.channel.message}${options.channelid}/${data.epmid}?pageIndex=${data.pageIndex}&pageSize=${data.pageSize}&usertype=${usertype}`,
            success: (res) => {
              if (res.statusCode == 200) {
                const arr = res.data.result.data;
                arr.reverse();
                this.setData({
                  msg: arr
                })
              } else {
                wx.showToast({
                  title: res.data.message,
                  icon: 'none'
                })
              }
            },
            fail: (error) => {
              wx.showToast({
                title: error,
                icon: 'none'
              })
            }
          })
          return;
        }
        if (options.employee) {
          wx.request({
            url: `${globalData.api.baseUrl}${globalData.api.message.epmDetail}${data.pageIndex}/${data.pageSize}/${options.company}/${options.employee}?usertype=${usertype}`,
            success: (res) => {
              if (res.statusCode == 200) {
                const arr = res.data.result.data;
                arr.reverse();
                this.setData({
                  msg: arr
                })
              } else {
                wx.showToast({
                  title: res.data.message,
                  icon: 'none'
                })
              }
            },
            fail: (error) => {
              wx.showToast({
                title: error,
                icon: 'none'
              })
            }
          })
        } else if (options.user) {
          wx.request({
            url: `${globalData.api.baseUrl}${globalData.api.message.userDetail}${data.pageIndex}/${data.pageSize}/${options.company}/${options.user}?usertype=${usertype}`,
            success: (res) => {
              if (res.statusCode == 200) {
                const arr = res.data.result.data;
                arr.reverse();
                this.setData({
                  msg: arr
                })
              } else {
                wx.showToast({
                  title: res.data.message,
                  icon: 'none'
                })
              }
            },
            fail: (error) => {
              wx.showToast({
                title: error,
                icon: 'none'
              })
            }
          })
        }
      }
      if (cardId == 3) {
        usertype = 'employee';
        if (options.channelid) {
          wx.request({
            url: `${globalData.api.baseUrl}${globalData.api.channel.message}${options.channelid}/${data.epmid}?pageIndex=${data.pageIndex}&pageSize=${data.pageSize}&usertype=${usertype}`,
            success: (res) => {
              if (res.statusCode == 200) {
                const arr = res.data.result.data;
                arr.reverse();
                this.setData({
                  msg: arr
                })
              } else {
                wx.showToast({
                  title: res.data.message,
                  icon: 'none'
                })
              }
            },
            fail: (error) => {
              wx.showToast({
                title: error,
                icon: 'none'
              })
            }
          })
          return;
        }
        wx.request({
          url: `${globalData.api.baseUrl}${globalData.api.message.epmDetail}${data.pageIndex}/${data.pageSize}/${options.company}/${options.employee}?usertype=${usertype}`,
          success: (res) => {
            if (res.statusCode == 200) {
              const arr = res.data.result.data;
              arr.reverse();
              this.setData({
                msg: arr
              })
            } else {
              wx.showToast({
                title: res.data.message,
                icon: 'none'
              })
            }
          },
          fail: (error) => {
            wx.showToast({
              title: error,
              icon: 'none'
            })
          }
        })
      }
      if (cardId == 4) {
        usertype = 'user'
        wx.request({
          url: `${globalData.api.baseUrl}${globalData.api.message.userDetail}${data.pageIndex}/${data.pageSize}/${options.company}/${options.user}?usertype=${usertype}`,
          success: (res) => {
            if (res.statusCode == 200) {
              const arr = res.data.result.data;
              arr.reverse();
              this.setData({
                msg: arr
              })
            } else {
              wx.showToast({
                title: res.data.message,
                icon: 'none'
              })
            }
          },
          fail: (error) => {
            wx.showToast({
              title: error,
              icon: 'none'
            })
          }
        })
      }
    }
    if (cardId == 1 && data.userid) {
      wx.request({
        url: globalData.api.baseUrl + globalData.api.user.baseInfo + data.userid,
        success: (res) => {
          if (res.statusCode == 200) {
            let userInfo = res.data.result;
            const statusArr = ['暂无', '在职，无跳槽打算', '在职，正在找工作', '离职，正在找工作', '离职，无找工作计划', '自由职业者'];
            this.setData({
              status: statusArr[userInfo.status]
            })
            if (userInfo.birthday) {
              let birthYear = parseInt(userInfo.birthday.slice(0, 4));
              let year = new Date().getFullYear();
              userInfo.age = year - birthYear + 1 + '岁';
            } else {
              userInfo.age = '暂无'
            }
            this.setData({
              userInfo
            })
          }
        }
      })
      this.getIndustry('');
      this.getLoction('');
      let cerUrl = globalData.api.user.certificate,
        eduUrl = globalData.api.user.edu,
        wrokexpUrl = globalData.api.user.workexp;
      wx.request({
        url: globalData.api.baseUrl + cerUrl + '/' + data.userid,
        success: (res) => {
          if (res.statusCode == 200) {
            this.setData({
              certificate: res.data.result
            })
          } else {
            wx.showToast({
              title: res.data.message,
              icon: 'none'
            })
          }
        },
        fail: (error) => {
          wx.showToast({
            title: error,
            icon: 'none'
          })
        }
      })
      wx.request({
        url: globalData.api.baseUrl + eduUrl + '/' + data.userid,
        success: (res) => {
          if (res.statusCode == 200) {
            const edu = res.data.result;
            edu.map((item) => {
              item.startYear = item.admission_date.slice(0, 4);
              item.endYear = item.graduation_date.slice(0, 4);
            })
            this.setData({
              edu
            })
          } else {
            wx.showToast({
              title: res.data.message,
              icon: 'none'
            })
          }
        },
        fail: (error) => {
          wx.showToast({
            title: error,
            icon: 'none'
          })
        }
      })
      wx.request({
        url: globalData.api.baseUrl + wrokexpUrl + '/' + data.userid,
        success: (res) => {
          if (res.statusCode == 200) {
            const workexp = res.data.result;
            workexp.map((item) => {
              item.startDate = item.employment_date ? item.employment_date.slice(0, 10).replace(/-/g, '.') : item.employment_date;
              item.endDate = item.leave_date ? item.leave_date.slice(0, 10).replace(/-/g, '.') : item.leave_date;
            })
            this.setData({
              workexp
            })
          } else {
            wx.showToast({
              title: res.data.message,
              icon: 'none'
            })
          }
        },
        fail: (error) => {
          wx.showToast({
            title: error,
            icon: 'none'
          })
        }
      })
      wx.request({
        url: globalData.api.baseUrl + globalData.api.user.lang + '/' + data.userid,
        success: (res) => {
          if (res.statusCode == 200) {
            const typeArr = ['一般', '良好', '熟练', '精通'];
            const arr = res.data.result;
            arr.map(item => {
              item.hear = typeArr[item.hear - 1];
              item.read_write = typeArr[item.read_write - 1];
            })
            this.setData({
              lange: arr
            })
          }
        }
      })
      wx.request({
        url: globalData.api.baseUrl + globalData.api.user.expand + '/' + data.userid,
        success: (res) => {
          if (res.statusCode == 200) {
            this.setData({
              expand: res.data.result
            })
          }
        }
      })
      wx.request({
        url: globalData.api.baseUrl + globalData.api.user.skill + '/' + data.userid,
        success: (res) => {
          if (res.statusCode == 200) {
            const typeArr = ['一般', '良好', '熟练', '精通'];
            const arr = res.data.result;
            arr.map(item => {
              item.mastery_level = typeArr[item.mastery_level - 1];
            })
            this.setData({
              skills: arr
            })
          }
        }
      })
      wx.request({
        url: globalData.api.baseUrl + globalData.api.user.train + '/' + data.userid,
        success: (res) => {
          if (res.statusCode == 200) {
            const arr = res.data.result;
            arr.map(item => {
              item.start = item.begin_date.slice(0, 7).replace(/-/g, '.');
              item.end = item.end_date.slice(0, 7).replace(/-/g, '.');
            })
            this.setData({
              trains: arr
            })
          }
        }
      })
      wx.request({
        url: globalData.api.baseUrl + globalData.api.user.projectexp + '/' + data.userid,
        success: (res) => {
          if (res.statusCode == 200) {
            const arr = res.data.result;
            arr.map(item => {
              item.start = item.begin_date.slice(0, 7).replace(/-/g, '.');
              item.end = item.end_date.slice(0, 7).replace(/-/g, '.');
            })
            this.setData({
              projectexp: arr
            })
          }
        }
      })
      wx.request({
        url: globalData.api.baseUrl + globalData.api.user.userRecommend + data.userid,
        success: (res) => {
          if (res.statusCode == 200) {
            const data = res.data.result;
            let industrys = data.industrys ? data.industrys.split(',') : [];
            const regions = data.regions ? data.regions.split(',') : [];
            const industry = [],
              region = [];
            let salary = this.data.salary;
            let salaryInLow = this.data.salaryInLow;
            let salaryInUp = this.data.salaryInUp;
            if (data.pay_lower == -1) {
              salary = '不限';
            } else {
              salary = `${data.pay_lower}~${data.pay_upper}`;
            }
            industrys.map(item => {
              this.data.industryDetailList.map(inner => {
                if (inner.id == item) {
                  industry.push(inner.name);
                }
              })
            })
            regions.map(item => {
              this.data.loctionDetailList.map(inner => {
                if (inner.id == item) {
                  region.push(inner.name);
                }
              })
            })
            this.setData({
              industry: industry.join('/'),
              loction: region.join('/'),
              salary,
              jobs: data.jobs ? data.jobs.replace(/,/g, '/') : ''
            })
          } else {
            // wx.showToast({
            //   title: res.data.message,
            //   icon: 'none'
            // })
          }
        },
        fail: (error) => {
          wx.showToast({
            title: error,
            icon: 'none'
          })
        }
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    if (this.data.type == 2 && this.data.userid) {
      wx.request({
        url: globalData.api.baseUrl + globalData.api.company.ask,
        success: (res) => {
          if (res.statusCode == 200) {
            const arr = res.data.result;
            this.setData({
              postAsk: arr,
            })
          }
        }
      })
      wx.request({
        url: globalData.api.baseUrl + globalData.api.company.answer + '/' + this.data.companyid,
        success: (res) => {
          if (res.statusCode == 200) {
            const arr = res.data.result;
            if (arr.length > 0) {
              this.setData({
                postAnswer: arr
              })
            }
          }
        }
      })
      wx.request({
        url: globalData.api.baseUrl + globalData.api.user.ask,
        success: (res) => {
          if (res.statusCode == 200) {
            const arr = res.data.result;
            this.setData({
              applyAsk: arr,
            })
          }
        }
      })
      wx.request({
        url: globalData.api.baseUrl + globalData.api.user.answer + '/' + this.data.userid,
        success: (res) => {
          if (res.statusCode == 200) {
            const arr = res.data.result;
            if (arr.length > 0) {
              this.setData({
                applyAnswer: arr
              })
            }
          }
        }
      })
      wx.request({
        url: globalData.api.baseUrl + globalData.api.company.ads + '/' + this.data.companyid,
        success: (res) => {
          if (res.statusCode == 200) {
            this.setData({
              companyAds: res.data.result
            })
          }
        }
      })
      wx.request({
        url: globalData.api.baseUrl + globalData.api.user.videofile + '/' + this.data.userid,
        success: (res) => {
          if (res.statusCode == 200) {
            this.setData({
              applyVideo: res.data.result
            })
          }
        }
      })
      wx.request({
        url: `${globalData.api.baseUrl}${globalData.api.user.characteranswer}/${this.data.userid}`,
        success: (res) => {
          if (res.statusCode == 200) {
            this.getLabels(res.data.result.flag);
          } else {
            wx.showToast({
              title: res.data.message,
              icon: 'none'
            })
          }
        },
        fail: (error) => {
          wx.showToast({
            title: error,
            icon: 'none',
          })
        }
      })
      let updateMsgInter = setInterval(() => {
        let msg = this.data.msg;
        let msgIdArr = [];
        msg.map((item) => {
          if (!item.status) {
            msgIdArr.push(item.id);
          }
        })
        let type = this.data.userid ? 'user' : 'employee';
        msgIdArr.length > 0 && this.updateMsgStatus(type, msgIdArr, () => {
          msg.map(item => {
            if (!item.status) {
              item.status = true;
            }
          })
        });
      }, 300)
      this.setData({
        updateMsgInter
      })
    } else {
      let type = this.data.cardId == 1 ? 'company' : this.data.cardId == 3 ? 'employee' : 'user';
      let sysMsg = this.data.sysMsg;
      if (!sysMsg.status) {
        let msgIdArr = [sysMsg.id];
        this.updateSysMsgStatus(type, msgIdArr, () => {
          sysMsg.status = true;
          this.setData({
            sysMsg
          })
        })
      }

    }
    const animation = wx.createAnimation({
      duration: 500,
      timingFunction: 'ease',
    })
    this.setData({
      animation
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {
    clearInterval(this.data.inter);
    clearInterval(this.data.updateMsgInter);
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})