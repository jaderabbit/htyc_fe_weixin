// pages/interTanlantInfo/interTanlantInfo.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id: '',
    certificate: [],
    edu: [],
    workexp: [],
    userInfo: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    try {
      let userJson = wx.getStorageSync('user');
      if (userJson) {
        let userInfo = JSON.parse(userJson);
        if (userInfo.birthday) {
          let birthYear = parseInt(userInfo.birthday.slice(0, 4));
          let year = new Date().getFullYear();
          userInfo.age = year - birthYear  + '岁';
        } else {
          userInfo.age = '暂无'
        }
        this.setData({
          userInfo
        })
        wx.removeStorageSync('user');
      }
    } catch (e) {

    }
    const id = options.id;
    let cerUrl = globalData.api.employee.certificate,
      eduUrl = globalData.api.employee.edu,
      wrokexpUrl = globalData.api.employee.workexp;
    wx.request({
      url: globalData.api.baseUrl + cerUrl + '/' + id,
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            certificate: res.data.result
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      fail: (error) => {
        wx.showToast({
          title: error,
          icon: 'none'
        })
      }
    })
    wx.request({
      url: globalData.api.baseUrl + eduUrl + '/' + id,
      success: (res) => {
        if (res.statusCode == 200) {
          const edu = res.data.result;
          edu.map((item) => {
            item.startYear = item.admission_date.slice(0, 4);
            item.endYear = item.graduation_date.slice(0, 4);
          })
          this.setData({
            edu
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      fail: (error) => {
        wx.showToast({
          title: error,
          icon: 'none'
        })
      }
    })
    wx.request({
      url: globalData.api.baseUrl + wrokexpUrl + '/' + id,
      success: (res) => {
        if (res.statusCode == 200) {
          const workexp = res.data.result;
          workexp.map((item) => {
            item.startDate = item.employment_date ? item.employment_date.slice(0, 10).replace(/-/g, '.') : item.employment_date;
            item.endDate = item.leave_date ? item.leave_date.slice(0, 10).replace(/-/g, '.') : item.leave_date;
          })
          this.setData({
            workexp
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      fail: (error) => {
        wx.showToast({
          title: error,
          icon: 'none'
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})