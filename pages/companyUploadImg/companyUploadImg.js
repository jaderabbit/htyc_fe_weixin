// pages/companyUploadImg/companyUploadImg.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    ads:'',
    licenseImg:'',
    arr:[]
  },
  deleteImg(e){
    const id = e.currentTarget.dataset.id;
    const num = e.currentTarget.dataset.num;
    wx.request({
      url: globalData.api.baseUrl+globalData.api.company.expand+id,
      method:'DELETE',
      success:(res)=>{
        if(res.statusCode==200){
          const arr = this.data.arr;
          arr.splice(num,1);
          wx.showToast({
            title: '删除成功',
            icon:"none"
          })
          this.setData({
            arr
          })
        }
      }
    })
  },
  getAds(){
    wx.request({
      url: globalData.api.baseUrl+globalData.api.company.expand+globalData.userInfo.id,
      success:(res)=>{
        if(res.statusCode==200){
          this.setData({
            arr:res.data.result
          })
        }
      }
    })
  },
  comfrin(){
    wx.navigateBack({
      delta: 1,
    })
  },
  upload() {
    wx.chooseImage({
      success: (res) => {
        const IMGURL = res.tempFilePaths[0];
        this.setData({
          licenseImg: IMGURL
        })
        wx.uploadFile({
          url: globalData.api.baseUrl + globalData.api.common.uploadFile,
          filePath: IMGURL,
          name: 'name',
          success: (res) => {
            if (res.statusCode == 200) {
              const ads = JSON.parse(res.data).result;
              this.setData({
                ads
              })
              globalData.userInfo.ads_file = ads;
              const adsArr = [];
              this.data.arr.map(item=>{
                adsArr.push(item.ads_file)
              })
              adsArr.push(ads);
              wx.showLoading({
                title: '上传中',
              })
              wx.request({
                url: globalData.api.baseUrl + globalData.api.company.expand,
                method:'POST',
                data:{
                  "ads_file": adsArr,
                  "companyid": globalData.userInfo.id
                },
                success:(res)=>{
                  wx.hideLoading();
                  if(res.statusCode==200){
                    this.getAds();
                    wx.showToast({
                      title: '上传成功',
                    })
                    this.setData({
                      licenseImg:''
                    })
                  }else{
                    wx.showToast({
                      title: res.data.message,
                      icon:'none'
                    })
                  }
                },
                complete:()=>{
                  
                }
              })
            }
          },
          fail: (res) => {
            console.log(res);
          }
        })
      },
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      this.getAds();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})