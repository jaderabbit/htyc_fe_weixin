// pages/addTask/addTask.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    task: [{
      type: 1,
      task_name: '',
      task_answer: ''
    }],
    name:''
  },
  nameIn(e){
    this.setData({
      name:e.detail.value
    })
  },
  qusIn(e){
    const task = this.data.task;
    const val = e.detail.value;
    const index = e.currentTarget.dataset.index;
    task[index].task_name = val;
    this.setData({
      task
    })
  },
  answerIn(e){
    const task = this.data.task;
    const val = e.detail.value;
    const index = e.currentTarget.dataset.index;
    task[index].task_answer = val;
    this.setData({
      task
    })
  },
  changeType(e){
    const task = this.data.task;
    const index = e.currentTarget.dataset.index;
    const type = e.currentTarget.dataset.type;
    task[index].type = type;
    this.setData({
      task
    })
  },
  addTask(){
    const task = this.data.task;
    task.push({
      type:1,
      task_name:'',
      task_answer:''
    })
    this.setData({
      task
    })
  },
  comfrin(){
    const data = this.data;
    if(!data.name){
      wx.showToast({
        title: '请输入流程名字',
        icon:'none'
      })
      return;
    }
    if(data.task.length==0){
      wx.showToast({
        title: '请添加流程动作',
        icon:'none'
      })
      return;
    }
    const procedure = {
      name:data.name,
      tasks:data.task
    }
    try{
      wx.setStorageSync('procedure', JSON.stringify(procedure));
      wx.navigateBack({
        delta: 1,
      })
    }catch(e){

    }
  
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})