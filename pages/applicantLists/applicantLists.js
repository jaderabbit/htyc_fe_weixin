// pages/applicantLists/applicantLists.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageIndex: 1,
    pageSize: 10,
    applys: [],
    id: '',
    statusArr:['未联系','已联系','已淘汰','企业已发送offer','用户已接受offer','用户已拒绝offer','企业已撤回offer','企业已关闭offer','offer已过期','已入职','试用期被淘汰','已转正','已离职']
  },
  checkApplicantInfo(e) {
    const id = e.currentTarget.dataset.id;
    const num = e.currentTarget.dataset.num;
    const jobid = e.currentTarget.dataset.jobid;
    const status = e.currentTarget.dataset.status;
    try {
      wx.setStorageSync('user', JSON.stringify(this.data.applys[num].user));
      wx.navigateTo({
        url: `/pages/applicantInfo/applicantInfo?id=${id}&jobid=${jobid}&status=${status}`,
        success: function(res) {},
        fail: function(res) {},
        complete: function(res) {},
      })
    } catch (e) {

    }

  },
  agree(e) {
    const id = e.currentTarget.dataset.id;
    const action = e.currentTarget.dataset.action;
    const num = e.currentTarget.dataset.action;
    wx.request({
      url: `${globalData.api.baseUrl}${globalData.api.job.jobSet}?id=${id}&action=${action}`,
      method: 'PUT',
      success: (res) => {
        if (res.statusCode == 200) {
          wx.showToast({
            title: '操作成功',
          })
          if (!action) {
            const applys = this.data.applys;
            applys.splice(num, 1);
            this.setData({
              applys
            })
          }
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      complete: () => {

      }
    })
  },
  getApplyList(id) {
    const data = this.data;
    wx.request({
      url: `${globalData.api.baseUrl}${globalData.api.job.applyList}${data.pageIndex}/${data.pageSize}/${id}`,
      method: 'GET',
      success: (res) => {
        if (res.statusCode == 200) {
          let arr = res.data.result.data;
          let applys = this.data.applys;
          arr.map((item) => {
            if (item.user.birthday) {
              let birthYear = parseInt(item.user.birthday.slice(0, 4));
              let year = new Date().getFullYear();
              item.user.age = year - birthYear  + '岁';
            } else {
              item.user.age = '暂无'
            }
            return item;
          })
          applys = applys.concat(arr);
          this.setData({
            applys
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      complete: () => {

      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const id = options.id;
    this.setData({
      id
    })
    this.getApplyList(id);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    let pageIndex = this.data.pageIndex;
    pageIndex++;
    this.setData({
      pageIndex
    })
    this.getApplyList(this.data.id);
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})