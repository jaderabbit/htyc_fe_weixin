// pages/tanlenTestDetail/tanlenTestDetail.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageIndex:1,
    pageSize:10,
    list:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const name = options.name,
    id = options.id,
    data = this.data;
    wx.setNavigationBarTitle({
      title: name,
    })
    wx.request({
      url: `${globalData.api.baseUrl}${globalData.api.exam.paperUserList}${data.pageIndex}/${data.pageSize}/${globalData.userInfo.id}/${id}`,
      success:(res)=>{
        if(res.statusCode==200){
          this.setData({
            list:res.data.result.data
          })
        }else{
          wx.showToast({
            title: res.data.message,
            icon:'none'
          })
        }
      },
      fail:(error)=>{
        wx.showToast({
          title: error,
          icon:'none'
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})