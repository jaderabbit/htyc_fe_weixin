// pages/userSkills/userSkills.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    name: '',
    time: '',
    ability: [{
      name: '一般',
      id: 1
    }, {
      name: '良好',
      id: 2
    }, {
      name: '熟悉',
      id: 3
    }, {
      name: '精通',
      id: 4
    }],
    hearShow: false,
    hear: '',
    hearNum: 0
  },
  nameIn(e) {
    this.setData({
      name: e.detail.value
    })
  },
  timeIn(e) {
    this.setData({
      time: e.detail.value
    })
  },
  hearToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        hearShow: true
      })
    } else {
      this.setData({
        hearShow: false
      })
      this.setData({
        hear: this.data.ability[this.data.hearNum].name,
      })
    }
  },
  hearChange(e) {
    this.setData({
      hearNum: e.detail.value[0]
    })
  },
  save() {
    const data = this.data;
    if (!data.name) {
      wx.showToast({
        title: '请输入技能名称',
        icon: 'none'
      })
      return;
    }
    if (!data.hear) {
      wx.showToast({
        title: '请选择掌握程度',
        icon: 'none'
      })
      return;
    }
    if (!data.time) {
      wx.showToast({
        title: '请输入使用时长',
        icon: 'none'
      })
      return;
    }
    let method = '', postData = null, skillUrl='';
    if (globalData.cardId == 3) {
      skillUrl = globalData.api.employee.skill;
    } else if (globalData.cardId == 4) {
      skillUrl = globalData.api.user.skill;
    }
    if (data.id) {
      method = 'PUT';
      postData = {
        "duration_use": parseInt(data.time),
        "mastery_level": data.ability[data.hearNum].id,
        "name": data.name,
        "id": data.id,
      }
    } else {
      method = 'POST';
      postData = {
        "duration_use": parseInt(data.time),
        "mastery_level": data.ability[data.hearNum].id,
        "name": data.name,
      }
      if (globalData.cardId == 3) {
        postData.employeeid = globalData.userInfo.id;
      } else if (globalData.cardId == 4) {
        postData.userid = globalData.userInfo.id;
      }
    }
    wx.showLoading({
      title: '保存中...',
    })
    wx.request({
      url: globalData.api.baseUrl + skillUrl,
      method: method,
      data: postData,
      success: (res) => {
        wx.hideLoading();
        if (res.statusCode == 200) {
          wx.showToast({
            title: '保存成功',
          })
          wx.navigateBack({
            delta: 1,
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      fail: (error) => {
        wx.hideLoading();
        wx.showToast({
          title: error,
          icon: 'none'
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const id = options.id;
    if (id) {
      wx.setNavigationBarTitle({
        title: '编辑专业技能',
      })
      try {
        let skillJson = wx.getStorageSync('skill');
        if (skillJson) {
          const skill = JSON.parse(skillJson);
          let hearNum = this.data.hearNum;
          this.data.ability.map((item, index) => {
            if (item.name == skill.mastery_level) {
              hearNum = index;
            }
           
          })
          this.setData({
            hearNum,
            id: parseInt(id),
            hear: skill.mastery_level,
            name:skill.name,
            time: skill.duration_use
          })
          wx.removeStorageSync('skill');
        }
      } catch (e) {

      }
    } else {
      wx.setNavigationBarTitle({
        title: '添加专业技能',
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})