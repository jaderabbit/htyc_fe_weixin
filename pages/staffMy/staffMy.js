// pages/staffMy/staffMy.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    roleMaskShow: false,
    userInfo: {},
    percentcomplete:0,
    num: 0,
    roleNum:0,
    noreadcount:0
  },
  roleChange(e){
    const roleNum = e.detail.value[0];
   this.setData({
     roleNum
   })
  },
  roleChangeComfirn(){
    this.setData({
      roleMaskShow: false
    })
    const roleNum = this.data.roleNum;
    if(roleNum==0){
      wx.request({
        url: globalData.api.baseUrl + globalData.api.company.isregister,
        method: 'POST',
        data: {
          mobilephone: globalData.userInfo.telephone,
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        success: (res) => {
          if (res.statusCode == 200) {
            if (res.data.result){
              globalData.cardId = 1;
              globalData.userInfo = res.data.result;
              let loginTime = new Date().getTime();
              try {
                wx.setStorageSync('cardId', 1);
                wx.setStorageSync('userInfo', JSON.stringify(res.data.result));
                wx.setStorageSync('loginTime', loginTime);
              } catch (e) {

              }
              wx.redirectTo({
                url: '/pages/recruit/recruit',
                success: function (res) { },
                fail: function (res) { },
                complete: function (res) { },
              })
            }else{
              wx.showToast({
                title: '您没有企业管理权限，请向企业管理员申请。',
                icon: 'none'
              })
            }
            
          } else {
            wx.showToast({
              title: res.data.message,
              icon: 'none'
            })
          }
        },
        fail: (error) => {
          wx.showToast({
            title: error,
            icon: 'none'
          })
        }
      })
      return;
    }
    wx.request({
      url: globalData.api.baseUrl + globalData.api.employee.convertuser,
      method:'POST',
      data:{
        telephone:globalData.userInfo.telephone
      },
      header:{
        'content-type': 'application/x-www-form-urlencoded' 
      },
      success:(res)=>{
        if(res.statusCode==200){
          globalData.cardId = 4;
          globalData.userInfo = res.data.result;
          let loginTime = new Date().getTime();
          try {
            wx.setStorageSync('cardId', 4);
            wx.setStorageSync('userInfo', JSON.stringify(res.data.result));
            wx.setStorageSync('loginTime', loginTime);
          } catch (e) {

          }

          wx.redirectTo({
            url: '/pages/personalHome/personalHome',
            success: function (res) { },
            fail: function (res) { },
            complete: function (res) { },
          })
        }else{
          wx.showToast({
            title: res.data.message,
            icon:'none'
          })
        }
      },
      fail:(error)=>{
        wx.showToast({
          title: error,
          icon:'none'
        })
      }
    })
   
  },
  roleChooseShow() {
    this.setData({
      roleMaskShow: true
    })
  },
  roleChooseHide() {
    this.setData({
      roleMaskShow: false
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const userInfo = globalData.userInfo;
    this.setData({
      userInfo
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.message.noreadcount + '?msgType=employee' + '&accountid=' + globalData.userInfo.id,
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            noreadcount: res.data.result
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const data = this.data;
    wx.request({
      url: globalData.api.baseUrl + globalData.api.employee.percentcomplete + globalData.userInfo.id,
      success: (res) => {
        if (res.statusCode == 200) {
          const num = Math.round(res.data.result / 100 * 17);
          this.setData({
            num,
            percentcomplete: res.data.result
          })
        }
      },
      complete: () => {

      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})