// pages/personalMy/personalMy.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    roleMaskShow: false,
    chooseImageMask: false,
    userInfo: {},
    percentcomplete: 0,
    num: 0,
    roleNum: 0,
    noreadcount:0,
    isanswer:false
  },
  roleChange(e) {
    const roleNum = e.detail.value[0];
    this.setData({
      roleNum
    })
  },
  roleChangeComfirn() {
    this.setData({
      roleMaskShow: false
    })
    const roleNum = this.data.roleNum;
    if (roleNum == 1) {
      this.setData({
        roleNum: 0
      })
      return;
    }
    wx.request({
      url: globalData.api.baseUrl + globalData.api.employee.isregister,
      method:'POST',
      data:{
        mobilephone: globalData.userInfo.telephone
      },
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      success:(res)=>{
        if(res.statusCode==200){
            if(res.data.result){
              globalData.cardId = 3;
              globalData.userInfo = res.data.result;
              let loginTime = new Date().getTime();
              try {
                wx.setStorageSync('cardId', 3);
                wx.setStorageSync('userInfo', JSON.stringify(res.data.result));
                wx.setStorageSync('loginTime', loginTime);
              } catch (e) {

              }

              wx.redirectTo({
                url: '/pages/staffCommu/staffCommu',
                success: function (res) { },
                fail: function (res) { },
                complete: function (res) { },
              })
            }else{
              wx.navigateTo({
                url: '/pages/chooseCompany/chooseCompany',
              })
            }
        }else{
          wx.showToast({
            title: res.data.message,
            icon:'none'
          })
        }
      },
      fail:(error)=>{
        wx.showToast({
          title: error,
          icon:'none'
        })
      }
    })
   
  },
  roleChooseShow() {
    this.setData({
      roleMaskShow: true
    })
  },
  roleChooseHide() {
    this.setData({
      roleMaskShow: false
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const userInfo = globalData.userInfo;
    this.setData({
      userInfo
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.message.noreadcount + '?msgType=user' + '&accountid=' + globalData.userInfo.id,
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            noreadcount: res.data.result
          })
        }
      }
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.user.isanswer + '?userid=' + globalData.userInfo.id,
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            isanswer: res.data.result
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    const data = this.data;
    wx.request({
      url: globalData.api.baseUrl + globalData.api.user.percentcomplete + globalData.userInfo.id,
      success: (res) => {
        if (res.statusCode == 200) {
          const num = Math.round(res.data.result / 100 * 17);
          this.setData({
            num,
            percentcomplete: res.data.result
          })
        }
      },
      complete: () => {

      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})