// pages/addDepartment/addDepartment.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    name:'',
    depid:''
  },
  edit(){
    const data = this.data;
    if(!data.name){
      wx.showToast({
        title: '请输入部门名称',
        icon:'none'
      })
      return;
    }
    let method = data.depid?'PUT':'POST';
    let obj = null;
    if(data.depid){
      obj = {
        "id": data.depid,
        "name": data.name
      }
    }else{
      obj = {
        "companyid": globalData.userInfo.id,
        "name": data.name
      }
    }
    wx.showLoading({
      title: '提交中',
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.department.edit,
      method:method,
      data:obj,
      success:(res)=>{
        wx.hideLoading();
        if(res.statusCode==200){
          wx.navigateBack({
            delta: 1,
          })
        }else{
          wx.showToast({
            title: res.data.message,
            icon:'none'
          })
        }
      },
      fail:(error)=>{
        wx.hideLoading();
        wx.showToast({
          title: error,
          icon:'none'
        })
      }
    })
  },
  nameIn(e){
    this.setData({
      name:e.detail.value
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const name = options.name;
    const id = options.id;
    if(name ){
      this.setData({
        name,
        depid:parseInt(id)
      })
      wx.setNavigationBarTitle({
        title: '部门编辑',
        success: function(res) {},
        fail: function(res) {},
        complete: function(res) {},
      })
    }else{
      wx.setNavigationBarTitle({
        title: '新增部门',
        success: function (res) { },
        fail: function (res) { },
        complete: function (res) { },
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})