// pages/staffBasicInfo/staffBasicInfo.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    marryArr: ['已婚未育', '未婚', '已婚已育'],
    userInfo: {},
    majorDetailList:[],
    degreeList: [],
    majorList: [],
    certificateList: [],
    titleList: [],
    departmentList: [],
    sexArr: ['女', '男'],
    sexShow: false,
    sexNum: 0,
    majorShow:false,
    majorNum:0,
    degreeShow:false,
    degreeNum:0,
    departmentShow:false,
    departmentNum:0,
    certificateShow:false,
    certificateNum:0,
    titleShow:false,
    titleNum:0,
    birthday:'',
    name:'',
    idNumber:'',
    info:{},
    currentImg:'',
    email:''
  },
  marryChange(e) {
    this.setData({
      'userInfo.ismarried': this.data.marryArr[e.detail.value] == '未婚' ? '0' : this.data.marryArr[e.detail.value] == '已婚未育' ? '1' : '2'
    })
  },
  cityChange(e) {
    this.setData({
      'userInfo.position': e.detail.value[1] + e.detail.value[2]
    })
  },
  emailIn(e){
    this.setData({
      email:e.detail.value,
      'userInfo.email': e.detail.value
    })
  },
  upload() {
    wx.chooseImage({
      success: (res) => {
        const IMGURL = res.tempFilePaths[0];
        this.setData({
          currentImg: IMGURL
        })
        wx.uploadFile({
          url: globalData.api.baseUrl + globalData.api.common.uploadFile,
          filePath: IMGURL,
          name: 'name',
          success: (res) => {
            if (res.statusCode == 200) {
              this.setData({
                'userInfo.logo': JSON.parse(res.data).result
              })
            }
          },
          fail: (res) => {
            console.log(res);
          }
        })
      },
    })
  },
  idNumberIn(e){
    this.setData({
      idNumber:e.detail.value,
      'userInfo.idnumber': e.detail.value
    })
  },
  nameIn(e){
    this.setData({
      name:e.detail.value,
      'userInfo.name': e.detail.value
    })
  },
  birthdayChange(e){
    this.setData({
      birthday:e.detail.value,
      'userInfo.birthday': e.detail.value
    })
  },
  titleToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        titleShow: true
      })
    } else {
      this.setData({
        titleShow: false
      })
      this.setData({
        'userInfo.titleid': this.data.titleList[this.data.titleNum].id,
        'info.pro': this.data.titleList[this.data.titleNum].name
      })
    }
  },
  titleChange(e) {
    this.setData({
      titleNum: e.detail.value[0]
    })
    this.setData({
      'userInfo.titleid': this.data.titleList[e.detail.value[0]].id,
      'info.pro': this.data.titleList[this.data.titleNum].name
    })
  },
  certificateToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        certificateShow: true
      })
    } else {
      this.setData({
        certificateShow: false
      })
      this.setData({
        'userInfo.certificateid': this.data.certificateList[this.data.certificateNum].id,
        'info.certificate': this.data.certificateList[this.data.certificateNum].name
      })
    }
  },
  certificateChange(e) {
    this.setData({
      certificateNum: e.detail.value[0]
    })
    this.setData({
      'userInfo.certificateid': this.data.certificateList[e.detail.value[0]].id,
      'info.certificate': this.data.certificateList[e.detail.value[0]].name
    })
  },
  departmentToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        departmentShow: true
      })
    } else {
      this.setData({
        departmentShow: false
      })
      this.setData({
        'userInfo.depid': this.data.departmentList[this.data.departmentNum].id,
        'info.department': this.data.departmentList[this.data.departmentNum].name
      })
    }
  },
  departmentChange(e) {
    this.setData({
      departmentNum: e.detail.value[0]
    })
    this.setData({
      'userInfo.depid': this.data.departmentList[e.detail.value[0]].id,
      'info.department': this.data.departmentList[e.detail.value[0]].name
    })
  },
  degreeToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        degreeShow: true
      })
    } else {
      this.setData({
        degreeShow: false
      })
      this.setData({
        'userInfo.degreeid': this.data.degreeList[this.data.degreeNum].id,
        'info.degree': this.data.degreeList[this.data.degreeNum].name
      })
    }
  },
  degreeChange(e) {
    this.setData({
      degreeNum: e.detail.value[0]
    })
    this.setData({
      'userInfo.degreeid': this.data.degreeList[e.detail.value[0]].id,
      'info.degree': this.data.degreeList[e.detail.value[0]].name
    })
  },
  majorToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        majorShow: true
      })
    } else {
      this.setData({
        majorShow: false
      })
      this.setData({
        'userInfo.majorid': this.data.majorDetailList[this.data.majorNum].id,
        'info.major': this.data.majorDetailList[this.data.majorNum].name
      })
    }
  },
  majorChange(e) {
    this.getMajor(this.data.majorList[e.detail.value[0]].id);
    this.setData({
      majorNum: e.detail.value[1]
    })
    this.setData({
      'userInfo.majorid': this.data.majorDetailList[e.detail.value[1]].id,
      'info.major': this.data.majorDetailList[this.data.majorNum].name
    })
  },
  sexToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        sexShow: true
      })
    } else {
      this.setData({
        sexShow: false
      })
      this.setData({
        'userInfo.sex': this.data.sexArr[this.data.sexNum]=='女'?true:false,
        'info.sex': this.data.sexArr[this.data.sexNum]
      })
    }
  },
  sexChange(e) {
    this.setData({
      sexNum: e.detail.value[0]
    })
    this.setData({
      'userInfo.sex': this.data.sexArr[e.detail.value[0]] == '女' ? true : false,
      'info.sex': this.data.sexArr[e.detail.value[0]]
    })
  },
save(){
  const user = this.data.userInfo;
  const info = this.data.info;
  const isIdnumber = /^[1-9][0-9]{5}([1][9][0-9]{2}|[2][0][0|1][0-9])([0][1-9]|[1][0|1|2])([0][1-9]|[1|2][0-9]|[3][0|1])[0-9]{3}([0-9]|[X])$/;
  const ISEMAIL = /^([a-zA-Z]|[0-9])(\w|\-)+@[a-zA-Z0-9]+\.([a-zA-Z]{2,4})$/;
  if(!user.name){
    wx.showToast({
      title: '请输入姓名',
      icon:'none'
    })
    return;
  }
  if (!user.ismarried){
    wx.showToast({
      title: '请选择婚育情况',
      icon:'none'
    })
    return;
  }
  if (!user.position){
    wx.showToast({
      title: '请选择所在城市',
      icon:'none'
    })
    return;
  }
  if(!user.birthday){
    wx.showToast({
      title: '请选择出生日期',
      icon:'none'
    })
    return;
  }
  if (!isIdnumber.test(user.idnumber)){
    wx.showToast({
      title: '请输入正确的身份证号',
      icon:'none'
    })
    return;
  }
  if (!ISEMAIL.test(user.email)){
    wx.showToast({
      title: '请输入正确的邮箱',
      icon:'none'
    })
    return;
  }
  wx.showLoading({
    title: '修改中...',
  })
  wx.request({
    url: globalData.api.baseUrl + globalData.api.employee.update,
    method:'PUT',
    data:{
      "birthday": user.birthday+' 00:00:00',
      "certificateid": user.certificateid,
      "degreeid": user.degreeid,
      "depid": user.depid,
      "id": user.id,
      "idnumber": user.idnumber,
      "majorid": user.majorid,
      "ismarried": user.ismarried,
      "position": user.position,
      "name": user.name,
      "sex": user.sex,
      "email": user.email,
      "titleid": user.titleid,
      logo:user.logo
    },
    success:(res)=>{
      wx.hideLoading();
      if(res.statusCode==200){
        globalData.userInfo.birthday = user.birthday;
        globalData.userInfo.certificateid = user.certificateid;
        globalData.userInfo.certificate.name = info.certificate;
        globalData.userInfo.degreeid = user.degreeid;
        globalData.userInfo.degree.name = info.degree;
        globalData.userInfo.depid = user.depid;
        globalData.userInfo.dep.name = info.department;
        globalData.userInfo.idnumber = user.idnumber;
        globalData.userInfo.idnumber = user.idnumber;
        globalData.userInfo.name = user.name;
        globalData.userInfo.ismarried = user.ismarried;
        globalData.userInfo.position = user.position;
        globalData.userInfo.sex = user.sex;
        globalData.userInfo.majorid = user.majorid;
        globalData.userInfo.major.name = info.major;
        globalData.userInfo.titleid = user.titleid;
        globalData.userInfo.title.name = info.pro;
        globalData.userInfo.logo = user.logo;
        globalData.userInfo.email = user.email;
        try{
          wx.setStorageSync('userInfo', JSON.stringify(globalData.userInfo));
        }catch(e){

        }
        wx.navigateBack({
          delta: 1,
        })
      }else{
        wx.showToast({
          title: res.data.message,
          duration:2000,
          icon:'none'
        })
      }
    },
    complete:()=>{
      
    },
    fail:(error)=>{
      wx.hideLoading();
      wx.showToast({
        title: error,
        duration: 2000,
        icon: 'none'
      })
    }
  })
},
  getMajor(id) {
    wx.request({
      url: globalData.api.baseUrl + globalData.api.dict.majorList + id,
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            majorDetailList: res.data.result
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const info = {};
    info.sex = globalData.userInfo.sex?'女':'男';
    info.major = globalData.userInfo.major.name;
    info.degree = globalData.userInfo.degree.name;
    info.department = globalData.userInfo.dep.name;
    info.certificate = globalData.userInfo.certificate.name;
    info.pro = globalData.userInfo.title.name||'暂无';
    this.setData({
      userInfo: globalData.userInfo,
      birthday: globalData.userInfo.birthday.slice(0,10),
      name: globalData.userInfo.name,
      idNumber: globalData.userInfo.idnumber,
      info,
      currentImg: 'https://futurenext.com.cn:8086/' + globalData.userInfo.logo,
      'userInfo.logo': globalData.userInfo.logo,
      email:globalData.userInfo.email
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.dict.degreeList,
      method: 'GET',
      success: (res) => {
        if (res.statusCode == 200) {
          console.log(res)
          this.setData({
            degreeList: res.data.result
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon:'none'
          })
        }
      },
      complete: () => {

      }
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.dict.majorclass,
      method: 'GET',
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            majorList: res.data.result
          })
          this.getMajor(res.data.result[0].id)
        } else {
          wx.showToast({
            title: res.data.message,
            icon:'none'
          })
        }
      },
      complete: () => {

      }
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.dict.certificateList,
      method: 'GET',
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            certificateList: res.data.result
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon:'none'
          })
        }
      },
      complete: () => {

      }
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.dict.titleList,
      method: 'GET',
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            titleList: res.data.result
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon:'none'
          })
        }
      },
      complete: () => {

      }
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.department.list + '1/100/' + globalData.userInfo.companyid,
      method: 'GET',
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            departmentList: res.data.result.data
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon:'none'
          })
        }
      },
      complete: () => {

      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})