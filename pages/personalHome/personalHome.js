// pages/personalHome/personalHome.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    staffSizeArr: ['0-49',  '50-99', '100-499',  '500-999', '1000-4999', '5000以上'],
    listedArr: ['未上市', '国内上市', '海外上市'],
    companyTypeArr: ['国有企业', '外资企业', '民营企业', '中外合资'],
    resume: {},
    animationData: {},
    animation: null,
    applyNum: 0,
    passNum:0,
    userName: '',
    scopeShow: false,
    action:'',
    time:'',
    noreadcount:0,
    is_first_login:0,
    remain_numbers:0
  },
  hintHide(){
    globalData.userInfo.is_first_login = 0;
    this.setData({
      is_first_login:0
    })
  },
  getuserinfo(e) {
    let code = '';
    wx.login({
      success: function(res) {
        code = res.code
      },
      fail: function(res) {},
      complete: function(res) {},
    })
    console.log(e)
  },
  opreateAction() {
    const action = this.data.action;
    wx.request({
      url: `${globalData.api.baseUrl}${globalData.api.job.jobSet}?userid=${globalData.userInfo.id}&jobid=${this.data.resume.id}&action=${action}`,
      method: 'POST',
      success: (res) => {
        if (res.statusCode == 200) {
          this.getRemainNum();
          let applyNum = this.data.applyNum;
          if (action) {
            applyNum++;
            this.setData({
              applyNum
            })
          }
          this.data.animation.scale(0, 0).step();
          this.setData({
            animationData: this.data.animation.export()
          })
          this.recommendJob(() => {
            this.data.animation.scale(1, 1).step();
            setTimeout(() => {
              this.setData({
                animationData: this.data.animation.export()
              })
            }, 1000)

          });
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      complete: () => {

      }
    })
  },
  opreate(e) {
    const action = e.currentTarget.dataset.action;
    let code = '';
    this.setData({
      action
    })
    if (!this.data.resume.id) {
      return;
    }
    // if (action) {
    //   wx.requestSubscribeMessage({
    //     tmplIds: ['JK-9crLhdVRKJMiCPCx3dBV77VKbhVrvDjEne9fZ4_I'],
    //     success(res) {
    //       console.log(res)
    //      },
    //      fail:(error)=>{
    //        console.log(error)
    //      }
    //   })
      // wx.getSetting({
      //   success: (res) => {
      //     if (res.authSetting['scope.userInfo']) {
      //       wx.getUserInfo({
      //         success: function(res) {
      //           wx.login({
      //             success: (res) => {
      //               console.log(res)
      //               code = res.code;

      //             }
      //           })
      //           console.log(res)
      //         }
      //       })
      //     }else{
      //       this.setData({
      //         scopeShow:true
      //       })
      //     }
      //   }
      // })
    //}
  this.opreateAction();
  },
  recommendJob(callback) {
    wx.request({
      url: globalData.api.baseUrl + globalData.api.job.recommendJob + globalData.userInfo.id,
      method: 'GET',
      success: (res) => {
        console.log(res);
        if (res.statusCode == 200) {
          const resume = res.data.result;
          resume.be_listed = resume.be_listed > 0 ? this.data.listedArr[resume.be_listed-1]:'暂无';
          resume.company_type = resume.company_type > 0 ? this.data.companyTypeArr[resume.company_type-1]:'暂无';
          resume.staff_size = resume.staff_size > 0 ? this.data.staffSizeArr[resume.staff_size-1]:'暂无';
          callback && callback();
          this.setData({
            resume
          })
        }else{
          this.setData({
            resume:{}
          })
        }
      }
    })
  },
  getRemainNum(){
    wx.request({
      url: globalData.api.baseUrl + globalData.api.job.remainnumbers+globalData.userInfo.id,
      success:(res)=>{
        if(res.statusCode==200){
          this.setData({
            remain_numbers:res.data.result
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.getRemainNum();
    const date = new Date();
    this.setData({
      is_first_login:globalData.userInfo.is_first_login
    })
    let month = date.getMonth() + 1;
    month = month >= 10 ? month : '0' + month;
    let datey = date.getDate();
    datey = datey >= 10 ? datey : '0' + datey;
    let day = date.getDay();
    const arr = ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六']
    const time = `${month}月${datey}日  ${arr[day]}`;
    this.setData({
      time
    })
    this.recommendJob();
    this.setData({
      userName: globalData.userInfo.name
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.job.applynumbers+globalData.userInfo.id,
      success:(res)=>{
        if(res.statusCode==200){
          this.setData({
            applyNum:res.data.result
          })
        }
      }
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.message.noreadcount + '?msgType=user' + '&accountid=' + globalData.userInfo.id,
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            noreadcount:res.data.result
          })
        }
      }
    })
      wx.request({
        url: `${globalData.api.baseUrl}${globalData.api.job.userApplyList}${1}/${100}/${globalData.userInfo.id}/${false}/${true}`,
        success: (res) => {
          if (res.statusCode == 200) {
            this.setData({
              passNum: res.data.result.data.length
            })
          } else {
            wx.showToast({
              title: res.data.message,
              icon: 'none'
            })
          }
        },
        complete: () => {

        }
      })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    const animation = wx.createAnimation({
      duration: 1000,
      timingFunction: 'ease',
      transformOrigin: '0 0 0',
    })
    this.setData({
      animation
    })

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})