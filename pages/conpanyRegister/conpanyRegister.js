// pages/conpanyRegister/conpanyRegister.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cardId: '',
    industryList:[],
    industryDetailList:[],
    industryNum:0,
    industryShow:false,
    nextStep: false,
    agreementMaskShow: false,
    address: '',
    contact: '',
    industry: '',
    legalperson: '',
    license_file: '',
    licensecode: '',
    mobilephone: '',
    name: '',
    password: '',
    licenseImg: '',
    confirmPassword: '',
    isAgree: false,
    partment: '',
    codeText:'获取验证码',
    isSendCode:false,
    code:'',
    employeeName:'',
    companyKey:'',
    companyArr:[],
    companyId:'',
    companyShow:false,
    degreeList: [],
    majorList: [],
    majorDetailList:[],
    certificateList: [],
    titleList: [],
    departmentList: [],
    majorShow: false,
    majorNum: 0,
    major:'',
    degreeShow: false,
    degreeNum: 0,
    degree:'',
    departmentShow: false,
    departmentNum: 0,
    department:'',
    certificateShow: false,
    certificateNum: 0,
    certificate:'',
    titleShow: false,
    titleNum: 0,
    title:'',
    invitType:'',
    invitor:''
  },
  companyChoose(e){
    this.setData({
      companyId:e.currentTarget.dataset.id,
      companyShow:false,
      companyKey: e.currentTarget.dataset.name
    })
     wx.request({
      url: globalData.api.baseUrl + globalData.api.department.list + '1/100/' + this.data.companyId,
      method: 'GET',
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            departmentList: res.data.result.data
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      complete: () => {

      }
    })
  },
  companyKeyIn(e){
    const val = e.detail.value;
    if(!val){
      return;
    }
    this.setData({
      companyKey:val
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.company.searchKey+val+'?pageIndex=1&pageSize=100',
      success:(res)=>{
        if(res.statusCode==200){
          if (res.data.result.data.length>0){
            this.setData({
              companyArr: res.data.result.data,
              companyShow:true
            })
          }
         
        }
      }
    })
  },
  industryToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        industryShow: true
      })
    } else {
      this.setData({
        industryShow: false
      })
      this.setData({
        industry: this.data.industryDetailList[this.data.industryNum].name
      })
    }
  },
  industryChange(e) {
    this.getIndustry(this.data.industryList[e.detail.value[0]].id);
    this.setData({
      industryNum: e.detail.value[1]
    })
    this.setData({
      industry: this.data.industryDetailList[e.detail.value[1]].name
    })
  },
  titleToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        titleShow: true
      })
    } else {
      this.setData({
        titleShow: false
      })
      this.setData({
        title: this.data.titleList[this.data.titleNum].name
      })
    }
  },
  titleChange(e) {
    this.setData({
      titleNum: e.detail.value[0]
    })
    this.setData({
      title: this.data.titleList[e.detail.value[0]].name
    })
  },
  certificateToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        certificateShow: true
      })
    } else {
      this.setData({
        certificateShow: false
      })
      this.setData({
        certificate: this.data.certificateList[this.data.certificateNum].name
      })
    }
  },
  certificateChange(e) {
    this.setData({
      certificateNum: e.detail.value[0]
    })
    this.setData({
      certificate: this.data.certificateList[e.detail.value[0]].name
    })
  },
  departmentToggle(e) {
    if (!this.data.companyId || this.data.departmentList.length==0){
      wx.showToast({
        title: '公司不存在',
        duration:2000,
        icon:'none'
      })
      return;
    }
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        departmentShow: true
      })
    } else {
      this.setData({
        departmentShow: false
      })
      this.setData({
        department: this.data.departmentList[this.data.departmentNum].name
      })
    }
  },
  departmentChange(e) {
    this.setData({
      departmentNum: e.detail.value[0]
    })
    this.setData({
      department: this.data.departmentList[e.detail.value[0]].name
    })
  },
  degreeToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        degreeShow: true
      })
    } else {
      this.setData({
        degreeShow: false
      })
      this.setData({
        degree: this.data.degreeList[this.data.degreeNum].name
      })
    }
  },
  degreeChange(e) {
    this.setData({
      degreeNum: e.detail.value[0]
    })
    this.setData({
      degree: this.data.degreeList[e.detail.value[0]].name
    })
  },
  majorToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        majorShow: true
      })
    } else {
      this.setData({
        majorShow: false
      })
      this.setData({
        major: this.data.majorDetailList[this.data.majorNum].name
      })
    }
  },
  majorChange(e) {
    this.getMajor(this.data.majorList[e.detail.value[0]].id);
    this.setData({
      majorNum: e.detail.value[1]
    })
    this.setData({
      major: this.data.majorDetailList[e.detail.value[1]].name
    })
  },
  employeeNameIn(e){
    this.setData({
      employeeName:e.detail.value
    })
  },
  codeIn(e){
    this.setData({
      code:e.detail.value
    })
  },
  sendCode(){
    const data = this.data;
    const type = data.cardId == 1 ? 'company' : data.cardId == 3 ? 'employee' : 'user';
    if(data.isSendCode){
      return;
    }
    const ISPHONE = /^1[3456789]\d{9}$/;
    if (!ISPHONE.test(data.mobilephone)) {
      wx.showToast({
        title: '请输入正确的手机号',
        duration: 1500
      })
      return;
    }
    wx.request({
      url: globalData.api.baseUrl + globalData.api.common.registerCaptcha+type+'/'+data.mobilephone,
      method:'GET',
      success:(res)=>{
        if(res.statusCode==200){

        }else{
          wx.showToast({
            title: res.data.message,
            duration: 2000,
            icon: 'none'
          })
        }
      },
      complete:()=>{

      }
    })
    let time = 60;
    let inter = null;
    this.setData({
      isSendCode:true,
      codeText:'还剩60s'
    })
    inter = setInterval(()=>{
      time--;
      this.setData({
        codeText: `还剩${time}s`
      })
      if(time==0){
        clearInterval(inter);
        this.setData({
          isSendCode: false,
          codeText: '获取验证码'
        })
      }
    },1000)
  },
  
  next() {
    const data = this.data;
    if (data.cardId == 1) {
      if(!data.license_file){
        wx.showToast({
          title: '正在上传执照，请稍候',
          duration: 2000,
          icon:'none'
        })
        return;
      }
      if (!(data.name && data.licensecode  && data.legalperson && data.industry && data.address && data.contact)) {
        wx.showToast({
          title: '请输入完整信息',
          duration: 2000,
          icon:'none'
        })
        return;
      }

    }
    if(data.cardId==3){
      if(!(data.employeeName&&data.companyId&&data.department&&data.title)){
        wx.showToast({
          title: '请输入完整信息',
          duration: 2000
        })
        return;
      }
    }
    this.setData({
      nextStep: true
    })
  },
  agreeShow() {
    this.setData({
      agreementMaskShow: true
    })
  },
  agreeHide() {
    this.setData({
      agreementMaskShow: false
    })
  },
  nameChange(e) {
    this.setData({
      name: e.detail.value
    })
  },
  licensecodeIn(e) {
    this.setData({
      licensecode: e.detail.value
    })
  },
  legalpersonIn(e) {
    this.setData({
      legalperson: e.detail.value
    })
  },
  addressIn(e) {
    this.setData({
      address: e.detail.value
    })
  },
  contactIn(e) {
    this.setData({
      contact: e.detail.value
    })
  },
  mobilephoneIn(e) {
    this.setData({
      mobilephone: e.detail.value
    })
  },
  passwordIn(e) {
    this.setData({
      password: e.detail.value
    })
  },
  confirmPasswordIn(e) {
    this.setData({
      confirmPassword: e.detail.value
    })
  },
  toggleAgree() {
    const isAgree = !this.data.isAgree;
    this.setData({
      isAgree
    })
  },
  upload() {
    wx.chooseImage({
      success: (res) => {
        const IMGURL = res.tempFilePaths[0];
        this.setData({
          licenseImg: IMGURL
        })
        wx.uploadFile({
          url: globalData.api.baseUrl + globalData.api.common.uploadFile,
          filePath: IMGURL,
          name: 'name',
          success: (res) => {
            if(res.statusCode==200){
              this.setData({
                license_file:JSON.parse(res.data).result
              })
            }
          },
          fail: (res) => {
            console.log(res);
          }
        })
      },
    })
  },
  register() {
    const ISPHONE = /^1[3456789]\d{9}$/;
    const data = this.data;
    if (!ISPHONE.test(data.mobilephone)) {
      wx.showToast({
        title: '请输入正确的手机号',
        duration: 1500
      })
      return;
    }
    if (!data.password) {
      wx.showToast({
        title: '请设置密码',
        duration: 1500
      })
      return;
    }
    if (data.password !== data.confirmPassword) {
      wx.showToast({
        title: '两次密码不一致，请重新输入',
        duration: 1500
      })
      return;
    }
    if (!data.code) {
      wx.showToast({
        title: '请输入验证码',
        duration: 1500
      })
      return;
    }
    if (!data.isAgree) {
      wx.showToast({
        title: '请勾选同意协议',
        duration: 1500
      })
      return;
    }
    wx.showLoading({
      title: '注册中...',
    })
    if (data.cardId == 1) {
      wx.request({
        url: globalData.api.baseUrl + globalData.api.company.register,
        method: 'POST',
        data: {
          "address": data.address,
          "contact": data.contact,
          "industry_id": data.industryDetailList[data.industryNum].id,
          "legalperson": data.legalperson,
          "license_file": data.license_file,
          "licensecode": data.licensecode,
          "mobilephone": data.mobilephone,
          "name": data.name,
          "password": data.password,
          captcha:data.code
        },
        success: (res) => {
          wx.hideLoading();
          if (res.statusCode == 200) {
            wx.showToast({
              title: '注册成功',
            })
            data.invitor && this.companyInvite(res.data.result.id,parseInt(data.invitor));
            globalData.userInfo = res.data.result;
            wx.redirectTo({
              url: '/pages/companyHome/companyHome',
            })
          } else {
            wx.showToast({
              title: res.data.message,
              duration: 2000,
              icon: 'none'
            })
          }
        },
        fail: (error) => {
          wx.hideLoading();
          wx.showToast({
            title: error,
            duration: 2000,
            icon: 'none'
          })
        },
        complete: () => {
         
        }
      })
      return;
    }
    if(data.cardId==4){
      wx.request({
        url: globalData.api.baseUrl + globalData.api.user.register,
        method:'POST',
        data:{
          "password": data.password,
          "telephone": data.mobilephone,
          captcha: data.code
        },
        success:(res)=>{
          wx.hideLoading();
          if(res.statusCode==200){
            wx.showToast({
              title: '注册成功',
            })
            data.invitor && this.userInvite(res.data.result.id,parseInt(data.invitor));
            globalData.userInfo = res.data.result;
            wx.redirectTo({
              url: '/pages/personalHome/personalHome' ,
            })
          }else{
            wx.showToast({
              title: res.data.message,
              duration: 2000,
              icon: 'none'
            })
          }
        },
        fail:(error)=>{
          wx.hideLoading();
          wx.showToast({
            title: error,
            duration: 2000,
            icon: 'none'
          })

        },
        complete:()=>{
         
        }
      })
      return;
    }
    if(data.cardId==3){
      wx.request({
        url: globalData.api.baseUrl + globalData.api.employee.register,
        method:'POST',
        data:{
          "captcha": data.code,
          // "certificateid": data.certificateList[data.certificateNum].id,
          "companyid": data.companyId,
          // "degreeid": data.degreeList[data.degreeNum].id,
          "depid": data.departmentList[data.departmentNum].id,
          // "majorid": data.majorDetailList[data.majorNum].id,
          "name": data.employeeName,
          "password": data.password,
          "telephone": data.mobilephone,
          "titleid": data.titleList[data.titleNum].id
        },
        success:(res)=>{
          wx.hideLoading();
          if(res.statusCode==200){
            wx.showToast({
              title: '注册成功',
            })
            globalData.userInfo = res.data.result;
            wx.redirectTo({
              url: '/pages/staffCommu/staffCommu' ,
            })
          }else{
            wx.showToast({
              title: res.data.message,
              duration: 2000,
              icon: 'none'
            })
          }
        },
        fail:(error)=>{
          wx.hideLoading();
          wx.showToast({
            title: error,
            duration: 2000,
            icon: 'none'
          })
        },
        complete:()=>{
          
        }
      })
      return;
    }
  },
  getIndustry(id){
    wx.request({
      url: globalData.api.baseUrl+globalData.api.dict.industry+id,
      success:(res)=>{
        if(res.statusCode==200){
          this.setData({
            industryDetailList:res.data.result
          })
        }
      }
    })
  },
  getMajor(id){
    wx.request({
      url: globalData.api.baseUrl + globalData.api.dict.majorList + id,
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            majorDetailList: res.data.result
          })
        }
      }
    })
  },
  companyInvite(companyid, invitor){
    wx.request({
      url: globalData.api.baseUrl+globalData.api.company.invite,
      method:'POST',
      data:{
        companyid: companyid,
        invitor: parseInt(invitor)
      },
      header:{
        'content-type': 'application/x-www-form-urlencoded'
      },
      success:(res)=>{
        if(res.statusCode==200){
          
        }
      }
    })
  },
  userInvite(userid, invitor){
    wx.request({
      url: globalData.api.baseUrl+globalData.api.user.invite,
      method:'POST',
      data:{
        userid: userid,
        invitor: parseInt(invitor)
      },
      header:{
        'content-type': 'application/x-www-form-urlencoded'
      },
      success:(res)=>{
        if(res.statusCode==200){

        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const invitType = options.invitType;
    const invitor = options.invitor;
    if (invitType && invitor){
      globalData.cardId = invitType==1?1:4;
      this.setData({
        invitType,
        invitor
      })
    }
    this.setData({
      cardId: globalData.cardId
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.dict.degreeList,
      method: 'GET',
      success: (res) => {
        if (res.statusCode == 200) {
          console.log(res)
          this.setData({
            degreeList: res.data.result
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      complete: () => {

      }
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.dict.majorclass,
      method: 'GET',
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            majorList: res.data.result
          })
          this.getMajor(res.data.result[0].id)
        } else {
          wx.showToast({
            title: res.data.message,
            icon:'none'
          })
        }
      },
      complete: () => {

      }
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.dict.certificateList,
      method: 'GET',
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            certificateList: res.data.result
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon:'none'
          })
        }
      },
      complete: () => {

      }
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.dict.titleList,
      method: 'GET',
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            titleList: res.data.result
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon:'none'
          })
        }
      },
      complete: () => {

      }
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.dict.industryclass,
      method: 'GET',
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            industryList: res.data.result
          })
          this.getIndustry(res.data.result[0].id);
        } else {
          wx.showToast({
            title: res.data.message,
            icon:'none'
          })
        }
      },
      complete: () => {

      }
    })
   
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

})