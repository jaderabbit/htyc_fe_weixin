// pages/customerService/customerService.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tiem:'',
    content:''
  },
  contentIn(e){
    this.setData({
      content:e.detail.value
    })
  },
  send(){
    if(!this.data.content){
      wx.showToast({
        title: '请输入内容',
        icon:'none'
      })
      return;
    }
    let type = globalData.cardId == 1 ? 'company' : globalData.cardId == 3 ?'employee':'user';
    wx.request({
      url: globalData.api.baseUrl + globalData.api.comment.add,
      method:'POST',
      data:{
        "accountid": globalData.userInfo.id,
        "text": this.data.content,
        "type": type
      },
      success:(res)=>{
        if(res.statusCode==200){
          wx.showToast({
            title: '提交成功',
          })
          this.setData({
            content:""
          })
        }else{
          wx.showToast({
            title: res.data.message,
          })
        }
      },
      fail:(error)=>{
        wx.showToast({
          title: error,
          icon:'none'
        })
      }
    })
  },
  callPhone(e){
    const tel = e.currentTarget.dataset.tel;
    wx.makePhoneCall({
      phoneNumber: tel,
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const date = new Date();
    let month = date.getMonth()+1;
    month = month>=10?month:'0'+month;
    let datey = date.getDate();
    datey = datey>=10?datey:'0'+datey;
    let day = date.getDay();
    const arr = ['星期日','星期一','星期二','星期三','星期四','星期五','星期六']
    const time = `${month}月${datey}日  ${arr[day]}`;
    this.setData({
      time
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})