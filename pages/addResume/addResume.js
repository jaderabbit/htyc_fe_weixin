// pages/addResume/addResume.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tems:[],
    file:'',
    name:'',
    temFile:'',
    url:''
  },

nameIn(e){
  this.setData({
    name:e.detail.value
  })
},
  upload() {
    wx.chooseMessageFile({
      count:1,
      type:'file',
      success: (res) => {
        console.log(res)
        const IMGURL = res.tempFiles[0].path;
        this.setData({
          temFile: IMGURL
        })
        wx.uploadFile({
          url: globalData.api.baseUrl + globalData.api.common.fileupload,
          filePath: IMGURL,
          name: 'name',
          success: (res) => {
            if (res.statusCode == 200) {
              this.setData({
                file: JSON.parse(res.data).result
              })
            }
          },
          fail: (res) => {
            console.log(res);
          }
        })
      },
    })
  },
 add(){
   const data = this.data;
   if(!data.file){
     wx.showToast({
       title: '请上传简历文件',
       duration:1500
     })
     return;
   }
   if(!data.name){
     wx.showToast({
       title: '请为你的简历命名',
       duration:1500
     })
     return;
   }
   wx.showLoading({
     title: '上传中...',
   })
   wx.request({
     url: globalData.api.baseUrl+globalData.api.resume.add,
     method:'POST',
     data:{
       "description": "测试",
       "resume_file": data.file,
       "subject": data.name,
       "userid": globalData.userInfo.id
     },
     success:(res)=>{
       wx.hideLoading();
       if(res.statusCode==200){
         wx.showToast({
           title: '上传成功',
         })
         wx.navigateBack({
           delta: 1,
         })
       }else{
         wx.showToast({
           title: res.data.message,
           duration:2000,
           icon:'none'
         })
       }
     },
     complete:()=>{
       
     },
     fail:(error)=>{
       wx.hideLoading();
       wx.showToast({
         title: error,
         duration: 2000,
         icon: 'none'
       })
     }
   })
 },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      url:'https://futurenext.com.cn/file/resume/addresume.html?id='+globalData.userInfo.id
    })
    // wx.request({
    //   url: globalData.api.baseUrl + globalData.api.resume.template,
    //   success:(res)=>{
    //     if(res.statusCode==200){
    //       this.setData({
    //         tems:res.data.result
    //       })
    //     }
    //   }
    // })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})