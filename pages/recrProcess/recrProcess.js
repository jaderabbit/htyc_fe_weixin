// pages/recrProcess/recrProcess.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    procedure:[],
    job:{}
  },
  addTask(){
    wx.navigateTo({
      url: '/pages/addTask/addTask',
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    })
  },
  addJob(e){
    const status = globalData.userInfo.status;
    const pass = e.currentTarget.dataset.pass;
    let title = '';
    switch(status){
      case 0:
        title = '您的企业正在审核中，请耐心等待';
        break;
      case 1:
        title = '您的企业审核未通过';
        break;
      case 4:
        title = '您的企业已被临时封号';
        break;
      case 5:
        title = '您的企业已被永久封号';
        break;
      case 6:
        title = '您的企业已被删除';
        break;
      default:
        title = '';
        break;
    }
    if(title){
      wx.showToast({
        title: title,
        icon:'none'
      })
      return;
    }
    const data = this.data;
    const job = data.job;
    if(!pass&&data.procedure.length==0){
      wx.showToast({
        title: '请先添加流程',
        duration:1500
      })
      return;
    }
    wx.showLoading({
      title: '请求中...',
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.job.add,
      method:'POST',
      data:{
        "age_lower": job.age_lower,
        "age_upper": job.age_upper,
        "companyid": globalData.userInfo.id,
        "degreeid": job.degreeid,
        "location": job.location,
        "majorid": job.majorid,
        "name": job.name,
        "pay_lower": job.pay_lower,
        "pay_upper": job.pay_upper,
        "position_label": job.position_label,
        "procedures": data.procedure,
        "description": job.description,
        "valid_enddate": job.valid_enddate,
        "valid_startdate": job.valid_startdate,
        "provinceid": job.provinceid,
        "type": job.type,
        "cityid":job.cityid,
        "work_age_lower": job.work_age_lower,
        "work_age_upper": job.work_age_upper,
        "fringe_benefits": job.fringe_benefits,
      },
      success:(res)=>{
        wx.hideLoading();
        if(res.statusCode==200){
          wx.showToast({
            title: '新增成功',
          })
          wx.navigateTo({
            url: '/pages/postManage/postManage',
          })
        }else{
          wx.showToast({
            title: res.data.message,
            duration:2000,
            icon:'none'
          })
        }
      },
      complete:()=>{
      
      },
      fail:(error)=>{
        wx.hideLoading();
        wx.showToast({
          title: error,
          duration: 2000,
          icon: 'none'
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const procedure = this.data.procedure;
    let job = this.data.job;
    try {
      const procedureJson = wx.getStorageSync('procedure');
      if (procedureJson) {
        procedure.push(JSON.parse(procedureJson));
        this.setData({
          procedure
        })
        wx.removeStorageSync('procedure');
      }

    } catch (e) {

    }
    try {
      const jobJson = wx.getStorageSync('job');
      if (jobJson) {
        job = JSON.parse(jobJson);
        this.setData({
          job
        })
        wx.removeStorageSync('job')
      }

    } catch (e) {

    }

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})