// pages/applyMustAnswer/applyMustAnswer.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    asks: [],
    result: []
  },
  answerIn(e) {
    const num = e.currentTarget.dataset.num;
    const result = this.data.result;
    result[num].answer = e.detail.value.trim();
    this.setData({
      result
    })
  },
  save() {
    const data = this.data;
    if (data.result.some(item => { return item.answer === '' })) {
      wx.showToast({
        title: '请填写完答案',
        icon: 'none'
      })
      return;
    }
    wx.showLoading({
      title: '提交中...',
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.user.answer,
      method: 'POST',
      data: {
        "userid": globalData.userInfo.id,
        "results": data.result
      },
      success: (res) => {
        wx.hideLoading();
        if (res.statusCode == 200) {
          wx.navigateBack({
            delta: 1,
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      fail: (error) => {
        wx.hideLoading();
        wx.showToast({
          title: error,
          icon: 'none'
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中...',
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.user.ask,
      success: (res) => {
        if (res.statusCode == 200) {
          const arr = res.data.result;
          const result = [];
          arr.map(item => {
            let obj = {
              "answer": '',
              "askid": item.id
            }
            result.push(obj);
          })
          this.setData({
            asks: arr,
            result
          })
        
          wx.request({
            url: globalData.api.baseUrl + globalData.api.user.answer + '/' + globalData.userInfo.id,
            success: (res) => {
              if (res.statusCode == 200) {
                wx.hideLoading();
                const arr = res.data.result;
                const result = this.data.result;
                if (arr.length > 0) {
                  arr.map((item, index) => {
                    result[index].answer = item.answer;
                  })
                  this.setData({
                    result
                  })
                }
              }
            }
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
   
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})