// pages/chooseSchool/chooseSchool.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    key: ''
  },
  comfrin(){
    try {
      wx.setStorageSync('school', this.data.key);
      wx.navigateBack({
        delta: 1,
      })
    } catch (e) {

    }
  },
  dele() {
    this.setData({
      list: [],
      key: ''
    })
  },
  choose(e) {
    const school = e.currentTarget.dataset.name;
    try {
      wx.setStorageSync('school', school);
      wx.navigateBack({
        delta: 1,
      })
    } catch (e) {

    }
  },
  keyIn(e) {
    const key = e.detail.value;
   this.setData({
     key
   })
    if (key) {
      let schoolUrl = '';
      if (globalData.cardId == 3) {
        schoolUrl = globalData.api.employee.schoolSearch;
      } else if (globalData.cardId == 4) {
        schoolUrl = globalData.api.user.schoolSearch;
      }
      wx.request({
        url: globalData.api.baseUrl + schoolUrl + key,
        success: (res) => {
          if (res.statusCode == 200) {
            this.setData({
              list: res.data.result
            })
          } else {
            wx.showToast({
              title: res.data.message,
              icon: 'none'
            })
          }
        },
        fail: (error) => {
          wx.showToast({
            title: error,
            icon: 'none'
          })
        }
      })
    }
    setTimeout(() => {

    }, 20)
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})