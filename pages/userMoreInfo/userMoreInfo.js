// pages/userMoreInfo/userMoreInfo.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    certificate: [],
    edu: [],
    workexp: []
  },
  toWorkexp(e) {
    const id = e.currentTarget.dataset.id;
    const num = e.currentTarget.dataset.num;
    let url = '';
    if (id) {
      try {
        wx.setStorageSync('workexp', JSON.stringify(this.data.workexp[num]))
      } catch (e) {

      }
      url = `/pages/userWokrep/userWokrep?id=${id}`
    } else {
      url = '/pages/userWokrep/userWokrep'
    }
    wx.navigateTo({
      url: url,
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    })
  },
  toCer(e) {
    const id = e.currentTarget.dataset.id;
    const num = e.currentTarget.dataset.num;
    let url = '';
    if (id) {
      try {
        wx.setStorageSync('cer', JSON.stringify(this.data.certificate[num]))
      } catch (e) {

      }
      url = `/pages/userCer/userCer?id=${id}`
    } else {
      url = '/pages/userCer/userCer'
    }
    wx.navigateTo({
      url: url,
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    })
  },
  toEdu(e) {
    const id = e.currentTarget.dataset.id;
    const num = e.currentTarget.dataset.num;
    let url = '';
    if (id) {
      try {
        wx.setStorageSync('edu', JSON.stringify(this.data.edu[num]))
      } catch (e) {

      }
      url = `/pages/userEdu/userEdu?id=${id}`
    } else {
      url = '/pages/userEdu/userEdu'
    }
    wx.navigateTo({
      url: url,
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    let cerUrl='',eduUrl='',wrokexpUrl='';
    if(globalData.cardId==3){
      cerUrl = globalData.api.employee.certificate;
      eduUrl = globalData.api.employee.edu;
      wrokexpUrl = globalData.api.employee.workexp;
    }else if(globalData.cardId==4){
      cerUrl = globalData.api.user.certificate;
      eduUrl = globalData.api.user.edu;
      wrokexpUrl = globalData.api.user.workexp;
    }
    wx.request({
      url: globalData.api.baseUrl + cerUrl + '/' + globalData.userInfo.id,
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            certificate: res.data.result
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      fail: (error) => {
        wx.showToast({
          title: error,
          icon: 'none'
        })
      }
    })
    wx.request({
      url: globalData.api.baseUrl + eduUrl + '/' + globalData.userInfo.id,
      success: (res) => {
        if (res.statusCode == 200) {
          const edu = res.data.result;
          edu.map((item)=>{
            item.startYear = item.admission_date.slice(0,4);
            item.endYear = item.graduation_date.slice(0,4);
          })
          this.setData({
            edu
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      fail: (error) => {
        wx.showToast({
          title: error,
          icon: 'none'
        })
      }
    })
    wx.request({
      url: globalData.api.baseUrl + wrokexpUrl + '/' + globalData.userInfo.id,
      success: (res) => {
        if (res.statusCode == 200) {
          const workexp = res.data.result;
          workexp.map((item) => {
            item.startDate = item.employment_date ? item.employment_date.slice(0, 10).replace(/-/g, '.') : item.employment_date;
            item.endDate = item.leave_date ? item.leave_date.slice(0, 10).replace(/-/g, '.') : item.leave_date;
          })
          this.setData({
            workexp
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      fail: (error) => {
        wx.showToast({
          title: error,
          icon: 'none'
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})