// pages/companyMessage/companyMessage.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo:{},
    isInner:false,
    pageIndex: 1,
    pageSize: 10,
    sysPageIndex:1,
    sysPageSize:10,
    sysMessages: [],
    apply:[],
    company:[],
    cardId: '',
    type:1,//1：系统消息2：普通消息
    delBtnWidth: 144,
    startX: "",
    startY: ""
  },
  touchS: function (e) {
    if (e.touches.length == 1) {
      this.setData({
        //设置触摸起始点水平方向位置
        startX: e.touches[0].clientX,
        startY: e.touches[0].clientY
      });
    }
  },
  touchM: function (e) {
    if (e.touches.length == 1) {
      //手指移动时水平方向位置
      var moveX = e.touches[0].clientX;
      var moveY = e.touches[0].clientY;
      //手指起始点位置与移动期间的差值
      var disX = this.data.startX - moveX;
      var disY = this.data.startY - moveY;
      if(Math.abs(disY)>Math.abs(disX)){
        return;
      }
      var delBtnWidth = this.data.delBtnWidth;
      var txtStyle = "";
      if (disX == 0 || disX < 0) { //如果移动距离小于等于0，说明向右滑动，文本层位置不变
        txtStyle = "left:0px";
      } else if (disX > 0) { //移动距离大于0，文本层left值等于手指移动距离
        txtStyle = "left:-" + disX + "px";
        if (disX >= delBtnWidth) {
          //控制手指移动距离最大值为删除按钮的宽度
          txtStyle = "left:-" + delBtnWidth + "px";
        }
      }
      //获取手指触摸的是哪一项
      var index = e.currentTarget.dataset.index;
      var sysMessages = this.data.sysMessages;
      sysMessages[index].txtStyle = txtStyle;
      //更新列表的状态
      this.setData({
        sysMessages: sysMessages
      });
    }
  },
  touchE: function (e) {
    if (e.changedTouches.length == 1) {
      //手指移动结束后水平位置
      var endX = e.changedTouches[0].clientX;
      var endY = e.changedTouches[0].clientY;
      //触摸开始与结束，手指移动的距离
      var disX = this.data.startX - endX;
      var disY = this.data.startY - endY;
      if(Math.abs(disY)>Math.abs(disX)){
        return;
      }
      var delBtnWidth = this.data.delBtnWidth;
      //如果距离小于删除按钮的1/2，不显示删除按钮
      var txtStyle = disX > delBtnWidth / 2 ? "left:-" + delBtnWidth + "px" : "left:0px";
      //获取手指触摸的是哪一项
      var index = e.currentTarget.dataset.index;
      var sysMessages = this.data.sysMessages;
      sysMessages[index].txtStyle = txtStyle;
      //更新列表的状态
      this.setData({
        sysMessages: sysMessages
      });
    }
  },
  //获取元素自适应后的实际宽度
  getEleWidth: function (w) {
    var real = 0;
    try {
      var res = wx.getSystemInfoSync().windowWidth;
      var scale = (750 / 2) / (w / 2);
      real = Math.floor(res / scale);
      return real;
    } catch (e) {
      return false;
      // Do something when catch error
    }
  },
  initEleWidth: function () {
    var delBtnWidth = this.getEleWidth(this.data.delBtnWidth);
    this.setData({
      delBtnWidth: delBtnWidth
    });
  },
  //点击删除按钮事件
  delItem: function (e) {
    //获取列表中要删除项的下标
    var index = e.currentTarget.dataset.index;
    var channelid = e.currentTarget.dataset.channelid;
    var sysMessages = this.data.sysMessages;
    let msgType = this.data.cardId == 1 ? 'company' : this.data.cardId == 3 ? 'employee' :'user';
    let url = this.data.type == 1 ? `${globalData.api.baseUrl}${globalData.api.message.deletesys}?msgType=${msgType}&ids=${[sysMessages[index].id]}` : `${globalData.api.baseUrl}${globalData.api.message.deleteusermsg}?msgType=${msgType}&ids=${[sysMessages[index].id]}`;
    if (channelid ){
      if (this.data.cardId == 1){
        msgType = 'companyemployee';
      }
      url = `${globalData.api.baseUrl}${globalData.api.channel.deletemsg}?msgType=${msgType}&ids=${[sysMessages[index].id]}`;
    }
    wx.request({
      url: url,
      method: 'DELETE',
      success: (res) => {
        if (res.statusCode == 200) {
          //移除列表中下标为index的项
          sysMessages.splice(index, 1);
          //更新列表的状态
          this.setData({
            sysMessages: sysMessages
          });
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      fail: (error) => {
        wx.showToast({
          title: error,
          icon: 'none'
        })
      }
    })

  },
  answer(e){
    const num = e.currentTarget.dataset.num;
    const userid = e.currentTarget.dataset.userid;
    try{
      wx.setStorageSync('taskQue', JSON.stringify(this.data.company[num].job))
    }catch(e){

    }
    wx.navigateTo({
      url: '/pages/userAnswerQue/userAnswerQue?userid=' + userid,
    })
  },
  checkResult(e){
    const jobid = e.currentTarget.dataset.jobid;
    const userid = e.currentTarget.dataset.userid;
    wx.navigateTo({
      url: '/pages/comMsgDetail/comMsgDetail?jobid='+jobid+'&userid='+userid,
    })
  },
  toDetail(e){
    const data = this.data;
    const dataset = e.currentTarget.dataset;
    let url = '';
    if (data.type==1){
      wx.setStorageSync('MSG', JSON.stringify(data.sysMessages[dataset.num]));
      url = '/pages/messageDetail/messageDetail?type=' + this.data.type;
    }else{
      url = `/pages/messageDetail/messageDetail?type=${data.type}&user=${dataset.user}&company=${dataset.company}&employee=${dataset.employee}&channelid=${dataset.channelid}`
    }
    wx.navigateTo({
      url: url,
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    })
  },
  changeType(e){
    const type = e.target.dataset.type
    this.setData({
      type,
      sysMessages:[],
      sysPageIndex:1,
      pageIndex:1
    })
    if(type==1){
      this.getSysMessage();
    }else{
      this.getMessage()
    }
  },
  getSysMessage() {
    const data = this.data;
    let type = globalData.cardId == 1 ? 'company' : globalData.cardId == 3 ? 'employee' : 'user';
    let id = data.cardId == 1 ? globalData.userInfo.user.id : globalData.userInfo.id;
    wx.request({
      url: `${globalData.api.baseUrl}${globalData.api.message.list}${data.sysPageIndex}/${data.sysPageSize}/${id}/${type}`,
      success: (res) => {
        if (res.statusCode == 200) {
          const arr = res.data.result.data;
          let sysMessages = data.sysMessages;
          if(arr){
            sysMessages = sysMessages.concat(arr);
          }
          this.setData({
            sysMessages
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      complete: () => {

      }
    })
  },
  getMessage(){
    const data = this.data;
    if(data.cardId==1){
      if(!data.isInner){
        wx.request({
          url: `${globalData.api.baseUrl}${globalData.api.message.companyEmp}${data.pageIndex}/${data.pageSize}/${globalData.userInfo.user.id}`,
          success: (res) => {
            if (res.statusCode == 200) {
              const arr = res.data.result.data;
              let sysMessages = data.sysMessages;
              if (arr) {
                sysMessages = sysMessages.concat(arr)
              }
              this.setData({
                sysMessages
              })
            } else {
              wx.showToast({
                title: res.data.message,
                icon: 'none'
              })
            }
          },
          complete: () => {

          }
        })
        wx.request({
          url: `${globalData.api.baseUrl}${globalData.api.message.companyUser}${data.pageIndex}/${data.pageSize}/${globalData.userInfo.user.id}`,
          success: (res) => {
            if (res.statusCode == 200) {
              const arr = res.data.result.data;
              let sysMessages = data.sysMessages;
              if (arr) {
                sysMessages = sysMessages.concat(arr)
              }
              this.setData({
                sysMessages
              })
            } else {
              wx.showToast({
                title: res.data.message,
                icon: 'none'
              })
            }
          },
          complete: () => {

          }
        })
      }else{
        wx.request({
          url: `${globalData.api.baseUrl}${globalData.api.channel.employeemessage}${globalData.userInfo.user.id}?pageIndex=${data.pageIndex}&pageSize=${data.pageSize}`,
          success: (res) => {
            if (res.statusCode == 200) {
              const arr = res.data.result.data;
              let sysMessages = data.sysMessages;
              if (arr) {
                sysMessages = sysMessages.concat(arr)
              }
              this.setData({
                sysMessages
              })
            } else {
              wx.showToast({
                title: res.data.message,
                icon: 'none'
              })
            }
          },
          complete: () => {

          }
        })

      }
      
      
      wx.request({
        url: `${globalData.api.baseUrl}${globalData.api.job.userlistbycompanyid}1/10/${globalData.userInfo.id}?status=${0}`,
        success:(res)=>{
          if(res.statusCode==200){
            this.setData({
              apply:res.data.result.data
            })
          }
        }
      })
      return;
    }
    if(data.cardId==3){
      wx.request({
        url: `${globalData.api.baseUrl}${globalData.api.message.employee}${data.pageIndex}/${data.pageSize}/${globalData.userInfo.id}`,
        success: (res) => {
          if (res.statusCode == 200) {
            const arr = res.data.result.data;
            let sysMessages = data.sysMessages;
            if (arr) {
              sysMessages = sysMessages.concat(arr)
            }
            this.setData({
              sysMessages
            })
          } else {
            wx.showToast({
              title: res.data.message,
              icon: 'none'
            })
          }
        },
        complete: () => {

        }
      })
      wx.request({
        url: `${globalData.api.baseUrl}${globalData.api.channel.empmessage}${globalData.userInfo.id}?pageIndex=${data.pageIndex}&pageSize=${data.pageSize}`,
        success: (res) => {
          if (res.statusCode == 200) {
            const arr = res.data.result.data;
            let sysMessages = data.sysMessages;
            if (arr) {
              sysMessages = sysMessages.concat(arr)
            }
            this.setData({
              sysMessages
            })
          } else {
            wx.showToast({
              title: res.data.message,
              icon: 'none'
            })
          }
        },
        complete: () => {

        }
      })
      return;
    }
    if(data.cardId==4){
      wx.request({
        url: `${globalData.api.baseUrl}${globalData.api.message.user}${data.pageIndex}/${data.pageSize}/${globalData.userInfo.id}`,
        success: (res) => {
          if (res.statusCode == 200) {
            const arr = res.data.result.data;
            let sysMessages = data.sysMessages;
            if (arr) {
              sysMessages = sysMessages.concat(arr)
            }
            this.setData({
              sysMessages
            })
          } else {
            wx.showToast({
              title: res.data.message,
              icon: 'none'
            })
          }
        },
        complete: () => {

        }
      })
      wx.request({
        url: `${globalData.api.baseUrl}${globalData.api.job.beforeanswer}${globalData.userInfo.id}/?pageIndex=${data.pageIndex}&pageSize=${data.pageSize}`,
        success: (res) => {
          if (res.statusCode == 200) {
            this.setData({
              company: res.data.result.data
            })
          } else {
            wx.showToast({
              title: res.data.message,
              icon: 'none'
            })
          }
        },
        complete: () => {

        }
      })
      return;
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      cardId: globalData.cardId,
      isInner:globalData.isInner,
      userInfo:globalData.userInfo
    })
    this.getSysMessage();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    this.initEleWidth();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    const data = this.data;
    if(globalData.cardId==1){
      wx.request({
        url: `${globalData.api.baseUrl}${globalData.api.job.userlistbycompanyid}1/10/${globalData.userInfo.id}?status=${0}`,
        success: (res) => {
          if (res.statusCode == 200) {
            this.setData({
              apply: res.data.result.data
            })
          }
        }
      })
    }
    if(globalData.cardId==4){
      wx.request({
        url: `${globalData.api.baseUrl}${globalData.api.job.beforeanswer}${globalData.userInfo.id}/?pageIndex=${data.pageIndex}&pageSize=${data.pageSize}`,
        success: (res) => {
          if (res.statusCode == 200) {
            this.setData({
              company: res.data.result.data
            })
          } else {
            wx.showToast({
              title: res.data.message,
              icon: 'none'
            })
          }
        },
        complete: () => {

        }
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    const type = this.data.type;
    if(type==1){
      let sysPageIndex =this.data.sysPageIndex;
      sysPageIndex++;
      this.setData({
        sysPageIndex
      })
      this.getSysMessage();
    }else{
      let pageIndex = this.data.pageIndex;
      pageIndex++;
      this.setData({
        pageIndex
      })
      this.getMessage();
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})