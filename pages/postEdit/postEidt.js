// pages/postEdit/postEidt.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    workAgeShow: false,
    work_age_arr: [{
      low: '不限',
      up: '不限'
    }, {
      low: 0,
      up: 1
    }, {
      low: 1,
      up: 2
    }, {
      low: 2,
      up: 3
    }, {
      low: 3,
      up: 4
    }],
    workAgeNum: 0,
    workAge: '',
    workAgeInUp: '',
    workAgeInLow: '',
    ageShow: false,
    age_arr: [{
      low: '不限',
      up: '不限'
    }, {
      low: 16,
      up: 20
    }, {
      low: 20,
      up: 25
    }, {
      low: 25,
      up: 30
    }, {
      low: 30,
      up: 35
    }, {
      low: 35,
      up: 40
    }, {
      low: 40,
      up: 45
    }, {
      low: 45,
      up: 50
    }, {
      low: 50,
      up: 55
    }, {
      low: 55,
      up: 60
    }, {
      low: 60,
      up: 65
    }, {
      low: 65,
      up: 70
    }],
    ageNum: 0,
    age: '',
    ageInUp: '',
    ageInLow: '',
    loctionDetailList: [],
    loctionList: [],
    loctionClassShow: true,
    loctionShow: false,
    province: null,
    city: null,
    loctionNum: NaN,
    salaryShow: false,
    types: [{
      id: 1,
      name: '全职'
    }, {
      id: 2,
      name: '兼职'
    }, {
      id: 3,
      name: '实习'
    }],
    type: '',
    salaryArr: [{
      low: '不限',
      up: '不限'
    }, {
      low: 1000,
      up: 2000
    }, {
      low: 2000,
      up: 5000
    }, {
      low: 5000,
      up: 10000
    }],
    educationShow: false,

    marjorMask: false,
    majorDetailList: [],
    name: '',
    loction: '',
    degreeList: [],
    majorList: [],
    salaryNum: 0,
    salary: '',
    salaryInUp: '',
    salaryInLow: '',
    degree: '',
    degreeNum: 0,
    majorNum: 0,
    major: '',
    joblabel: '',
    jobdesc: '',
    startDate: '',
    endDate: '',
    labels: [],
    labelShow: false,
    hint:`请描述一下您的招聘岗位吧~   详细清晰的职位描述能获得更多求职者的关注；不要填写违反广告法及包含歧视的词语，不要填写联系方式。  1、工作内容   2、任务要求  3、特别说明`,
    fringe_benefits: [{ name: '五险一金', isChoose: false }, { name: '五险', isChoose: false }, { name: '双休', isChoose: false }]
  },
  fringe_benefitsChoose(e){
    const fringe_benefits = this.data.fringe_benefits;
    const num = e.currentTarget.dataset.num;
    fringe_benefits[num].isChoose = !fringe_benefits[num].isChoose;
    this.setData({
      fringe_benefits
    })
  },
  joblabelIn(e){
    this.setData({
      joblabel:e.detail.value
    })
  },
  loctionToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        loctionShow: true,
        loctionNum: NaN
      })
    } else {
      if (!this.data.province) {
        wx.showToast({
          title: '请选择省份',
          icon: 'none'
        })
        return;
      }
      if (!this.data.city) {
        wx.showToast({
          title: '请选择城市',
          icon: 'none'
        })
        return;
      }
      this.setData({
        loctionShow: false,
        loctionClassShow: true,
        loction: this.data.province.name != '不限' ? this.data.province.name+ '/' + this.data.city.name:'不限'
      })
    }

  },

  loctionChoose(e) {
    const num = e.currentTarget.dataset.num;
    const loctionDetailList = this.data.loctionDetailList;
    this.setData({
      city: loctionDetailList[num],
      loctionNum: num
    })
  },
  loctionNavClick() {
    this.setData({
      loctionClassShow: true
    })
  },
  loctionClassClick(e) {
    const num = e.currentTarget.dataset.num;
    const loction = this.data.loctionList[num];
    this.getLoction(loction.id);
    this.setData({
      loctionClassShow: false,
      province: loction
    })
  },
  labelChoose(e) {
    const num = e.currentTarget.dataset.num;
    const labels = this.data.labels;
    labels[num].isClick = !labels[num].isClick;
    this.setData({
      labels
    })
  },
  labelToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        labelShow: true
      })
    } else {
      // const labels = this.data.labels;
      // const joblabel = []
      // labels.map(item => {
      //   if (item.isClick) {
      //     joblabel.push(item.name)
      //   }
      // });
      this.setData({
        labelShow: false,
        // joblabel: joblabel.join('/')
      })
    }
  },
  chooseType(e) {
    const id = e.currentTarget.dataset.id;
    this.setData({
      type: id
    })
  },
  salaryInUpIn(e) {
    this.setData({
      salaryInUp: parseInt(e.detail.value)
    })
  },
  salaryInLowIn(e) {
    this.setData({
      salaryInLow: parseInt(e.detail.value)
    })
  },
  next() {
    const data = this.data;
    if (!data.name) {
      wx.showToast({
        title: '请输入岗位名称',
        icon: 'none'
      })
      return;
    }
    if (!data.loction) {
      wx.showToast({
        title: '请选择工作地点',
        icon: 'none'
      })
      return;
    }
    if (!data.salary) {
      wx.showToast({
        title: '请选择薪水范围',
        icon: 'none'
      })
      return;
    }
    if (!data.degree) {
      wx.showToast({
        title: '请选择学历要求',
        icon: 'none'
      })
      return;
    }
    if (!data.age) {
      wx.showToast({
        title: '请选择年龄要求',
        icon: 'none'
      })
      return;
    }
    if (!data.major) {
      wx.showToast({
        title: '请选择专业要求',
        icon: 'none'
      })
      return;
    }
    if (!data.joblabel) {
      wx.showToast({
        title: '请输入岗位标签',
        icon: 'none'
      })
      return;
    }
    if (!data.type) {
      wx.showToast({
        title: '请选择岗位类型',
        icon: 'none'
      })
      return;
    }
    if (!data.startDate) {
      wx.showToast({
        title: '请选择开始日期',
        icon: 'none'
      })
      return;
    }
    if (!data.endDate) {
      wx.showToast({
        title: '请选择结束日期',
        icon: 'none'
      })
      return;
    }
    if (!data.workAge) {
      wx.showToast({
        title: '请选择工作年限',
        icon: 'none'
      })
      return;
    }
    if (!data.jobdesc) {
      wx.showToast({
        title: '请输入岗位描述',
        icon: 'none'
      })
      return;
    }
    let fringe_benefits = [];
    data.fringe_benefits.map(item=>{
      if(item.isChoose){
        fringe_benefits.push(item.name)
      }
    })
    if (fringe_benefits.length==0){
      wx.showToast({
        title: '请选择福利待遇',
        icon: 'none'
      })
      return;
    }
    let salaryLow = '',
      salaryUp = '',
      workLow = '',
      workUp = '',
      ageLow = '',
      ageUp = '';
    if (data.salaryInLow && data.salaryInUp) {
      salaryLow = data.salaryInLow;
      salaryUp = data.salaryInUp;
    } else if (data.salaryNum == 0) {
      salaryLow = -1;
      salaryUp = -1;
    } else {
      salaryLow = data.salaryArr[data.salaryNum].low;
      salaryUp = data.salaryArr[data.salaryNum].up;
    }
    if (data.workAgeInLow && data.workAgeInUp) {
      workLow = data.workAgeInLow;
      workUp = data.workAgeInUp;
    } else if (data.workAgeNum == 0) {
      workLow = -1;
      workUp = -1;
    } else {
      workLow = data.work_age_arr[data.workAgeNum].low;
      workUp = data.work_age_arr[data.workAgeNum].up;
    }
    if (data.ageInLow && data.ageInUp) {
      ageLow = data.ageInLow;
      ageUp = data.ageInUp;
    } else if (data.ageNum == 0) {
      ageLow = -1;
      ageUp = -1;
    } else {
      ageLow = data.age_arr[data.ageNum].low;
      ageUp = data.age_arr[data.ageNum].up;
    }

    const job = {
      "age_lower": ageLow,
      "age_upper": ageUp,
      "degreeid": data.degreeList[data.degreeNum].id,
      "location": data.loction,
      "majorid": data.majorDetailList[data.majorNum].id,
      "name": data.name,
      "pay_lower": salaryLow,
      "pay_upper": salaryUp,
      "position_label": data.joblabel,
      "description": data.jobdesc,
      "valid_enddate": data.endDate + ' 00:00:00',
      "valid_startdate": data.startDate + ' 00:00:00',
      "type": data.type,
      "cityid": data.city.id,
      "provinceid": data.province.id,
      "work_age_lower": workLow,
      "work_age_upper": workUp,
      fringe_benefits: fringe_benefits.toString()
    }
    // try {
    //   wx.setStorageSync('job', JSON.stringify(job));
    // } catch (e) {

    // }
    // wx.navigateTo({
    //   url: '/pages/recrProcess/recrProcess',
    // })
    const status = globalData.userInfo.status;
    let title = '';
    switch (status) {
      case 0:
        title = '您的企业正在审核中，请耐心等待';
        break;
      case 1:
        title = '您的企业审核未通过';
        break;
      case 4:
        title = '您的企业已被临时封号';
        break;
      case 5:
        title = '您的企业已被永久封号';
        break;
      case 6:
        title = '您的企业已被删除';
        break;
      default:
        title = '';
        break;
    }
    if (title) {
      wx.showToast({
        title: title,
        icon: 'none'
      })
      return;
    }
    wx.showLoading({
      title: '请求中...',
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.job.add,
      method: 'POST',
      data: {
        "age_lower": job.age_lower,
        "age_upper": job.age_upper,
        "companyid": globalData.userInfo.id,
        "degreeid": job.degreeid,
        "location": job.location,
        "majorid": job.majorid,
        "name": job.name,
        "pay_lower": job.pay_lower,
        "pay_upper": job.pay_upper,
        "position_label": job.position_label,
        "description": job.description,
        "valid_enddate": job.valid_enddate,
        "valid_startdate": job.valid_startdate,
        "provinceid": job.provinceid,
        "type": job.type,
        "cityid": job.cityid,
        "work_age_lower": job.work_age_lower,
        "work_age_upper": job.work_age_upper,
        "fringe_benefits": job.fringe_benefits,
      },
      success: (res) => {
        wx.hideLoading();
        if (res.statusCode == 200) {
          wx.showToast({
            title: '新增成功',
          })
          wx.redirectTo({
            url: '/pages/postManage/postManage',
          })
        } else {
          wx.showToast({
            title: res.data.message,
            duration: 2000,
            icon: 'none'
          })
        }
      },
      complete: () => {

      },
      fail: (error) => {
        wx.hideLoading();
        wx.showToast({
          title: error,
          duration: 2000,
          icon: 'none'
        })
      }
    })
  },
  nameIn(e) {
    this.setData({
      name: e.detail.value
    })
  },

  jobdescIn(e) {
    this.setData({
      jobdesc: e.detail.value
    })
  },
  cityChange(e) {
    this.setData({
      city: e.detail.value[1] + e.detail.value[2]
    })
  },
  startDateChange(e) {
    this.setData({
      startDate: e.detail.value
    })
  },
  endDateChange(e) {
    if (new Date(this.data.startDate.replace(/-/g, '/')).getTime() >= new Date(e.detail.value.replace(/-/g, '/')).getTime()) {
      wx.showToast({
        title: '结束日期必须大于开始日期',
        icon: 'none'
      })
      return;
    }
    this.setData({
      endDate: e.detail.value
    })
  },
  salaryToggle(e) {

    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        salaryShow: true
      })
    } else {
      if (this.data.salaryInLow && this.data.salaryInUp) {
        if (this.data.salaryInLow < 0) {
          wx.showToast({
            title: '请输入正确的薪水值',
            icon: 'none'
          })
          return;
        }
        if (this.data.salaryInUp <= this.data.salaryInLow) {
          wx.showToast({
            title: '薪水上限必须大于薪水下限',
            icon: 'none'
          })
          return;
        }
        this.setData({
          salary: this.data.salaryInLow + '~' + this.data.salaryInUp,
          salaryShow: false
        })
        return;
      }

      if (this.data.salaryNum == 0) {
        this.setData({
          salary: `不限`
        })
      } else {
        this.setData({
          salary: `${this.data.salaryArr[this.data.salaryNum].low}~${this.data.salaryArr[this.data.salaryNum].up}`
        })
      }
      this.setData({
        salaryShow: false
      })
    }
  },
  salaryChange(e) {
    this.setData({
      salaryNum: e.detail.value[0],
    })

  },

  degreeToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        educationShow: true
      })
    } else {
      this.setData({
        educationShow: false
      })
      this.setData({
        degree: `${this.data.degreeList[this.data.degreeNum].name}`
      })
    }
  },
  degreeChange(e) {
    this.setData({
      degreeNum: e.detail.value[0]
    })
    this.setData({
      degree: `${this.data.degreeList[e.detail.value[0]].name}`
    })
  },
  majorToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        marjorMask: true
      })
    } else {
      this.setData({
        marjorMask: false
      })
      this.setData({
        major: this.data.majorDetailList[this.data.majorNum].name
      })
    }
  },
  majorChange(e) {
    this.getMajor(this.data.majorList[e.detail.value[0]].id);
    this.setData({
      majorNum: e.detail.value[1]
    })
    this.setData({
      major: this.data.majorDetailList[e.detail.value[1]].name
    })
  },
  getMajor(id) {
    wx.request({
      url: globalData.api.baseUrl + globalData.api.dict.majorList + id,
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            majorDetailList: res.data.result
          })
        }
      }
    })
  },
  getLoction(id) {
    if(id==-1){
      this.setData({
        loctionDetailList:[{id:-1,name:'不限'}]
      })
      return;
    }
    wx.request({
      url: globalData.api.baseUrl + globalData.api.dict.city + id,
      success: (res) => {
        if (res.statusCode == 200) {
          const loctionDetailList = res.data.result;
          this.setData({
            loctionDetailList
          })
        }
      }
    })
  },
  workAgeInUpIn(e) {
    this.setData({
      workAgeInUp: parseInt(e.detail.value)
    })
  },
  workAgeInLowIn(e) {
    this.setData({
      workAgeInLow: parseInt(e.detail.value)
    })
  },
  workAgeToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        workAgeShow: true
      })
    } else {
      if (this.data.workAgeInLow && this.data.workAgeInUp) {
        if (this.data.workAgeInLow < 0) {
          wx.showToast({
            title: '请输入正确的工作年限',
            icon: 'none'
          })
          return;
        }
        if (this.data.workAgeInUp <= this.data.workAgeInLow) {
          wx.showToast({
            title: '工作年限上限必须大于工作年限下限',
            icon: 'none'
          })
          return;
        }
        this.setData({
          workAge: this.data.workAgeInLow + '~' + this.data.workAgeInUp,
          workAgeShow: false
        })
        return;
      }

      if (this.data.workAgeNum == 0) {
        this.setData({
          workAge: `不限`
        })
      } else {
        this.setData({
          workAge: `${this.data.work_age_arr[this.data.workAgeNum].low}~${this.data.work_age_arr[this.data.workAgeNum].up}`
        })
      }
      this.setData({
        workAgeShow: false
      })
    }
  },
  workAgeChange(e) {
    this.setData({
      workAgeNum: e.detail.value[0]
    })

  },
  ageInUpIn(e) {
    this.setData({
      ageInUp: parseInt(e.detail.value)
    })
  },
  ageInLowIn(e) {
    this.setData({
      ageInLow: parseInt(e.detail.value)
    })
  },
  ageToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        ageShow: true
      })
    } else {
      if (this.data.ageInLow && this.data.ageInUp) {
        if (this.data.ageInLow < 0) {
          wx.showToast({
            title: '请输入正确的年龄',
            icon: 'none'
          })
          return;
        }
        if (this.data.ageInUp <= this.data.ageInLow) {
          wx.showToast({
            title: '年龄上限必须大于年龄下限',
            icon: 'none'
          })
          return;
        }
        this.setData({
          age: this.data.ageInLow + '~' + this.data.ageInUp,
          ageShow: false
        })
        return;
      }

      if (this.data.ageNum == 0) {
        this.setData({
          age: `不限`
        })
      } else {
        this.setData({
          age: `${this.data.age_arr[this.data.ageNum].low}~${this.data.age_arr[this.data.ageNum].up}`
        })
      }
      this.setData({
        ageShow: false
      })
    }
  },
  ageChange(e) {
    this.setData({
      ageNum: e.detail.value[0]
    })

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    wx.request({
      url: globalData.api.baseUrl + globalData.api.dict.province,
      method: 'GET',
      success: (res) => {
        if (res.statusCode == 200) {
          const arr = res.data.result;
          arr.unshift({ id: -1, name: '不限' });
          this.setData({
            loctionList: arr
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      complete: () => {

      }
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.dict.degreeList,
      method: 'GET',
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            degreeList: res.data.result
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      complete: () => {

      }
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.dict.majorclass,
      method: 'GET',
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            majorList: res.data.result.reverse()
          })
          this.getMajor(res.data.result[0].id)
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      complete: () => {

      }
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.job.label,
      success: (res) => {
        if (res.statusCode == 200) {
          const labels = res.data.result;
          labels.map(item => {
            item.isClick = false;
          })
          this.setData({
            labels
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})