// pages/postManage/postManage.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageIndex: 1,
    pageSize: 10,
    jobs: [],
    delBtnWidth: 250,
    startX: "",
    startY: "",
    noreadcount:0,
    postHintShow:false
  },
  hintHide(){
    this.setData({
      postHintShow:false
    })
   
  },
  touchS: function(e) {
    if (e.touches.length == 1) {
      this.setData({
        //设置触摸起始点水平方向位置
        startX: e.touches[0].clientX,
        startY: e.touches[0].clientY
      });
    }
  },
  touchM: function(e) {
    if (e.touches.length == 1) {
      //手指移动时水平方向位置
      var moveX = e.touches[0].clientX;
      var moveY = e.touches[0].clientY;
      //手指起始点位置与移动期间的差值
      var disX = this.data.startX - moveX;
      var disY = this.data.startY - moveY;
      if(Math.abs(disY)>Math.abs(disX)){
        return;
      }
      var delBtnWidth = this.data.delBtnWidth;
      var txtStyle = "";
      if (disX == 0 || disX < 0) { //如果移动距离小于等于0，说明向右滑动，文本层位置不变
        txtStyle = "left:0px";
      } else if (disX > 0) { //移动距离大于0，文本层left值等于手指移动距离
        txtStyle = "left:-" + disX + "px";
        if (disX >= delBtnWidth) {
          //控制手指移动距离最大值为删除按钮的宽度
          txtStyle = "left:-" + delBtnWidth + "px";
        }
      }
      //获取手指触摸的是哪一项
      var index = e.currentTarget.dataset.index;
      var jobs = this.data.jobs;
      jobs[index].txtStyle = txtStyle;
      //更新列表的状态
      this.setData({
        jobs
      });
    }
  },
  touchE: function(e) {
    if (e.changedTouches.length == 1) {
      //手指移动结束后水平位置
      var endX = e.changedTouches[0].clientX;
      var endY = e.changedTouches[0].clientY;
      //触摸开始与结束，手指移动的距离
      var disX = this.data.startX - endX;
      var disY = this.data.startY - endY; 
      if(Math.abs(disY)>Math.abs(disX)){
        return;
      }
      var delBtnWidth = this.data.delBtnWidth;
      //如果距离小于删除按钮的1/2，不显示删除按钮
      var txtStyle = disX > delBtnWidth / 2 ? "left:-" + delBtnWidth + "px" : "left:0px";
      //获取手指触摸的是哪一项
      var index = e.currentTarget.dataset.index;
      var jobs = this.data.jobs;
      jobs[index].txtStyle = txtStyle;
      //更新列表的状态
      this.setData({
        jobs
      });
    }
  },
  //获取元素自适应后的实际宽度
  getEleWidth: function(w) {
    var real = 0;
    try {
      var res = wx.getSystemInfoSync().windowWidth;
      var scale = (750 / 2) / (w / 2);
      real = Math.floor(res / scale);
      return real;
    } catch (e) {
      return false;
      // Do something when catch error
    }
  },
  initEleWidth: function() {
    var delBtnWidth = this.getEleWidth(this.data.delBtnWidth);
    this.setData({
      delBtnWidth: delBtnWidth
    });
  },
  //点击删除按钮事件
  delItem: function(e) {
    //获取列表中要删除项的下标
    var index = e.currentTarget.dataset.index;
    var status = e.currentTarget.dataset.status;
    var jobs = this.data.jobs;
    wx.request({
      url: globalData.api.baseUrl + globalData.api.job.status + jobs[index].id+'?status='+status,
      method: 'PUT',
      success: (res) => {
        if (res.statusCode == 200) {
          //移除列表中下标为index的项
          jobs.splice(index, 1);
          //更新列表的状态
          this.setData({
            jobs
          });
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      fail: (error) => {
        wx.showToast({
          title: error,
          icon: 'none'
        })
      }
    })

  },
  edit() {
    const status = globalData.userInfo.status;
    let title = '';
    switch (status) {
      case 0:
        title = '您的企业正在审核中，请耐心等待';
        break;
      case 1:
        title = '您的企业审核未通过';
        break;
      case 4:
        title = '您的企业已被临时封号';
        break;
      case 5:
        title = '您的企业已被永久封号';
        break;
      case 6:
        title = '您的企业已被删除';
        break;
      default:
        title = '';
        break;
    }
    if (title) {
      wx.showToast({
        title: title,
        icon: 'none'
      })
      return;
    }
    wx.navigateTo({
      url: '/pages/postEdit/postEidt',
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    })
  },
  checkApplicant(e) {
    wx.navigateTo({
      url: `/pages/applicantLists/applicantLists?id=${e.currentTarget.dataset.id}`,
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    })
  },
  getJob() {
    const data = this.data;
    wx.showLoading({
      title: '加载中...',
    })
    wx.request({
      url: `${globalData.api.baseUrl}${globalData.api.job.list}${data.pageIndex}/${data.pageSize}/${globalData.userInfo.id}`,
      method: 'GET',
      success: (res) => {
        wx.hideLoading();
        if (res.statusCode == 200) {
          const jobs = res.data.result.data;
          let arr = this.data.jobs;
          jobs.map((item) => {
            item.lastmodified = item.lastmodified ? item.lastmodified.slice(0, 10).replace(/-/g, '.') : item.lastmodified;
            item.valid_startdate = item.valid_startdate ? item.valid_startdate.slice(0, 10).replace(/-/g, '.') : item.valid_startdate;
            item.valid_enddate = item.valid_enddate ? item.valid_enddate.slice(0, 10).replace(/-/g, '.') : item.valid_enddate;
          })
          arr = arr.concat(jobs);
          if(data.pageIndex==1&&((arr&&arr.length==0)||!arr)){
            this.setData({
              postHintShow:true
            })
          }
          this.setData({
            jobs: arr
          })
        } else {
          wx.showToast({
            title: res.data.message,
            duration: 2000,
            icon: 'none'
          })
        }
      },
      complete: () => {

      },
      fail: (error) => {
        wx.hideLoading();
        wx.showToast({
          title: error,
          duration: 2000,
          icon: 'none'
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.getJob();
    wx.request({
      url: globalData.api.baseUrl + globalData.api.message.noreadcount + '?msgType=company' + '&accountid=' + globalData.userInfo.id,
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            noreadcount: res.data.result
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    this.initEleWidth();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    let pageIndex = this.data.pageIndex;
    pageIndex++;
    this.setData({
      pageIndex
    })
    this.getJob();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})