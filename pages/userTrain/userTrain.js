// pages/userTrain/userTrain.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    startDate:'',
    endDate:'',
    name:'',
    desc:''
  },
  startDateChange(e){
      this.setData({
        startDate:e.detail.value
      })
  },
  endDateChange(e){
    if (new Date(this.data.startDate.replace(/-/g, '/')).getTime() >= new Date(e.detail.value.replace(/-/g, '/')).getTime()) {
      wx.showToast({
        title: '结束时间不能小于开始时间',
        icon: 'none'
      })
      return;
    }
      this.setData({
        endDate:e.detail.value
      })
  },
  nameIn(e){
    this.setData({
      name:e.detail.value
    })
  },
  descIn(e){
    this.setData({
      desc:e.detail.value
    })
  },
  save() {
    const data = this.data;
    if (!data.name) {
      wx.showToast({
        title: '请输入培训机构',
        icon: 'none'
      })
      return;
    }
    if (!data.startDate) {
      wx.showToast({
        title: '请选择开始时间',
        icon: 'none'
      })
      return;
    }
    if (!data.endDate) {
      wx.showToast({
        title: '请选择结束时间',
        icon: 'none'
      })
      return;
    }
    if (!data.desc) {
      wx.showToast({
        title: '请输入培训课程',
        icon: 'none'
      })
      return;
    }
    let method = '', postData = null, trainUrl='';
    if (globalData.cardId == 3) {
      trainUrl = globalData.api.employee.train;
    } else if (globalData.cardId == 4) {
      trainUrl = globalData.api.user.train;
    }
    if (data.id) {
      method = 'PUT';
      postData = {
        "begin_date": data.startDate+' 00:00:00',
        "course": data.name,
        "end_date": data.endDate+' 00:00:00',
        "institutions": data.desc,
        "id": data.id,
      }
    } else {
      method = 'POST';
      postData = {
        "begin_date": data.startDate + ' 00:00:00',
        "course": data.name,
        "end_date": data.endDate + ' 00:00:00',
        "institutions": data.desc,
      }
      if (globalData.cardId == 3) {
        postData.employeeid = globalData.userInfo.id;
      } else if (globalData.cardId == 4) {
        postData.userid = globalData.userInfo.id;
      }
    }
    wx.showLoading({
      title: '保存中...',
    })
    wx.request({
      url: globalData.api.baseUrl + trainUrl,
      method: method,
      data: postData,
      success: (res) => {
        wx.hideLoading();
        if (res.statusCode == 200) {
          wx.showToast({
            title: '保存成功',
          })
          wx.navigateBack({
            delta: 1,
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      fail: (error) => {
        wx.hideLoading();
        wx.showToast({
          title: error,
          icon: 'none'
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const id = options.id;
    if (id) {
      wx.setNavigationBarTitle({
        title: '编辑培训经历',
      })
      try {
        let trainJson = wx.getStorageSync('train');
        if (trainJson) {
          const train = JSON.parse(trainJson);
          this.setData({
            id: parseInt(id),
            startDate: train.begin_date.slice(0,10),
            endDate: train.end_date.slice(0, 10),
            name: train.course,
            desc: train.institutions
          })
          wx.removeStorageSync('train');
        }
      } catch (e) {

      }
    } else {
      wx.setNavigationBarTitle({
        title: '添加培训经历',
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})