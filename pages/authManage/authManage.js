// pages/authManage/authManage.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    nextStep: false,
    employeeLists: [],
    pageIndex: 1,
    pageSize: 10,
    chooseId: '',
    tel:'',
    roleList: [],
    roleId: '',
    searchPageIndex: 1,
    searchPageSize: 10,
    search: ''
  },
  searchIn(e) {
    let val = e.detail.value.trim();
    if (val) {
      wx.request({
        url: `${globalData.api.baseUrl}${globalData.api.employee.search}${globalData.userInfo.id}/${val}?pageIndex=${this.data.searchPageIndex}&pageSize=${this.data.searchPageSize}`,
        success: (res) => {
          if (res.statusCode == 200) {
            this.setData({
              employeeLists: res.data.result.data
            })
          } else {
            wx.showToast({
              title: res.data.message,
              icon: 'none'
            })
          }
        },
        fail: (error) => {
          wx.showToast({
            title: error,
            icon: 'none'
          })
        }
      })
    } else {
      this.getEmp();
    }
    this.setData({
      search: val
    })
  },
  next(){
    if(!this.data.chooseId){
      wx.showToast({
        title: '请选择员工',
        icon:'none'
      })
      return;
    }
    this.setData({
      nextStep:true
    })
  },
  chooseEmp(e) {
    this.setData({
      chooseId: e.currentTarget.dataset.id,
      tel: e.currentTarget.dataset.tel
    })
  },
  chooseRole(e) {
    const roleId = e.currentTarget.dataset.id;
    this.setData({
      roleId
    })
  },
  comfirm() {
    const data = this.data;
    if(!data.chooseId){
      wx.showToast({
        title: '请选择员工',
        icon:'none'
      })
      return;
    }
    if(!data.roleId){
      wx.showToast({
        title: '请选择权限',
        icon:'none'
      })
      return;
    }
    wx.request({
      url: `${globalData.api.baseUrl}${globalData.api.employee.convertcompanyuser}?telephone=${data.tel}&roleID=${data.roleId}`,
      method:'POST',
      success:(res)=>{
        if(res.statusCode==200){
          if(res.data.result){
            wx.showToast({
              title: '设置成功',
            })
            wx.navigateBack({
              delta: 1,
            })
          }else{
            wx.showToast({
              title: res.data.result,
              icon: 'none'
            })
          }
          
        }else{
          wx.showToast({
            title: res.data.message,
            icon:'none'
          })
        }
      },
      fail:(error)=>{
        wx.showToast({
          title: error,
          icon:'none'
        })
      }
    })
  },
  getEmp() {
    const data = this.data;
    wx.request({
      url: `${globalData.api.baseUrl}${globalData.api.employee.list}${data.pageIndex}/${data.pageSize}/${globalData.userInfo.id}`,
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            employeeLists: res.data.result.data
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      complete: () => {

      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const data = this.data;
    this.getEmp();
    wx.request({
      url: globalData.api.baseUrl + globalData.api.dict.roleList,
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            roleList: res.data.result
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      fail: (error) => {
        wx.showToast({
          title: error,
          icon: 'none'
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})