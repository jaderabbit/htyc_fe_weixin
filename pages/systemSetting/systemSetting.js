// pages/systemSetting/systemSetting.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    prePassword: '',
    password: '',
    confrimPassword: '',
    cardId:'',
    confirmDeleteShow:false
  },
  confirmDeleteToggle(e) {
    const status = e.currentTarget.dataset.status;
    const confirmDeleteShow = status == 1 ? true : false;
    this.setData({
      confirmDeleteShow
    })
  },
  confirmDelete(){
    let url = '';
    if (this.data.cardId == 3) {
      url = globalData.api.baseUrl + globalData.api.employee.update + globalData.userInfo.id
    } else if (this.data.cardId == 4) {
      url = globalData.api.baseUrl + globalData.api.user.edit + globalData.userInfo.id
    }
    wx.request({
      url: url,
      method: 'DELETE',
      success: (res) => {
        if (res.statusCode == 200) {
          wx.showToast({
            title: '注销成功',
            icon: 'none',
            duration: 1000,
            mask: true,
            success: ()=> {
              try {
                wx.removeStorageSync('userInfo');
                wx.removeStorageSync('loginTime');
              } catch (e) {

              }
              wx.reLaunch({
                url: '/pages/login/login',
                success: function (res) { },
                fail: function (res) { },
                complete: function (res) { },
              })
            },
            fail: function(res) {},
            complete: function(res) {},
          })
         
        }else{
          wx.showToast({
            title: data.message,
            icon:'none'
          })
        }
      },
      fail:(error)=>{
        wx.showToast({
          title: error,
          icon:'none'
        })
      }
    })
  },
  cancellation(){
    this.setData({
      confirmDeleteShow:true
    })
  },
  loginOut(){
    try{
      wx.removeStorageSync('userInfo');
      wx.removeStorageSync('loginTime');
      wx.removeStorageSync('cardId');
      globalData.cardId=1;
    }catch(e){

    }
    wx.reLaunch({
      url: '/pages/index/index',
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    })
  },
  prePasswordIn(e) {
    this.setData({
      prePassword: e.detail.value
    })
  },
  passwordIn(e) {
    this.setData({
      password: e.detail.value
    })
  },
  confrimPasswordIn(e) {
    this.setData({
      confrimPassword: e.detail.value
    })
  },
  update() {
    const data = this.data;
    if (!data.prePassword) {
      wx.showToast({
        title: '请输入原密码',
        duration: 1500
      })
      return;
    }
    if (!data.password) {
      wx.showToast({
        title: '请输入新密码',
        duration: 1500
      })
      return;
    }
    if (!data.confrimPassword) {
      wx.showToast({
        title: '请确认新密码',
        duration: 1500
      })
      return;
    }
    if (data.confrimPassword != data.password) {
      wx.showToast({
        title: '两次密码不一致',
        duration: 1500
      })
      return;
    }
    wx.showLoading({
      title: '修改密码中...',
    })
    if (globalData.cardId == 4) {
      wx.request({
        url: globalData.api.baseUrl + globalData.api.user.changepassword,
        method:'PUT',
        data:{
          "newPassword":data.password,
          "oldPassword": data.prePassword,
          "userid": globalData.userInfo.id
        },
        success:(res)=>{
          wx.hideLoading();
          if(res.statusCode==200){
            wx.showToast({
              title: '修改成功',
            })
         wx.navigateBack({
           delta: 1,
         })
          }else{
            wx.showToast({
              title: res.data.message,
              duration:2000,
              icon:'none'
            })
          }
        },
        complete:()=>{
          
        },
        fail:(error)=>{
          wx.hideLoading();
          wx.showToast({
            title: error,
            duration: 2000,
            icon: 'none'
          })
        }
      })
      return;
    }
    if (globalData.cardId == 3) {
      wx.request({
        url: globalData.api.baseUrl + globalData.api.employee.changepassword,
        method:'PUT',
        data:{
          "newPassword":data.password,
          "oldPassword": data.prePassword,
          "userid": globalData.userInfo.id
        },
        success:(res)=>{
          wx.hideLoading();
          if(res.statusCode==200){
            wx.showToast({
              title: '修改成功',
            })
          wx.navigateBack({
            delta: 1,
          })
          }else{
            wx.showToast({
              title: res.data.message,
              duration:2000,
              icon:'none'
            })
          }
        },
        complete:()=>{
         
        },
        fail:(error)=>{
          wx.hideLoading();
          wx.showToast({
            title: error,
            duration: 2000,
            icon: 'none'
          })
        }
      })
      return;
    }
    if (globalData.cardId == 1) {
      wx.request({
        url: globalData.api.baseUrl + globalData.api.company.changepassword,
        method:'PUT',
        data:{
          "newPassword":data.password,
          "oldPassword": data.prePassword,
          "userid": globalData.userInfo.user.id
        },
        success:(res)=>{
          wx.hideLoading();
          if(res.statusCode==200){
            wx.showToast({
              title: '修改成功',
            })
          wx.navigateBack({
            delta: 1,
          })
          }else{
            wx.showToast({
              title: res.data.message,
              duration:2000,
              icon:'none'
            })
          }
        },
        complete:()=>{
         
        },
        fail:(error)=>{
          wx.hideLoading();
          wx.showToast({
            title: error,
            duration: 2000,
            icon: 'none'
          })
        }
      })
      return;
    }

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      cardId:globalData.cardId
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },


})