// pages/staffResume/staffResume.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    certificate: [],
    edu: [],
    workexp: [],
    userInfo: {},
    lange: [],
    expand: {},
    skills: [],
    trains: [],
    projectexp: [],
    oprShow: false,
    resumeShow: false,
    noreadcount: 0
  },
  resumeShowToggle(e) {
    const type = e.currentTarget.dataset.type;
    let resumeShow = type == 1 ? true : false;
    this.setData({
      resumeShow
    })
  },
  oprToggle() {
    this.setData({
      oprShow: !this.data.oprShow
    })
  },
 
  toExpand(e) {
    const type = e.currentTarget.dataset.type;
    const id = e.currentTarget.dataset.id;
    try {
      if (type == 1) {
        wx.setStorageSync('self', this.data.expand.self_evaluation)
      } else {
        wx.setStorageSync('expand', this.data.expand.additional_information)
      }
    } catch (e) {

    }
    wx.navigateTo({
      url: '/pages/userSelfIntro/userSelfIntro?type=' + type + '&id=' + id,
    })
  },
  toTrain(e) {
    const id = e.currentTarget.dataset.id;
    const num = e.currentTarget.dataset.num;
    let url = '';
    if (id) {
      try {
        wx.setStorageSync('train', JSON.stringify(this.data.trains[num]))
      } catch (e) {

      }
      url = `/pages/userTrain/userTrain?id=${id}`
    } else {
      url = '/pages/userTrain/userTrain'
    }
    wx.navigateTo({
      url: url,
    })

  },
  toSkills(e) {
    const id = e.currentTarget.dataset.id;
    const num = e.currentTarget.dataset.num;
    let url = '';
    if (id) {
      try {
        wx.setStorageSync('skill', JSON.stringify(this.data.skills[num]))
      } catch (e) {

      }
      url = `/pages/userSkills/userSkills?id=${id}`
    } else {
      url = '/pages/userSkills/userSkills'
    }
    wx.navigateTo({
      url: url,
    })
  },
  toLange(e) {
    const id = e.currentTarget.dataset.id;
    const num = e.currentTarget.dataset.num;
    let url = '';
    if (id) {
      try {
        wx.setStorageSync('lange', JSON.stringify(this.data.lange[num]))
      } catch (e) {

      }
      url = `/pages/userLange/userLange?id=${id}`
    } else {
      url = '/pages/userLange/userLange'
    }
    wx.navigateTo({
      url: url,
    })
  },
  toEditInfo() {
    wx.navigateTo({
      url: '/pages/staffBasicInfo/staffBasicInfo',
      success: function (res) { },
      fail: function (res) { },
      complete: function (res) { },
    })
  },
  toProjectxp(e) {
    const id = e.currentTarget.dataset.id;
    const num = e.currentTarget.dataset.num;
    let url = '';
    if (id) {
      try {
        wx.setStorageSync('projectxp', JSON.stringify(this.data.projectexp[num]))
      } catch (e) {

      }
      url = `/pages/userProject/userProject?id=${id}`
    } else {
      url = '/pages/userProject/userProject'
    }
    wx.navigateTo({
      url: url,
      success: function (res) { },
      fail: function (res) { },
      complete: function (res) { },
    })
  },
  toWorkexp(e) {
    const id = e.currentTarget.dataset.id;
    const num = e.currentTarget.dataset.num;
    let url = '';
    if (id) {
      try {
        wx.setStorageSync('workexp', JSON.stringify(this.data.workexp[num]))
      } catch (e) {

      }
      url = `/pages/userWokrep/userWokrep?id=${id}`
    } else {
      url = '/pages/userWokrep/userWokrep'
    }
    wx.navigateTo({
      url: url,
      success: function (res) { },
      fail: function (res) { },
      complete: function (res) { },
    })
  },
  toCer(e) {
    const id = e.currentTarget.dataset.id;
    const num = e.currentTarget.dataset.num;
    let url = '';
    if (id) {
      try {
        wx.setStorageSync('cer', JSON.stringify(this.data.certificate[num]))
      } catch (e) {

      }
      url = `/pages/userCer/userCer?id=${id}`
    } else {
      url = '/pages/userCer/userCer'
    }
    wx.navigateTo({
      url: url,
      success: function (res) { },
      fail: function (res) { },
      complete: function (res) { },
    })
  },
  toEdu(e) {
    const id = e.currentTarget.dataset.id;
    const num = e.currentTarget.dataset.num;
    let url = '';
    if (id) {
      try {
        wx.setStorageSync('edu', JSON.stringify(this.data.edu[num]))
      } catch (e) {

      }
      url = `/pages/userEdu/userEdu?id=${id}`
    } else {
      url = '/pages/userEdu/userEdu'
    }
    wx.navigateTo({
      url: url,
      success: function (res) { },
      fail: function (res) { },
      complete: function (res) { },
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   
    wx.request({
      url: globalData.api.baseUrl + globalData.api.message.noreadcount + '?msgType=employee' + '&accountid=' + globalData.userInfo.id,
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            noreadcount: res.data.result
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const userInfo = globalData.userInfo;
    if (userInfo.birthday) {
      let birthYear = parseInt(userInfo.birthday.slice(0, 4));
      let year = new Date().getFullYear();
      userInfo.age = year - birthYear  + '岁';
    } else {
      userInfo.age = '暂无'
    }
    this.setData({
      userInfo
    })
    let cerUrl = '', eduUrl = '', wrokexpUrl = '';
    if (globalData.cardId == 3) {
      cerUrl = globalData.api.employee.certificate;
      eduUrl = globalData.api.employee.edu;
      wrokexpUrl = globalData.api.employee.workexp;
    } else if (globalData.cardId == 4) {
      cerUrl = globalData.api.user.certificate;
      eduUrl = globalData.api.user.edu;
      wrokexpUrl = globalData.api.user.workexp;
    }
    wx.request({
      url: globalData.api.baseUrl + globalData.api.employee.lang + '/' + globalData.userInfo.id,
      success: (res) => {
        if (res.statusCode == 200) {
          const typeArr = ['一般', '良好', '熟练', '精通'];
          const arr = res.data.result;
          arr.map(item => {
            item.hear = typeArr[item.hear - 1];
            item.read_write = typeArr[item.read_write - 1];
          })
          this.setData({
            lange: arr
          })
        }
      }
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.employee.expand + '/' + globalData.userInfo.id,
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            expand: res.data.result
          })
        }
      }
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.employee.skill + '/' + globalData.userInfo.id,
      success: (res) => {
        if (res.statusCode == 200) {
          const typeArr = ['一般', '良好', '熟练', '精通'];
          const arr = res.data.result;
          arr.map(item => {
            item.mastery_level = typeArr[item.mastery_level - 1];
          })
          this.setData({
            skills: arr
          })
        }
      }
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.employee.train + '/' + globalData.userInfo.id,
      success: (res) => {
        if (res.statusCode == 200) {
          const arr = res.data.result;
          arr.map(item => {
            item.start = item.begin_date.slice(0, 7).replace(/-/g, '.');
            item.end = item.end_date.slice(0, 7).replace(/-/g, '.');
          })
          this.setData({
            trains: arr
          })
        }
      }
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.employee.projectexp + '/' + globalData.userInfo.id,
      success: (res) => {
        if (res.statusCode == 200) {
          const arr = res.data.result;
          arr.map(item => {
            item.start = item.begin_date.slice(0, 7).replace(/-/g, '.');
            item.end = item.end_date.slice(0, 7).replace(/-/g, '.');
          })
          this.setData({
            projectexp: arr
          })
        }
      }
    })
    wx.request({
      url: globalData.api.baseUrl + cerUrl + '/' + globalData.userInfo.id,
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            certificate: res.data.result
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      fail: (error) => {
        wx.showToast({
          title: error,
          icon: 'none'
        })
      }
    })
    wx.request({
      url: globalData.api.baseUrl + eduUrl + '/' + globalData.userInfo.id,
      success: (res) => {
        if (res.statusCode == 200) {
          const edu = res.data.result;
          edu.map((item) => {
            item.startYear = item.admission_date.slice(0, 4);
            item.endYear = item.graduation_date.slice(0, 4);
          })
          this.setData({
            edu
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      fail: (error) => {
        wx.showToast({
          title: error,
          icon: 'none'
        })
      }
    })
    wx.request({
      url: globalData.api.baseUrl + wrokexpUrl + '/' + globalData.userInfo.id,
      success: (res) => {
        if (res.statusCode == 200) {
          const workexp = res.data.result;
          workexp.map((item) => {
            item.startDate = item.employment_date ? item.employment_date.slice(0, 10).replace(/-/g, '.') : item.employment_date;
            item.endDate = item.leave_date ? item.leave_date.slice(0, 10).replace(/-/g, '.') : item.leave_date;
          })
          this.setData({
            workexp
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      fail: (error) => {
        wx.showToast({
          title: error,
          icon: 'none'
        })
      }
    })
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})