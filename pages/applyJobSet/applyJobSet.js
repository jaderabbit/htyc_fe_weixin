// pages/applyJobSet/applyJobSet.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    job_type:'',
    types: [{
      id: 1,
      name: '全职'
    }, {
      id: 2,
      name: '兼职'
    }, {
      id: 3,
      name: '实习'
    }],
    salaryShow: false,
    salaryArr: [{
      low: '不限',
      up: '不限'
    }, {
      low: 1000,
      up: 2000
    }, {
      low: 2000,
      up: 5000
    }, {
      low: 5000,
      up: 10000
    }],
    salaryNum: 0,
    salary: '',
    salaryInUp: '',
    salaryInLow: '',
    workAgeShow: false,
    work_age_arr: [{
      low: '不限',
      up: '不限'
    }, {
      low: 0,
      up: 1
    }, {
      low: 1,
      up: 2
    }, {
      low: 2,
      up: 3
    }, {
      low: 3,
      up: 4
    }],
    workAgeNum: 0,
    workAge: '',
    workAgeInUp: '',
    workAgeInLow: '',
    ageShow: false,
    age_arr: [{
      low: '不限',
      up: '不限'
    }, {
      low: 25,
      up: 30
    }, {
      low: 30,
      up: 35
    }, {
      low: 35,
      up: 40
    }, {
      low: 40,
      up: 45
    }],
    ageNum: 0,
    age: '',
    ageInUp: '',
    ageInLow: '',
    degreeList: [],
    degreeNum: NaN,
    majorDetailList: [],
    majorList: [],
    majorShow: false,
    majorClassShow: true,
    major: '',
    majorClass: '请选择',
    majorChooseArr: [],
    industryDetailList: [],
    industryList: [],
    industryShow: false,
    industryClassShow: true,
    industry: '',
    industryClass: '请选择',
    industryChooseArr: [],
    loctionDetailList: [],
    loctionList: [],
    loctionShow: false,
    loctionClassShow: true,
    loction: '',
    loctionClass: '请选择',
    loctionChooseArr: [],
    job: '',
    skill: '',
    recommendId: '',
    statusArr: [{
      name: '在职，无跳槽打算',
      id: 1
    }, {
        name: '在职，正在找工作',
      id: 2
    }, {
        name: '离职，正在找工作',
      id: 3
    }, {
        name: '离职，无找工作计划',
      id: 4
    }, {
        name: '自由职业者',
      id: 5
    }],
    statusShow:false,
    status:'',
    statusNum:0
  },
  chooseType(e) {
    const id = e.currentTarget.dataset.id;
    this.setData({
      job_type: id
    })
  },
  statusToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        statusShow: true
      })
    } else {
     
      this.setData({
        status: this.data.statusArr[this.data.statusNum].name,
        statusShow: false,
      })
      wx.request({
        url: `${globalData.api.baseUrl}${globalData.api.user.status}${globalData.userInfo.id}?status=${this.data.statusArr[this.data.statusNum].id}`,
        method:'PUT',
        success:(res)=>{
          if(res.statusCode==200){
            globalData.userInfo.status = this.data.statusArr[this.data.statusNum].id;
          }
        }
      })
    }

  },
  statusChange(e) {
    this.setData({
      statusNum: e.detail.value[0]
    })

  },
  majorToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        majorShow: true
      })
    } else {
      let major = [];
      this.data.majorChooseArr.map(item => {
        major.push(item.name);
      })
      this.setData({
        major: major.toString(),
        majorShow: false,
        majorClassShow: true
      })
    }

  },
  majorDelete(e) {
    const num = e.currentTarget.dataset.num;
    const majorChooseArr = this.data.majorChooseArr;
    let major = majorChooseArr.splice(num, 1);
    const majorDetailList = this.data.majorDetailList;
    majorDetailList.map(item => {
      if (item.id == major[0].id) {
        item.isClick = false;
      }
    })
    this.setData({
      majorChooseArr,
      majorDetailList
    })
  },
  majorChoose(e) {
    const num = e.currentTarget.dataset.num;
    const majorDetailList = this.data.majorDetailList;
    majorDetailList[num].isClick = true;
    const majorChooseArr = this.data.majorChooseArr;
    majorChooseArr.push(majorDetailList[num]);
    this.setData({
      majorDetailList,
      majorChooseArr
    })
  },
  
  majroNavClick() {
    this.setData({
      majorClassShow: true
    })
  },
  majorClassClick(e) {
    const num = e.currentTarget.dataset.num;
    const major = this.data.majorList[num];
    this.getMajor(major.id);
    this.setData({
      majorClass: major.name,
      majorClassShow: false
    })
  },
  industryToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        industryShow: true
      })
    } else {
      let industry = [];
      this.data.industryChooseArr.map(item => {
        industry.push(item.name);
      })
      this.setData({
        industry: industry.toString(),
        industryShow: false,
        industryClassShow: true
      })
    }

  },
  industryDelete(e) {
    const num = e.currentTarget.dataset.num;
    const industryChooseArr = this.data.industryChooseArr;
    let industry = industryChooseArr.splice(num, 1);
    const industryDetailList = this.data.industryDetailList;
    industryDetailList.map(item => {
      if (item.id == industry[0].id) {
        item.isClick = false;
      }
    })
    this.setData({
      industryChooseArr,
      industryDetailList
    })
  },
  industryChoose(e) {
    const num = e.currentTarget.dataset.num;
    const industryDetailList = this.data.industryDetailList;
    industryDetailList[num].isClick = true;
    const industryChooseArr = this.data.industryChooseArr;
    industryChooseArr.push(industryDetailList[num]);
    this.setData({
      industryDetailList,
      industryChooseArr
    })
  },
  industryNavClick() {
    this.setData({
      industryClassShow: true
    })
  },
  industryClassClick(e) {
    const num = e.currentTarget.dataset.num;
    const industry = this.data.industryList[num];
    this.getIndustry(industry.id);
    this.setData({
      industryClass: industry.name,
      industryClassShow: false
    })
  },
  loctionToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        loctionShow: true
      })
    } else {
      let loction = [];
      this.data.loctionChooseArr.map(item => {
        loction.push(item.name);
      })
      this.setData({
        loction: loction.toString(),
        loctionShow: false,
        loctionClassShow: true
      })
    }

  },
  loctionDelete(e) {
    const num = e.currentTarget.dataset.num;
    const loctionChooseArr = this.data.loctionChooseArr;
    let loction = loctionChooseArr.splice(num, 1);
    const loctionDetailList = this.data.loctionDetailList;
    loctionDetailList.map(item => {
      if (item.id == loction[0].id) {
        item.isClick = false;
      }
    })
    this.setData({
      loctionChooseArr,
      loctionDetailList
    })
  },
  loctionChoose(e) {
    const num = e.currentTarget.dataset.num;
    const loctionDetailList = this.data.loctionDetailList;
    loctionDetailList[num].isClick = true;
    const loctionChooseArr = this.data.loctionChooseArr;
    loctionChooseArr.push(loctionDetailList[num]);
    this.setData({
      loctionDetailList,
      loctionChooseArr
    })
  },
  loctionNavClick() {
    this.setData({
      loctionClassShow: true
    })
  },
  loctionClassClick(e) {
    const num = e.currentTarget.dataset.num;
    const loction = this.data.loctionList[num];
    this.getLoction(loction.id);
    this.setData({
      loctionClass: loction.name,
      loctionClassShow: false
    })
  },
  salaryInUpIn(e) {
    this.setData({
      salaryInUp: parseInt(e.detail.value)
    })
  },
  salaryInLowIn(e) {
    this.setData({
      salaryInLow: parseInt(e.detail.value)
    })
  },
  salaryToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        salaryShow: true
      })
    } else {
      if (this.data.salaryInLow && this.data.salaryInUp) {
        if (this.data.salaryInLow < 0) {
          wx.showToast({
            title: '请输入正确的薪水值',
            icon: 'none'
          })
          return;
        }
        if (this.data.salaryInUp <= this.data.salaryInLow) {
          wx.showToast({
            title: '薪水上限必须大于薪水下限',
            icon: 'none'
          })
          return;
        }
        this.setData({
          salary: this.data.salaryInLow + '~' + this.data.salaryInUp,
          salaryShow: false
        })
        return;
      }

      if (this.data.salaryNum == 0) {
        this.setData({
          salary: `不限`
        })
      } else {
        this.setData({
          salary: `${this.data.salaryArr[this.data.salaryNum].low}~${this.data.salaryArr[this.data.salaryNum].up}`
        })
      }
      this.setData({
        salaryShow: false
      })
    }
  },
  salaryChange(e) {
    this.setData({
      salaryNum: e.detail.value[0]
    })

  },
  workAgeInUpIn(e) {
    this.setData({
      workAgeInUp: parseInt(e.detail.value)
    })
  },
  workAgeInLowIn(e) {
    this.setData({
      workAgeInLow: parseInt(e.detail.value)
    })
  },
  workAgeToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        workAgeShow: true
      })
    } else {
      if (this.data.workAgeInLow && this.data.workAgeInUp) {
        if (this.data.workAgeInLow < 0) {
          wx.showToast({
            title: '请输入正确的工作年限',
            icon: 'none'
          })
          return;
        }
        if (this.data.workAgeInUp <= this.data.workAgeInLow) {
          wx.showToast({
            title: '工作年限上限必须大于工作年限下限',
            icon: 'none'
          })
          return;
        }
        this.setData({
          workAge: this.data.workAgeInLow + '~' + this.data.workAgeInUp,
          workAgeShow: false
        })
        return;
      }

      if (this.data.workAgeNum == 0) {
        this.setData({
          workAge: `不限`
        })
      } else {
        this.setData({
          workAge: `${this.data.work_age_arr[this.data.workAgeNum].low}~${this.data.work_age_arr[this.data.workAgeNum].up}`
        })
      }
      this.setData({
        workAgeShow: false
      })
    }
  },
  workAgeChange(e) {
    this.setData({
      workAgeNum: e.detail.value[0]
    })

  },
  ageInUpIn(e) {
    this.setData({
      ageInUp: parseInt(e.detail.value)
    })
  },
  ageInLowIn(e) {
    this.setData({
      ageInLow: parseInt(e.detail.value)
    })
  },
  ageToggle(e) {
    const status = e.currentTarget.dataset.status;
    if (status == 1) {
      this.setData({
        ageShow: true
      })
    } else {
      if (this.data.ageInLow && this.data.ageInUp) {
        if (this.data.ageInLow < 0) {
          wx.showToast({
            title: '请输入正确的年龄',
            icon: 'none'
          })
          return;
        }
        if (this.data.ageInUp <= this.data.ageInLow) {
          wx.showToast({
            title: '年龄上限必须大于年龄下限',
            icon: 'none'
          })
          return;
        }
        this.setData({
          age: this.data.ageInLow + '~' + this.data.ageInUp,
          ageShow: false
        })
        return;
      }

      if (this.data.ageNum == 0) {
        this.setData({
          age: `不限`
        })
      } else {
        this.setData({
          age: `${this.data.age_arr[this.data.ageNum].low}~${this.data.age_arr[this.data.ageNum].up}`
        })
      }
      this.setData({
        ageShow: false
      })
    }
  },
  ageChange(e) {
    this.setData({
      ageNum: e.detail.value[0]
    })

  },

  jobIn(e) {
    this.setData({
      job: e.detail.value
    })
  },
  skillIn(e) {
    this.setData({
      skill: e.detail.value
    })
  },
  degreeChoose(e) {
    const index = e.currentTarget.dataset.index;
    this.setData({
      degreeNum: index
    })
  },
  save() {
    const data = this.data;
    if (!data.job) {
      wx.showToast({
        title: '请输入岗位关键词',
        icon: 'none'
      })
      return;
    }
    if (!data.industry) {
      wx.showToast({
        title: '请选择行业关键词',
        icon: 'none'
      })
      return;
    }
    if (!data.job_type) {
      wx.showToast({
        title: '请选择工作类型',
        icon: 'none'
      })
      return;
    }
    if (!data.major) {
      wx.showToast({
        title: '请选择专业关键词',
        icon: 'none'
      })
      return;
    }
    if (!data.loction) {
      wx.showToast({
        title: '请选择区域关键词',
        icon: 'none'
      })
      return;
    }
    if (!data.skill) {
      wx.showToast({
        title: '请输入职能关键词',
        icon: 'none'
      })
      return;
    }
    if (isNaN(data.degreeNum)) {
      wx.showToast({
        title: '请选择学历关键词',
        icon: 'none'
      })
      return;
    }
    if (!data.salary) {
      wx.showToast({
        title: '请选择薪资范围',
        icon: 'none'
      })
      return;
    }
    if (!data.workAge) {
      wx.showToast({
        title: '请选择工作年限',
        icon: 'none'
      })
      return;
    }
    if (!data.age) {
      wx.showToast({
        title: '请选择年龄要求',
        icon: 'none'
      })
      return;
    }
    wx.showLoading({
      title: '保存设置中...',
    })
    let industry = [],
      major = [],
      location = [];
    data.industryChooseArr.map(item => {
      industry.push(item.id);
    })
    data.majorChooseArr.map(item => {
      major.push(item.id);
    })
    data.loctionChooseArr.map(item => {
      location.push(item.id);
    })
    let salaryLow = '',
      salaryUp = '',
      workLow = '',
      workUp = '',
      ageLow = '',
      ageUp = '';
    if (data.salaryInLow && data.salaryInUp) {
      salaryLow = data.salaryInLow;
      salaryUp = data.salaryInUp;
    } else if (data.salaryNum == 0) {
      salaryLow = -1;
      salaryUp = -1;
    } else {
      salaryLow = data.salaryArr[data.salaryNum].low;
      salaryUp = data.salaryArr[data.salaryNum].up;
    }
    if (data.workAgeInLow && data.workAgeInUp) {
      workLow = data.workAgeInLow;
      workUp = data.workAgeInUp;
    } else if (data.workAgeNum == 0) {
      workLow = -1;
      workUp = -1;
    } else {
      workLow = data.work_age_arr[data.workAgeNum].low;
      workUp = data.work_age_arr[data.workAgeNum].up;
    }
    if (data.ageInLow && data.ageInUp) {
      ageLow = data.ageInLow;
      ageUp = data.ageInUp;
    } else if (data.ageNum == 0) {
      ageLow = -1;
      ageUp = -1;
    } else {
      ageLow = data.age_arr[data.ageNum].low;
      ageUp = data.age_arr[data.ageNum].up;
    }
    let method = '',
      postData = null;
    if (data.recommendId) {
      method = 'PUT';
      postData = {
        "age_lower": ageLow,
        "age_upper": ageUp,
        "degreeid": data.degreeList[data.degreeNum].id,
        "industrys": industry.toString(),
        "jobs": data.job,
        "majors": major.toString(),
        "pay_lower": salaryLow,
        "pay_upper": salaryUp,
        "regions": location.toString(),
        "skills": data.skill,
        "id": data.recommendId,
        "job_type": data.job_type,
        "work_age_lower": workLow,
        "work_age_upper": workUp
      }
    } else {
      method = 'POST';
      postData = {
        "age_lower": ageLow,
        "age_upper": ageUp,
        "degreeid": data.degreeList[data.degreeNum].id,
        "industrys": industry.toString(),
        "jobs": data.job,
        "majors": major.toString(),
        "pay_lower": salaryLow,
        "pay_upper": salaryUp,
        "regions": location.toString(),
        "skills": data.skill,
        "userid": globalData.userInfo.id,
        "work_age_lower": workLow,
        "work_age_upper": workUp
      }
    }
    wx.request({
      url: globalData.api.baseUrl + globalData.api.user.recommend,
      method: method,
      data: postData,
      success: (res) => {
        wx.hideLoading();
        if (res.statusCode == 200) {
          wx.showToast({
            title: '设置成功',

          })
          wx.navigateBack({
            delta: 1,
          })
        } else {
          wx.showToast({
            title: res.data.message,
            duration: 2000,
            icon: 'none'
          })
        }
      },

      complete: () => {

      },
      fail: (error) => {
        wx.hideLoading();
        wx.showToast({
          title: error,
          duration: 2000,
          icon: 'none'
        })
      }
    })
  },
  getMajor(id) {
    wx.request({
      url: globalData.api.baseUrl + globalData.api.dict.majorList + id,
      success: (res) => {
        if (res.statusCode == 200) {
          const majorDetailList = res.data.result;
          majorDetailList.map(item => {
            if (this.data.majorChooseArr.some(inner => {
                return item.id == inner.id;
              })) {
              item.isClick = true;
            } else {
              item.isClick = false;
            }

          })
          this.setData({
            majorDetailList
          })
        }
      }
    })
  },
  getIndustry(id) {
    wx.request({
      url: globalData.api.baseUrl + globalData.api.dict.industry + id,
      success: (res) => {
        if (res.statusCode == 200) {
          const industryDetailList = res.data.result;
          industryDetailList.map(item => {
            if (this.data.industryChooseArr.some(inner => {
                return item.id == inner.id;
              })) {
              item.isClick = true;
            } else {
              item.isClick = false;
            }

          })
          this.setData({
            industryDetailList
          })
        }
      }
    })
  },
  getLoction(id) {
    wx.request({
      url: globalData.api.baseUrl + globalData.api.dict.city + id,
      success: (res) => {
        if (res.statusCode == 200) {
          const loctionDetailList = res.data.result;
          loctionDetailList.map(item => {
            if (this.data.loctionChooseArr.some(inner => {
                return item.id == inner.id;
              })) {
              item.isClick = true;
            } else {
              item.isClick = false;
            }

          })
          this.setData({
            loctionDetailList
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.getIndustry('');
    this.getMajor('');
    this.getLoction('');
    wx.request({
      url: globalData.api.baseUrl + globalData.api.dict.industryclass,
      method: 'GET',
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            industryList: res.data.result
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      complete: () => {

      }
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.dict.province,
      method: 'GET',
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            loctionList: res.data.result
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      complete: () => {

      }
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.dict.degreeList,
      method: 'GET',
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            degreeList: res.data.result
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      complete: () => {

      }
    })
    wx.request({
      url: globalData.api.baseUrl + globalData.api.dict.majorclass,
      method: 'GET',
      success: (res) => {
        if (res.statusCode == 200) {
          this.setData({
            majorList: res.data.result
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      complete: () => {

      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    wx.request({
      url: globalData.api.baseUrl + globalData.api.user.userRecommend + globalData.userInfo.id,
      success: (res) => {
        if (res.statusCode == 200) {
          const data = res.data.result;
          let industrys = data.industrys.split(',');
          let degreeNum = this.data.degreeNum;
          const majors = data.majors.split(',');
          const regions = data.regions.split(',');
          const industry = [],
            major = [],
            region = [];
          const industryChooseArr = this.data.industryChooseArr;
          const majorChooseArr = this.data.majorChooseArr;
          const loctionChooseArr = this.data.loctionChooseArr;
          let salary = this.data.salary;
          let salaryNum = this.data.salaryNum;
          let salaryInLow = this.data.salaryInLow;
          let salaryInUp = this.data.salaryInUp;
          let workAge = this.data.workAge;
          let workAgeNum = this.data.workAgeNum;
          let workAgeInLow = this.data.workAgeInLow;
          let workAgeInUp = this.data.workAgeInUp;
          let age = this.data.age;
          let ageNum = this.data.ageNum;
          let ageInLow = this.data.ageInLow;
          let ageInUp = this.data.ageInUp;
          if (data.pay_lower == -1) {
            salary = '不限';
            salaryNum = 0;
          } else {
            salary = `${data.pay_lower}~${data.pay_upper}`;
            salaryInLow = data.pay_lower;
            salaryInUp = data.pay_upper;
          }
          if (data.work_age_lower == -1) {
            workAge = '不限';
            workAgeNum = 0;
          } else {
            workAge = `${data.work_age_lower}~${data.work_age_upper}`;
            workAgeInLow = data.work_age_lower;
            workAgeInUp = data.work_age_upper;
          }
          if (data.age_lower == -1) {
            age = '不限';
            ageNum = 0;
          } else {
            age = `${data.age_lower}~${data.age_upper}`;
            ageInLow = data.age_lower;
            ageInUp = data.age_upper;
          }
          this.data.degreeList.map((item, index) => {
            if (item.id == data.degreeid) {
              degreeNum = index;
            }
          })
          industrys.map(item => {
            this.data.industryDetailList.map(inner => {
              if (inner.id == item) {
                industryChooseArr.push(inner);
                industry.push(inner.name);
              }
            })
          })
          majors.map(item => {
            this.data.majorDetailList.map(inner => {
              if (inner.id == item) {
                majorChooseArr.push(inner);
                major.push(inner.name);
              }
            })
          })
          regions.map(item => {
            this.data.loctionDetailList.map(inner => {
              if (inner.id == item) {
                loctionChooseArr.push(inner);
                region.push(inner.name);
              }
            })
          })
          this.setData({
            industry: industry.toString(),
            industryChooseArr,
            major: major.toString(),
            majorChooseArr,
            loction: region.toString(),
            loctionChooseArr,
            job: data.jobs,
            skill: data.skills,
            degreeNum,
            salary,
            salaryNum,
            salaryInLow,
            salaryInUp,
            workAge,
            workAgeNum,
            workAgeInLow,
            workAgeInUp,
            age,
            ageNum,
            ageInLow,
            ageInUp,
            recommendId: data.id,
            job_type:data.job_type,
            status:this.data.statusArr[globalData.userInfo.status-1].name
          })
        } else {
          // wx.showToast({
          //   title: res.data.message,
          //   icon: 'none'
          // })
        }
      },
      fail: (error) => {
        wx.showToast({
          title: error,
          icon: 'none'
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})