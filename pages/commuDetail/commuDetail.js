// pages/commuDetail/commuDetail.js
const globalData = getApp().globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageIndex:1,
    pageSize:10,
    userList:[],
    id:'',
    name:''
  },
  toChat(e){
    const channelid = e.currentTarget.dataset.channelid;
    const employeeid = e.currentTarget.dataset.employeeid;
    wx.navigateTo({
      url: '/pages/messageDetail/messageDetail?employeeid=' + employeeid + '&channelid=' + channelid +'&type=2',
    })
  },
  toList(e){
    const channelid = e.currentTarget.dataset.channelid;
    const employeeid = e.currentTarget.dataset.employeeid;
    const empname = e.currentTarget.dataset.empname;
    wx.navigateTo({
      url: '/pages/commuDetailList/commuDetailList?employeeid=' + employeeid + '&channelid=' + channelid + '&name=' + this.data.name + '&empname=' + empname,
    })
  },
  getUser(){
    const data = this.data;
    wx.request({
      url: `${globalData.api.baseUrl}${globalData.api.channel.userlist}${data.pageIndex}/${data.pageSize}/${data.id}`,
      success: (res) => {
        if (res.statusCode == 200) {
          const arr = res.data.result.data;
          let userList = this.data.userList;
          arr.map(item=>{
            item.employee.name = item.employee.name.slice(0,1)+'**';
          })
          if(data.pageIndex>1){
            userList = userList.concat(arr);
          }else{
            userList = arr;
          }
          this.setData({
            userList
          })
        } else {
          wx.showToast({
            title: res.data.message,
            icon: 'none'
          })
        }
      },
      complete: () => {

      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const id = options.id,
    name = options.name;
    console.log(id)
    const data = this.data;
    wx.setNavigationBarTitle({
      title: name,
      
    })
    this.setData({
      id,
      name
    })
    this.getUser();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    let pageIndex = this.data.pageIndex;
    pageIndex++;
    this.setData({
      pageIndex
    })
    this.getUser();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})