//app.js
App({
  onLaunch: function () {
    try {
      const updateManager = wx.getUpdateManager()
      updateManager.onCheckForUpdate(function (res) {
        // 请求完新版本信息的回调
        console.log(res.hasUpdate)
      })
      updateManager.onUpdateReady(function () {
        wx.showModal({
          title: '更新提示',
          content: '新版本已经准备好，是否重启应用？',
          success: function (res) {
            if (res.confirm) {
              // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
              updateManager.applyUpdate()
            }
          }
        })

      })
    } catch (e) {

    }
    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
      }
    })
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo

              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        }
      }
    })
  },
  globalData: {
    userInfo: null,
    api:{
      baseUrl:'https://futurenext.com.cn/v1',
      company:{
        register:'/company/',
        update:'/company/',
        changepassword:'/company/changepassword',
        departmentList:'/company/department/list/',
        login:'/company/login/',
        loginByCode:'/company/login/identifyingcode/',
        role:'/company/role/',
        searchKey:'/company/search/',
        ads:'/company/ads',
        forgetpassword:'/company/forgetpassword',
        ask:'/company/job/ask',
        answer:'/company/job/ask/answer',
        invite:'/company/invite',
        expand:'/company/expand/ads/',
        isregister:'/company/mobilephone/isregister'
      },
      common:{
        uploadFile:'/common/WXUpload',
        captcha:'/common/captcha/',
        fileupload:'/common/fileupload',
        loginCaptcha:'/common/captcha/login/',
        registerCaptcha:'/common/captcha/register/',
        
      },
      channel:{
        edit:'/channel/',
        add:'/channel/',
        employeeEdit:'/channel/employee',
        employeeAdd:'/channel/employee',
        employeeGet:'/channel/employee/',
        list:'/channel/list/',
        userlist:'/channel/userlist/',
        detail:'/channel/',
        deleteC:'/channel/',
        employee: '/channel/employee',
        message:'/channel/message/list/',
        employeemessage:'/channel/message/employeelist/',
        empmessage:'/channel/message/channel/',
        latestlist:'/channel/message/latestlist/',
        sendmsg:'/channel/message',
        deletemsg:'/channel/message/ids',
        companyusernoreadcount:'/channel/message/companyuser/noreadcount'
      },
      employee:{
        update:'/employee/',
        register:'/employee/',
        changepassword:'/employee/changepassword',
        list:'/employee/list/',
        login:'/employee/login/',
        identifyingcode:'/employee/login/identifyingcode/',
        detail:'/employee/',
        deleteE:'/employee/',
        forgetpassword:'/employee/forgetpassword',
        percentcomplete:'/employee/percentcomplete/',
        certificate: '/employee/certificate',
        edu: '/employee/edu',
        workexp: '/employee/workexp',
        schoolSearch: '/employee/school/search/',
        search:'/employee/search/',
        lang: '/employee/lang',
        expand: '/employee/expand',
        skill: '/employee/skill',
        train: '/employee/train',
        projectexp: '/employee/projectexp',
        multisearch:'/employee/multisearch/',
        status:'/employee/status',
        convertuser:'/employee/convert/user',
        isregister:'/employee/mobilephone/isregister',
        convertcompanyuser:'/employee/convert/companyuser',
        number:'/employee/status/employee/number'
      },
      exam:{
        update:'/exam/',
        add:'/exam/',
        answer:'/exam/answer',
        list:'/exam/list/',
        editPaper:'/exam/paper/',
        addPaper:'/exam/paper/',
        paperList:'/exam/paper/list/',
        paperDetail:'/exam/paper/',
        paperUserList:'/exam/userlist/',
        examDetail:'/exam/',
        deleteExam:'/exam/',
        employeeExamlist:'/exam/employeeExamlist/',
        employeeallexamlist:'/exam/employeeallexamlist/',
        label:'/exam/label/'
      },
      job:{
        update:'/job/',
        add:'/job/',
        list:'/job/list/',
        applyList:'/job/userlist/',
        jobSet:'/job/recommend',
        recommendJob:'/job/recommend/',
        userApplyList:'/job/recommend/',
        jobDetail:'/job/',
        deleteJob:'/job/',
        userlistbycompanyid:'/job/userlistbycompanyid/',
        recommendcompany:'/job/recommendcompany/list/',
        procedureResult:'/job/procedure/result/',
        candidate:'/job/candidate',
        beforeanswer:'/job/recommend/beforeanswer/',
        label:'/job/label/list/',
        status:'/job/status/',
        applynumbers:'/job/recommend/apply/numbers/',
        revoke:'/job/recommend/revoke',
        remainnumbers:'/job/recommend/remain/numbers/'
      },
      resume:{
        update:'/resume/',
        add:'/resume/',
        help:'/resume/help',
        list:'/resume/list/',
        detailList:'/resume/detaillist/',
        deleteUpate:'/resume/',
        template:'/resume/template/list/'
      },
      user:{
        edit:'/user/',
        register:'/user/',
        changepassword:'/user/changepassword',
        login:'/user/login/',
        identifyingcode:'/user/login/identifyingcode/',
        recommend:'/user/recommend',
        forgetpassword:'/user/forgetpassword',
        percentcomplete:'/user/percentcomplete/',
        userRecommend:'/user/recommend/userid/',
        certificate:'/user/certificate',
        edu:'/user/edu',
        workexp:'/user/workexp',
        schoolSearch:'/user/school/search/',
        lang:'/user/lang',
        expand:'/user/expand',
        skill:'/user/skill',
        train:'/user/train',
        projectexp:'/user/projectexp',
        status:'/user/status/',
        invite:'/user/invite',
        ask:'/user/job/ask',
        answer:'/user/job/ask/answer/',
        videofile:'/user/videofile/',
        character:'/user/character',
        characteranswer:'/user/character/answer',
        isanswer:'/user/character/isanswer',
        baseInfo:'/user/userid/',
        convertemployee:'/user/convert/employee'
      },
      dict:{
        certificate:'/dict/certificate',
        certificateList:'/dict/certificate/list/',
        degree:'/dict/degree',
        degreeList:'/dict/degree/list/',
        major:'/dict/major',
        majorList:'/dict/major/list/',
        majorclass:'/dict/majorclass/list/',
        role:'/dict/role',
        roleList:'/dict/role/list/',
        title:'/dict/title',
        titleList:'/dict/title/list/',
        industryclass:'/dict/industryclass/list/',
        industry:'/dict/industry/list/',
        province:'/dict/province/list/',
        city:'/dict/city/list/'
      },
      message:{
        list:'/message/sys/list/',
        employee:'/message/employee/companylist/',
        companyEmp:'/message/employee/employeelist/',
        companyUser:'/message/user/userlist/',
        user:'/message/user/companylist/',
        epmDetail:'/message/employee/list/',
        userDetail:'/message/user/list/',
        userSend:'/message/user',
        epmSend:'/message/employee',
        userLast:'/message/user/latestlist/',
        epmLast:'/message/employee/latestlist/',
        status:'/message/status',
        noreadcount:'/message/sum/noreadcount',
        sysstatus:'/message/sys/status',
        deleteusermsg:'/message/usermsg/ids',
        deletesys:'/message/sys/msg/ids'
      },
      department:{
        list:'/department/list/',
        edit:'/department/'
      },
      thirdapplogin:{
        usermobilephone:'/thirdapplogin/wx/usermobilephone/'
      },
      offer:{
        company:'/offer/list/company/',
        user:'/offer/list/user/',
        opr:'/offer/'
      },
      comment:{
        add:'/comment/'
      }
    },
    cardId:1,//1:企业，2：其他，3：员工，4:个人
    isInner:false
  }
})