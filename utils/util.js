const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}
const getNoreadCount = (globalData,type,id)=>{
  let num = 0;
  wx.request({
    url: globalData.api.baseUrl + globalData.api.message.noreadcount + '?msgType=' + type +'&accountid='+id,
    success:(res)=>{
      if(res.statusCode==200){
        num = res.data.result;
      }
    }
  })
  return num;
}
module.exports = {
  formatTime: formatTime,
  getNoreadCount
}
